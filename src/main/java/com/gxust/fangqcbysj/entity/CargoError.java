package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("cargoerror")
public class CargoError implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * customer
     */
    @ApiModelProperty("customer")
    @TableField(value="customer")
    private String customer;

    /**
     * goods_bill_code
     */
    @ApiModelProperty("goods_bill_code")
    @TableField(value="goods_bill_code")
    private String goodsBillCode;

    /**
     * goods_name
     */
    @ApiModelProperty("goods_name")
    @TableField(value="goods_name")
    private String goodsName;

    /**
     * goods_revert_bill_code
     */
    @ApiModelProperty("goods_revert_bill_code")
    @TableField(value="goods_revert_bill_code")
    private String goodsRevertBillCode;

    /**
     * goods_value
     */
    @ApiModelProperty("goods_value")
    @TableField(value="goods_value")
    private Double goodsValue;

    /**
     * mistake_type
     */
    @ApiModelProperty("mistake_type")
    @TableField(value="mistake_type")
    private String mistakeType;

    /**
     * piece_amount
     */
    @ApiModelProperty("piece_amount")
    @TableField(value="piece_amount")
    private Integer pieceAmount;

    /**
     * size
     */
    @ApiModelProperty("size")
    @TableField(value="size")
    private String size;
}
