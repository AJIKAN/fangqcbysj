package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 客户回执信息
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("customerreceiptinfo")
public class CustomerReceiptInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type= IdType.AUTO)
    private Integer id;

    /**
     * carry_bill_event_id
     */
    @ApiModelProperty("carry_bill_event_id")
    @TableField(value="carry_bill_event_id")
    private Integer carryBillEventId;

    /**
     * check_goods_record
     */
    @ApiModelProperty("check_goods_record")
    @TableField(value="check_goods_record")
    private String checkGoodsRecord;

    /**
     * customer
     */
    @ApiModelProperty("customer")
    @TableField(value="customer")
    private String customer;

    /**
     * goods_bill_code
     */
    @ApiModelProperty("goods_bill_code")
    @TableField(value="goods_bill_code")
    private String goodsBillCode;

    /**
     * receive_goods_date
     */
    @ApiModelProperty("receive_goods_date")
    @TableField(value="receive_goods_date")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date receiveGoodsDate;

    /**
     * receive_goods_person
     */
    @ApiModelProperty("receive_goods_person")
    @TableField(value="receive_goods_person")
    private String receiveGoodsPerson;
}
