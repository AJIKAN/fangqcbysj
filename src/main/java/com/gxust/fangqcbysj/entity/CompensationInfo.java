package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("compensationinfo")
public class CompensationInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * amends
     */
    @ApiModelProperty("amends")
    @TableField(value="amends")
    private Double amends;

    /**
     * amends_time
     */
    @ApiModelProperty("amends_time")
    @TableField(value="amends_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date amendsTime;

    /**
     * bad_destroy_goods
     */
    @ApiModelProperty("bad_destroy_goods")
    @TableField(value="bad_destroy_goods")
    private Double badDestroyGoods;

    /**
     * customer
     */
    @ApiModelProperty("customer")
    @TableField(value="customer")
    private String customer;

    /**
     * receive_station_id
     */
    @ApiModelProperty("receive_station_id")
    @TableField(value="receive_station_id")
    private Integer receiveStationId;

    /**
     * receive_station_name
     */
    @ApiModelProperty("receive_station_name")
    @TableField(value="receive_station_name")
    private String receiveStationName;

    /**
     * remark
     */
    @ApiModelProperty("remark")
    @TableField(value="remark")
    private String remark;

    /**
     * write_date
     */
    @ApiModelProperty("write_date")
    @TableField(value="write_date")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date writeDate;
}
