package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("customeramount")
public class CustomerAmount implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * 发货客户
     */
    @ApiModelProperty("发货客户")
    @TableId(value="send_goods_customer",type = IdType.INPUT)
    private String sendGoodsCustomer;

    /**
     * 运费总计
     */
    @ApiModelProperty("运费总计")
    @TableField(value="carriage_total")
    private Double carriageTotal;

    /**
     * 保险费合计
     */
    @ApiModelProperty("保险费合计")
    @TableField(value="insurance_total")
    private Double insuranceTotal;

    /**
     * 件数总计
     */
    @ApiModelProperty("件数总计")
    @TableField(value="piece_amount_total")
    private Integer pieceAmountTotal;
}
