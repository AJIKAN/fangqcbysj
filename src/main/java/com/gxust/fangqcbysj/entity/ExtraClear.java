package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 杂费结算表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("extraclear")
public class ExtraClear implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id", type = IdType.AUTO)
    private Integer id;

    /**
     * balance_date
     */
    @ApiModelProperty("balance_date")
    @TableField(value="balance_date")
    private Date balanceDate;

    /**
     * balance_money
     */
    @ApiModelProperty("balance_money")
    @TableField(value="balance_money")
    private Double balanceMoney;

    /**
     * balance_type
     */
    @ApiModelProperty("balance_type")
    @TableField(value="balance_type")
    private String balanceType;

    /**
     * remark
     */
    @ApiModelProperty("remark")
    @TableField(value="remark")
    private String remark;

    /**
     * subject_name
     */
    @ApiModelProperty("subject_name")
    @TableField(value="subject_name")
    private String subjectName;
}
