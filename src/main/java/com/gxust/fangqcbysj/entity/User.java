package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author 方全朝
 * @Description 用户表
 * @Date 2021/3/14
 */
@Data
@Accessors(chain = true)
@TableName("user")
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 登录ID
     */
    @TableId(value = "login_id",type = IdType.INPUT)
    @ApiModelProperty(value = "登录id")
    private String loginId;

    /**
     * 密码
     */
    @TableField(value = "password")
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 是否在线：0（未在线）1（在线）
     */
    @TableField(value = "if_online")
    @ApiModelProperty(value = "是否在线")
    private Integer ifOnline;
}
