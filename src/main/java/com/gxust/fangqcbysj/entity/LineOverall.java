package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description 打印专线整体
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("lineoverall")
public class LineOverall implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * 装货地点
     */
    @ApiModelProperty("装货地点")
    @TableField(value="all_carriage_total")
    private Double allCarriageTotal;

    /**
     * 交货地点
     */
    @ApiModelProperty("交货地点")
    @TableField(value="deal_goods_station")
    private String dealGoodsStation;

    /**
     * 总运费合计
     */
    @ApiModelProperty("总运费合计")
    @TableField(value="insurance_total")
    private Double insuranceTotal;

    /**
     * 保险费合计
     */
    @ApiModelProperty("保险费合计")
    @TableField(value="load_station")
    private String loadStation;

    /**
     * 次数
     */
    @ApiModelProperty("次数")
    @TableField(value="times")
    private Integer times;
}
