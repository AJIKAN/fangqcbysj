package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("contactsservice")
public class ContactsService implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 发货客户
     */
    @ApiModelProperty("发货客户")
    @TableId(value="send_goods_customer",type = IdType.INPUT)
    private String sendGoodsCustomer;

    /**
     * 余额
     */
    @ApiModelProperty("余额")
    @TableField(value="balance")
    private Double balance;

    /**
     * 实际总运费
     */
    @ApiModelProperty("实际总运费")
    @TableField(value="bill_money")
    private Double billMoney;

    /**
     * 运费
     */
    @ApiModelProperty("运费")
    @TableField(value="carriage")
    private Double carriage;

    /**
     * 货运单编号
     */
    @ApiModelProperty("货运单编号")
    @TableField(value="goods_bill_code")
    private String goodsBillCode;

    /**
     * 保险
     */
    @ApiModelProperty("保险")
    @TableField(value="insurance")
    private Double insurance;

    /**
     * 应收金额
     */
    @ApiModelProperty("应收金额")
    @TableField(value="money_receivable")
    private Double moneyReceivable;

    /**
     * 收货地址
     */
    @ApiModelProperty("收货地址")
    @TableField(value="receive_goods_addr")
    private String receiveGoodsAddr;

    /**
     * 已收金额
     */
    @ApiModelProperty("已收金额")
    @TableField(value="received_money")
    private Double receivedMoney;

    /**
     * 发货地址
     */
    @ApiModelProperty("发货地址")
    @TableField(value="send_goods_addr")
    private String sendGoodsAddr;

    /**
     * 发货日期
     */
    @ApiModelProperty("发货日期")
    @TableField(value="send_goods_date")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date sendGoodsDate;
}
