package com.gxust.fangqcbysj.entity.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Description:
 * @Author: 方全朝
 * @Date: 2021/5/10
 */
@Data
@Accessors(chain = true)
public class CityExpandDto {

    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * city_id
     */
    @ApiModelProperty("city_id")
    @TableField(value="city_id")
    private Integer cityId;

    @ApiModelProperty("city_id")
    @TableField(value="city_id")
    private String city;

    /**
     * range_city
     */
    @ApiModelProperty("range_city")
    @TableField(value="range_city")
    private String rangeCity;

    @ApiModelProperty("range_city")
    @TableField(value="range_city")
    private String rangeCityName;
}
