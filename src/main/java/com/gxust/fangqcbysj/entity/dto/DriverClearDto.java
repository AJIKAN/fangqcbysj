package com.gxust.fangqcbysj.entity.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/4/2
 */
@Data
@Accessors(chain = true)
public class DriverClearDto {

    private static final long serialVersionUID = 1L;

    /**
     * back_bill_code
     */
    @ApiModelProperty("back_bill_code")
    @TableId(value="back_bill_code")
    private String backBillCode;

    /**
     * add_carriage
     */
    @ApiModelProperty("add_carriage")
    @TableField(value="add_carriage")
    private Double addCarriage;

    /**
     * balance
     */
    @ApiModelProperty("balance")
    @TableField(value="balance")
    private Double balance;

    /**
     * balance_time
     */
    @ApiModelProperty("balance_time")
    @TableField(value="balance_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date balanceTime;

    /**
     * balance_type
     */
    @ApiModelProperty("balance_type")
    @TableField(value="balance_type")
    private String balanceType;

    /**
     * bind_insurance
     */
    @ApiModelProperty("bind_insurance")
    @TableField(value="bind_insurance")
    private Double bindInsurance;

    /**
     * carry_fee
     */
    @ApiModelProperty("carry_fee")
    @TableField(value="carry_fee")
    private Double carryFee;

    /**
     * dispatch_service_fee
     */
    @ApiModelProperty("dispatch_service_fee")
    @TableField(value="dispatch_service_fee")
    private Double dispatchServiceFee;

    /**
     * driver_code
     */
    @ApiModelProperty("driver_code")
    @TableField(value="driver_code")
    private String driverCode;

    /**
     * insurance
     */
    @ApiModelProperty("insurance")
    @TableField(value="insurance")
    private Double insurance;

    /**
     * need_payment
     */
    @ApiModelProperty("need_payment")
    @TableField(value="need_payment")
    private Double needPayment;

    /**
     * payed_money
     */
    @ApiModelProperty("payed_money")
    @TableField(value="payed_money")
    private Double payedMoney;

    /**
     * prepay_money
     */
    @ApiModelProperty("prepay_money")
    @TableField(value="prepay_money")
    private Double prepayMoney;

    private String carryFeeTotal;

    private String addCarriageTotal;

    private String total;
}
