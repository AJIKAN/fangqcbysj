package com.gxust.fangqcbysj.entity.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/4/3
 */
@Data
public class CargoReceiptDto {

    /**
     * goods_revert_bill_code
     */
    @ApiModelProperty("goods_revert_bill_code")
    @TableId(value="goods_revert_bill_code")
    private String goodsRevertBillCode;

    /**
     * accept_station
     */
    @ApiModelProperty("accept_station")
    @TableField(value="accept_station")
    private String acceptStation;

    /**
     * all_carriage
     */
    @ApiModelProperty("all_carriage")
    @TableField(value="all_carriage")
    private Double allCarriage;

    /**
     * arrive_time
     */
    @ApiModelProperty("arrive_time")
    @TableField(value="arrive_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date arriveTime;

    /**
     * back_bill_state
     */
    @ApiModelProperty("back_bill_state")
    @TableField(value="back_bill_state")
    private String backBillState;

    /**
     * carriage_banlance_mode
     */
    @ApiModelProperty("carriage_banlance_mode")
    @TableField(value="carriage_banlance_mode")
    private String carriageBanlanceMode;

    /**
     * carriage_mode
     */
    @ApiModelProperty("carriage_mode")
    @TableField(value="carriage_mode")
    private String carriageMode;

    /**
     * carry_goods_bill_deposit
     */
    @ApiModelProperty("carry_goods_bill_deposit")
    @TableField(value="carry_goods_bill_deposit")
    private Double carryGoodsBillDeposit;

    /**
     * carry_goods_insurance
     */
    @ApiModelProperty("carry_goods_insurance")
    @TableField(value="carry_goods_insurance")
    private Double carryGoodsInsurance;

    /**
     * deal_goods_station
     */
    @ApiModelProperty("deal_goods_station")
    @TableField(value="deal_goods_station")
    private String dealGoodsStation;

    /**
     * dispatch_service_fee
     */
    @ApiModelProperty("dispatch_service_fee")
    @TableField(value="dispatch_service_fee")
    private Double dispatchServiceFee;

    /**
     * driver_id
     */
    @ApiModelProperty("driver_id")
    @TableField(value="driver_id")
    private String driverId;

    /**
     * if_balance
     */
    @ApiModelProperty("if_balance")
    @TableField(value="if_balance")
    private String ifBalance;

    /**
     * insurance
     */
    @ApiModelProperty("insurance")
    @TableField(value="insurance")
    private Double insurance;

    /**
     * linkman_phone
     */
    @ApiModelProperty("linkman_phone")
    @TableField(value="linkman_phone")
    private String linkmanPhone;

    /**
     * load_station
     */
    @ApiModelProperty("load_station")
    @TableField(value="load_station")
    private String loadStation;

    /**
     * receive_goods_detail_addr
     */
    @ApiModelProperty("receive_goods_detail_addr")
    @TableField(value="receive_goods_detail_addr")
    private String receiveGoodsDetailAddr;

    /**
     * receive_goods_linkman
     */
    @ApiModelProperty("receive_goods_linkman")
    @TableField(value="receive_goods_linkman")
    private String receiveGoodsLinkman;

    /**
     * remark
     */
    @ApiModelProperty("remark")
    @TableField(value="remark")
    private String remark;

    /**
     * sign_time
     */
    @ApiModelProperty("sign_time")
    @TableField(value="sign_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date signTime;

    /**
     * start_advance
     */
    @ApiModelProperty("start_advance")
    @TableField(value="start_advance")
    private Double startAdvance;

    /**
     * start_carry_time
     */
    @ApiModelProperty("start_carry_time")
    @TableField(value="start_carry_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date startCarryTime;

    private String allCarriageTotal;

    private String insuranceTotal;

    private String times;
}
