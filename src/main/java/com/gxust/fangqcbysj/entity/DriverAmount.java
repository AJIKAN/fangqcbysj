package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description 司机运量
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("driveramount")
public class DriverAmount implements Serializable {
    
    /**
     * driver_code
     */
    @ApiModelProperty("driver_code")
    @TableId(value="driver_code",type = IdType.INPUT)
    private String driverCode;

    /**
     * 承运费合计
     */
    @ApiModelProperty("承运费合计")
    @TableField(value="add_carriage_total")
    private Double addCarriageTotal;

    /**
     * 加运费合计
     */
    @ApiModelProperty("加运费合计")
    @TableField(value="carry_fee_total")
    private Double carryFeeTotal;

    /**
     * 总计
     */
    @ApiModelProperty("总计")
    @TableField(value="total")
    private Double total;
}
