package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 货运单主表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("goodsbill")
public class GoodsBill implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * goods_bill_code
     */
    @ApiModelProperty("goods_bill_code")
    @TableId(value="goods_bill_code",type = IdType.INPUT)
    private String goodsBillCode;

    /**
     * accept_procedure_rate
     */
    @ApiModelProperty("accept_procedure_rate")
    @TableField(value="accept_procedure_rate")
    private String acceptProcedureRate;

    /**
     * accept_station
     */
    @ApiModelProperty("accept_station")
    @TableField(value="accept_station")
    private String acceptStation;

    /**
     * carriage
     */
    @ApiModelProperty("carriage")
    @TableField(value="carriage")
    private Double carriage;

    /**
     * carry_goods_fee
     */
    @ApiModelProperty("carry_goods_fee")
    @TableField(value="carry_goods_fee")
    private Double carryGoodsFee;

    /**
     * employee_code
     */
    @ApiModelProperty("employee_code")
    @TableField(value="employee_code")
    private String employeeCode;

    /**
     * fact_deal_date
     */
    @ApiModelProperty("fact_deal_date")
    @TableField(value="fact_deal_date")
    private Date factDealDate;

    /**
     * fetch_goods_mode
     */
    @ApiModelProperty("fetch_goods_mode")
    @TableField(value="fetch_goods_mode")
    private String fetchGoodsMode;

    /**
     * help_accept_payment
     */
    @ApiModelProperty("help_accept_payment")
    @TableField(value="help_accept_payment")
    private Double helpAcceptPayment;

    /**
     * if_audit
     */
    @ApiModelProperty("if_audit")
    @TableField(value="if_audit")
    private String ifAudit;

    /**
     * if_settle_accounts
     */
    @ApiModelProperty("if_settle_accounts")
    @TableField(value="if_settle_accounts")
    private String ifSettleAccounts;

    /**
     * insurance
     */
    @ApiModelProperty("insurance")
    @TableField(value="insurance")
    private Double insurance;

    /**
     * money_of_change_pay
     */
    @ApiModelProperty("money_of_change_pay")
    @TableField(value="money_of_change_pay")
    private Double moneyOfChangePay;

    /**
     * pay_kickback
     */
    @ApiModelProperty("pay_kickback")
    @TableField(value="pay_kickback")
    private Double payKickback;

    /**
     * pay_mode
     */
    @ApiModelProperty("pay_mode")
    @TableField(value="pay_mode")
    private String payMode;

    /**
     * predelivery_date
     */
    @ApiModelProperty("predelivery_date")
    @TableField(value="predelivery_date")
    private Date predeliveryDate;

    /**
     * receive_goods_addr
     */
    @ApiModelProperty("receive_goods_addr")
    @TableField(value="receive_goods_addr")
    private String receiveGoodsAddr;

    /**
     * receive_goods_customer
     */
    @ApiModelProperty("receive_goods_customer")
    @TableField(value="receive_goods_customer")
    private String receiveGoodsCustomer;

    /**
     * receive_goods_customer_addr
     */
    @ApiModelProperty("receive_goods_customer_addr")
    @TableField(value="receive_goods_customer_addr")
    private String receiveGoodsCustomerAddr;

    /**
     * receive_goods_customer_code
     */
    @ApiModelProperty("receive_goods_customer_code")
    @TableField(value="receive_goods_customer_code")
    private String receiveGoodsCustomerCode;

    /**
     * receive_goods_customer_tel
     */
    @ApiModelProperty("receive_goods_customer_tel")
    @TableField(value="receive_goods_customer_tel")
    private String receiveGoodsCustomerTel;

    /**
     * reduce_fund
     */
    @ApiModelProperty("reduce_fund")
    @TableField(value="reduce_fund")
    private Double reduceFund;

    /**
     * remark
     */
    @ApiModelProperty("remark")
    @TableField(value="remark")
    private String remark;

    /**
     * send_goods_addr
     */
    @ApiModelProperty("send_goods_addr")
    @TableField(value="send_goods_addr")
    private String sendGoodsAddr;

    /**
     * send_goods_customer
     */
    @ApiModelProperty("send_goods_customer")
    @TableField(value="send_goods_customer")
    private String sendGoodsCustomer;

    /**
     * send_goods_customer_addr
     */
    @ApiModelProperty("send_goods_customer_addr")
    @TableField(value="send_goods_customer_addr")
    private String sendGoodsCustomerAddr;

    /**
     * send_goods_customer_no
     */
    @ApiModelProperty("send_goods_customer_no")
    @TableField(value="send_goods_customer_no")
    private String sendGoodsCustomerNo;

    /**
     * send_goods_customer_tel
     */
    @ApiModelProperty("send_goods_customer_tel")
    @TableField(value="send_goods_customer_tel")
    private String sendGoodsCustomerTel;

    /**
     * send_goods_date
     */
    @ApiModelProperty("send_goods_date")
    @TableField(value="send_goods_date")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date sendGoodsDate;

    /**
     * transfer_fee
     */
    @ApiModelProperty("transfer_fee")
    @TableField(value="transfer_fee")
    private Double transferFee;

    /**
     * transfer_station
     */
    @ApiModelProperty("transfer_station")
    @TableField(value="transfer_station")
    private String transferStation;

    /**
     * validity
     */
    @ApiModelProperty("validity")
    @TableField(value="validity")
    private String validity;

    /**
     * write_bill_person
     */
    @ApiModelProperty("write_bill_person")
    @TableField(value="write_bill_person")
    private String writeBillPerson;

    /**
     * write_date
     */
    @ApiModelProperty("write_date")
    @TableField(value="write_date")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date writeDate;
}
