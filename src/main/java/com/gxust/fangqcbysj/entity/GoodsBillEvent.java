package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gxust.fangqcbysj.utils.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 货运单事件表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("goodsbillevent")
public class GoodsBillEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * goods_bill_id
     */
    @ApiModelProperty("goods_bill_id")
    @Excel(name = "货运单编号")
    @TableId(value="goods_bill_id",type = IdType.INPUT)
    private String goodsBillId;

    /**
     * event_name
     */
    @ApiModelProperty("event_name")
    @Excel(name = "事件名称")
    @TableField(value="event_name")
    private String eventName;

    /**
     * occur_time
     */
    @ApiModelProperty("occur_time")
    @TableField(value="occur_time")
    @Excel(name = "发生时间",dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date occurTime;

    /**
     * remark
     */
    @ApiModelProperty("remark")
    @TableField(value="remark")
    @Excel(name = "备注")
    private String remark;
}
