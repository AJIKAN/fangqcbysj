package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description 装货发车清单
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("sentlist")
public class SentList implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * 序号
     */
    @ApiModelProperty("序号")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * 回结
     */
    @ApiModelProperty("回结")
    @TableField(value="back_cost")
    private Double backCost;

    /**
     * 车牌
     */
    @ApiModelProperty("车牌")
    @TableField(value="car_card_no")
    private String carCardNo;

    /**
     * 现付
     */
    @ApiModelProperty("现付")
    @TableField(value="cash_pay")
    private Double cashPay;

    /**
     * 司机名称
     */
    @ApiModelProperty("司机名称")
    @TableField(value="driver_name")
    private String driverName;

    /**
     * 货物编号
     */
    @ApiModelProperty("货物编号")
    @TableField(value="goods_code")
    private String goodsCode;

    /**
     * 货物名称
     */
    @ApiModelProperty("货物名称")
    @TableField(value="goods_name")
    private String goodsName;

    /**
     * 货运回执单编号
     */
    @ApiModelProperty("货运回执单编号")
    @TableField(value="goods_revert_bill_code")
    private String goodsRevertBillCode;

    /**
     * 代收
     */
    @ApiModelProperty("代收")
    @TableField(value="help_accept_fund")
    private String helpAcceptFund;

    /**
     * 手机
     */
    @ApiModelProperty("手机")
    @TableField(value="mobile")
    private String mobile;

    /**
     * 提付
     */
    @ApiModelProperty("提付")
    @TableField(value="pickup_pay")
    private Double pickupPay;

    /**
     * 件数
     */
    @ApiModelProperty("件数")
    @TableField(value="piece_amount")
    private Integer pieceAmount;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    @TableField(value="remark")
    private String remark;

    /**
     * 发货客户
     */
    @ApiModelProperty("发货客户")
    @TableField(value="send_goods_customer")
    private String sendGoodsCustomer;

    /**
     * 发货客户电话
     */
    @ApiModelProperty("发货客户电话")
    @TableField(value="send_goods_customer_tel")
    private String sendGoodsCustomerTel;

    /**
     * 中转目的
     */
    @ApiModelProperty("中转目的")
    @TableField(value="transfer_destination")
    private String transferDestination;

    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    @TableField(value="user_id")
    private Integer userId;
}
