package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gxust.fangqcbysj.utils.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 单据明细表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("billinfo")
public class BillInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    @Excel(name = "ID")
    private Integer id;

    /**
     * accept_station
     */
    @ApiModelProperty("accept_station")
    @TableField(value="accept_station",insertStrategy = FieldStrategy.IGNORED)
    private String acceptStation;

    /**
     * bill_code
     */
    @ApiModelProperty("bill_code")
    @Excel(name = "单据编号")
    @TableField(value="bill_code",insertStrategy = FieldStrategy.IGNORED)
    private String billCode;

    /**
     * bill_state
     */
    @ApiModelProperty("bill_state")
    @TableField(value="bill_state",insertStrategy = FieldStrategy.IGNORED)
    private String billState;

    /**
     * bill_type
     */
    @ApiModelProperty("bill_type")
    @Excel(name = "单据类型")
    @TableField(value="bill_type",insertStrategy = FieldStrategy.IGNORED)
    private String billType;

    /**
     * write_date
     */
    @ApiModelProperty("write_date")
    @Excel(name = "填写日期",dateFormat = "yyyy-MM-dd")
    @TableField(value="write_date",insertStrategy = FieldStrategy.IGNORED)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date writeDate;

}
