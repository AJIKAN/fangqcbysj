package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("carcost")
public class CarCost implements Serializable {
    
    private static final long serialVersionUID = 1L;

    /**
     * 司机名称
     */
    @ApiModelProperty("司机名称")
    @TableId(value="driver_code",type = IdType.INPUT)
    private String driverCode;

    /**
     * 加运费合计
     */
    @ApiModelProperty("加运费合计")
    @TableField(value="add_carriage_total")
    private Double addCarriageTotal;

    /**
     * 准载重量
     */
    @ApiModelProperty("准载重量")
    @TableField(value="allow_carry_weight")
    private Double allowCarryWeight;

    /**
     * 回执单编号
     */
    @ApiModelProperty("回执单编号")
    @TableField(value="back_bill_code")
    private String backBillCode;

    /**
     * 结算时间
     */
    @ApiModelProperty("结算时间")
    @TableField(value="balance_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date balanceTime;

    /**
     * 车号
     */
    @ApiModelProperty("车号")
    @TableField(value="car_no")
    private String carNo;

    /**
     * 车型
     */
    @ApiModelProperty("车型")
    @TableField(value="car_type")
    private String carType;

    /**
     * 车厢宽度
     */
    @ApiModelProperty("车厢宽度")
    @TableField(value="car_width")
    private String carWidth;

    /**
     * 承运费合计
     */
    @ApiModelProperty("承运费合计")
    @TableField(value="carry_fee_total")
    private Double carryFeeTotal;

    /**
     * 交货地点
     */
    @ApiModelProperty("交货地点")
    @TableField(value="deal_goods_station")
    private String dealGoodsStation;

    /**
     * 实际总运费
     */
    @ApiModelProperty("实际总运费")
    @TableField(value="fact_carriage_total")
    private Double factCarriageTotal;

    /**
     * 载物高度
     */
    @ApiModelProperty("载物高度")
    @TableField(value="goods_height")
    private String goodsHeight;

    /**
     * 装货地点
     */
    @ApiModelProperty("装货地点")
    @TableField(value="load_station")
    private String loadStation;
}
