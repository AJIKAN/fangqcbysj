package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("extraincome")
public class ExtraIncome implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id", type = IdType.AUTO)
    private Integer id;

    /**
     * income_month
     */
    @ApiModelProperty("income_month")
    @TableField(value="income_month")
    private String incomeMonth;

    /**
     * money
     */
    @ApiModelProperty("money")
    @TableField(value="money")
    private Double money;

    /**
     * name
     */
    @ApiModelProperty("name")
    @TableField(value="name")
    private String name;

    /**
     * write_date
     */
    @ApiModelProperty("write_date")
    @TableField(value="write_date")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date writeDate;
}
