package com.gxust.fangqcbysj.entity.Vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/4/1
 */
@Data
@Accessors(chain = true)
public class CustomerReceiptInfoVo {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
    @TableField(value="id")
    private Integer id;

    /**
     * carry_bill_event_id
     */
    @ApiModelProperty("carry_bill_event_id")
    @TableField(value="carry_bill_event_id")
    private Integer carryBillEventId;

    /**
     * check_goods_record
     */
    @ApiModelProperty("check_goods_record")
    @TableField(value="check_goods_record")
    private String checkGoodsRecord;

    /**
     * customer
     */
    @ApiModelProperty("customer")
    @TableField(value="customer")
    private String customer;

    /**
     * goods_bill_code
     */
    @ApiModelProperty("goods_bill_code")
    @TableField(value="goods_bill_code")
    private String goodsBillCode;

    /**
     * receive_goods_date
     */
    @ApiModelProperty("receive_goods_date")
    @TableField(value="receive_goods_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String receiveGoodsDate;

    /**
     * receive_goods_person
     */
    @ApiModelProperty("receive_goods_person")
    @TableField(value="receive_goods_person")
    private String receiveGoodsPerson;
}
