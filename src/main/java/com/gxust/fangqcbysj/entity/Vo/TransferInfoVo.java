package com.gxust.fangqcbysj.entity.Vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/4/1
 */
@Data
@Accessors(chain = true)
public class TransferInfoVo {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id")
    private Integer id;

    /**
     * after_transfer_bill
     */
    @ApiModelProperty("after_transfer_bill")
    @TableField(value="after_transfer_bill")
    private String afterTransferBill;

    /**
     * check_time
     */
    @ApiModelProperty("check_time")
    @TableField(value="check_time")
    private String checkTime;

    /**
     * description
     */
    @ApiModelProperty("description")
    @TableField(value="description")
    private String description;

    /**
     * goods_bill_code
     */
    @ApiModelProperty("goods_bill_code")
    @TableField(value="goods_bill_code")
    private String goodsBillCode;

    /**
     * transfer_addr
     */
    @ApiModelProperty("transfer_addr")
    @TableField(value="transfer_addr")
    private String transferAddr;

    /**
     * transfer_check
     */
    @ApiModelProperty("transfer_check")
    @TableField(value="transfer_check")
    private String transferCheck;

    /**
     * transfer_company
     */
    @ApiModelProperty("transfer_company")
    @TableField(value="transfer_company")
    private String transferCompany;

    /**
     * transfer_fee
     */
    @ApiModelProperty("transfer_fee")
    @TableField(value="transfer_fee")
    private Double transferFee;

    /**
     * transfer_station
     */
    @ApiModelProperty("transfer_station")
    @TableField(value="transfer_station")
    private String transferStation;

    /**
     * transfer_station_tel
     */
    @ApiModelProperty("transfer_station_tel")
    @TableField(value="transfer_station_tel")
    private String transferStationTel;
}
