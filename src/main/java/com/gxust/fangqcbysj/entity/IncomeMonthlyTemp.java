package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description 损益月报临时报表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("incomemonthlytemp")
public class IncomeMonthlyTemp implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * 经营费用
     */
    @ApiModelProperty("经营费用")
    @TableField(value="biz_fee")
    private Double bizFee;

    /**
     * 营业收入
     */
    @ApiModelProperty("营业收入")
    @TableField(value="biz_income")
    private Double bizIncome;

    /**
     * 车运
     */
    @ApiModelProperty("车运")
    @TableField(value="car_carriage")
    private Double carCarriage;

    /**
     * 运费金额
     */
    @ApiModelProperty("运费金额")
    @TableField(value="carriage_money")
    private Double carriageMoney;

    /**
     * 搬运工资
     */
    @ApiModelProperty("搬运工资")
    @TableField(value="convey_wage")
    private Double conveyWage;

    /**
     * 财务费用
     */
    @ApiModelProperty("财务费用")
    @TableField(value="finance_fee")
    private Double financeFee;

    /**
     * 房租
     */
    @ApiModelProperty("房租")
    @TableField(value="house_rent")
    private Double houseRent;

    /**
     * 收入
     */
    @ApiModelProperty("收入")
    @TableField(value="income")
    private Double income;

    /**
     * 保险金额
     */
    @ApiModelProperty("保险金额")
    @TableField(value="insurance_money")
    private Double insuranceMoney;

    /**
     * 管理费用
     */
    @ApiModelProperty("管理费用")
    @TableField(value="manage_fee")
    private Double manageFee;

    /**
     * 月份
     */
    @ApiModelProperty("月份")
    @TableField(value="month")
    private String month;

    /**
     * 办公费用
     */
    @ApiModelProperty("办公费用")
    @TableField(value="office_fee")
    private Double officeFee;

    /**
     * 其他
     */
    @ApiModelProperty("其他")
    @TableField(value="other")
    private Double other;

    /**
     * 支出
     */
    @ApiModelProperty("支出")
    @TableField(value="payout")
    private Double payout;

    /**
     * 电话
     */
    @ApiModelProperty("电话")
    @TableField(value="phone_fee")
    private Double phoneFee;

    /**
     * 利润
     */
    @ApiModelProperty("利润")
    @TableField(value="profit")
    private Double profit;

    /**
     * 非营业收入
     */
    @ApiModelProperty("非营业收入")
    @TableField(value="unbiz_income")
    private Double unbizIncome;

    /**
     * 工资
     */
    @ApiModelProperty("工资")
    @TableField(value="wage")
    private Double wage;

    /**
     * 水电
     */
    @ApiModelProperty("水电")
    @TableField(value="water_elec_fee")
    private Double waterElecFee;
}
