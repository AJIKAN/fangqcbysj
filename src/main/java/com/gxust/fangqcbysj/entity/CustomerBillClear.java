package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gxust.fangqcbysj.utils.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("customerbillclear")
public class CustomerBillClear implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * goods_bill_code
     */
    @ApiModelProperty("goods_bill_code")
    @TableId(value="goods_bill_code",type = IdType.INPUT)
    @Excel(name = "货运单编号")
    private String goodsBillCode;

    /**
     * customer_code
     */
    @ApiModelProperty("customer_code")
    @Excel(name = "客户编号")
    @TableField(value="customer_code")
    private String customerCode;

    /**
     * balance
     */
    @ApiModelProperty("balance")
    @TableField(value="balance")
    private Double balance;

    /**
     * balance_time
     */
    @ApiModelProperty("balance_time")
    @TableField(value="balance_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date balanceTime;

    /**
     * balance_type
     */
    @ApiModelProperty("balance_type")
    @TableField(value="balance_type")
    @Excel(name = "结算类型")
    private String balanceType;

    /**
     * bill_money
     */
    @ApiModelProperty("bill_money")
    @TableField(value="bill_money")
    private Double billMoney;

    /**
     * carriage_reduce_fund
     */
    @ApiModelProperty("carriage_reduce_fund")
    @TableField(value="carriage_reduce_fund")
    private Double carriageReduceFund;

    /**
     * carry_goods_fee
     */
    @ApiModelProperty("carry_goods_fee")
    @TableField(value="carry_goods_fee")
    private Double carryGoodsFee;

    /**
     * insurance
     */
    @ApiModelProperty("insurance")
    @TableField(value="insurance")
    private Double insurance;

    /**
     * money_receivable
     */
    @ApiModelProperty("money_receivable")
    @TableField(value="money_receivable")
    private Double moneyReceivable;

    /**
     * pay_kickback
     */
    @ApiModelProperty("pay_kickback")
    @TableField(value="pay_kickback")
    private Double payKickback;

    /**
     * prepay_money
     */
    @ApiModelProperty("prepay_money")
    @TableField(value="prepay_money")
    private Double prepayMoney;

    /**
     * received_money
     */
    @ApiModelProperty("received_money")
    @TableField(value="received_money")
    private Double receivedMoney;
}
