package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 职员信息表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("employee")
public class Employee implements Serializable {
    private static final long serialVersionUID = 1L;
    
    /**
     * employee_code
     */
    @ApiModelProperty("employee_code")
    @TableId(value="employee_code",type = IdType.INPUT)
    private String employeeCode;

    /**
     * birthday
     */
    @ApiModelProperty("birthday")
    @TableField(value="birthday")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date birthday;

    /**
     * department
     */
    @ApiModelProperty("department")
    @TableField(value="department")
    private String department;

    /**
     * employee_name
     */
    @ApiModelProperty("employee_name")
    @TableField(value="employee_name")
    private String employeeName;

    /**
     * gender
     */
    @ApiModelProperty("gender")
    @TableField(value="gender")
    private String gender;

    /**
     * position
     */
    @ApiModelProperty("position")
    @TableField(value="position")
    private String position;
}
