package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 代收货款结算表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("proxyfeeclear")
public class ProxyFeeClear implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * goods_bill_code
     */
    @ApiModelProperty("goods_bill_code")
    @TableId(value="goods_bill_code",type = IdType.INPUT)
    private String goodsBillCode;

    /**
     * account_receivable
     */
    @ApiModelProperty("account_receivable")
    @TableField(value="account_receivable")
    private Double accountReceivable;

    /**
     * balance_date
     */
    @ApiModelProperty("balance_date")
    @TableField(value="balance_date")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date balanceDate;

    /**
     * commision_rate
     */
    @ApiModelProperty("commision_rate")
    @TableField(value="commision_rate")
    private Float commisionRate;

    /**
     * commision_receivable
     */
    @ApiModelProperty("commision_receivable")
    @TableField(value="commision_receivable")
    private Double commisionReceivable;

    /**
     * customer_code
     */
    @ApiModelProperty("customer_code")
    @TableField(value="customer_code")
    private String customerCode;

    /**
     * fact_receive_fund
     */
    @ApiModelProperty("fact_receive_fund")
    @TableField(value="fact_receive_fund")
    private Double factReceiveFund;

    /**
     * goods_pay_change
     */
    @ApiModelProperty("goods_pay_change")
    @TableField(value="goods_pay_change")
    private Double goodsPayChange;

    /**
     * received_commision
     */
    @ApiModelProperty("received_commision")
    @TableField(value="received_commision")
    private Double receivedCommision;
}
