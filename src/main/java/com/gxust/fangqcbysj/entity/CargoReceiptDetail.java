package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("cargoreceiptdetail")
public class CargoReceiptDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * goods_revert_bill_id
     */
    @ApiModelProperty("goods_revert_bill_id")
    @TableId(value="goods_revert_bill_id",type = IdType.INPUT)
    private String goodsRevertBillId;

    /**
     * goods_bill_detail_id
     */
    @ApiModelProperty("goods_bill_detail_id")
    @TableField(value="goods_bill_detail_id")
    private String goodsBillDetailId;

    /**
     * goods_value
     */
    @ApiModelProperty("goods_value")
    @TableField(value="goods_value")
    private Double goodsValue;

    /**
     * piece_amount
     */
    @ApiModelProperty("piece_amount")
    @TableField(value="piece_amount")
    private Integer pieceAmount;

    /**
     * price_mode
     */
    @ApiModelProperty("price_mode")
    @TableField(value="price_mode")
    private String priceMode;

    /**
     * price_standard
     */
    @ApiModelProperty("price_standard")
    @TableField(value="price_standard")
    private String priceStandard;

    /**
     * volume
     */
    @ApiModelProperty("volume")
    @TableField(value="volume")
    private Double volume;

    /**
     * weight
     */
    @ApiModelProperty("weight")
    @TableField(value="weight")
    private Double weight;
}
