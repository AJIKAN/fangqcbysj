package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 财务费用表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("financefee")
public class FinanceFee implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id", type = IdType.AUTO)
    private Integer id;

    /**
     * fee
     */
    @ApiModelProperty("fee")
    @TableField(value="fee")
    private Double fee;

    /**
     * payout_month
     */
    @ApiModelProperty("payout_month")
    @TableField(value="payout_month")
    private String payoutMonth;

    /**
     * write_date
     */
    @ApiModelProperty("write_date")
    @TableField(value="write_date")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date writeDate;
}
