package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("billrelease")
public class BillRelease implements Serializable {
    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * accept_station
     */
    @ApiModelProperty("accept_station")
    @TableField(value="accept_station")
    private String acceptStation;

    /**
     * bill_code
     */
    @ApiModelProperty("bill_code")
    @TableField(value="bill_code")
    private String billCode;

    /**
     * bill_type
     */
    @ApiModelProperty("bill_type")
    @TableField(value="bill_type")
    private String billType;

    /**
     * receive_bill_person
     */
    @ApiModelProperty("receive_bill_person")
    @TableField(value="receive_bill_person")
    private String receiveBillPerson;

    /**
     * receive_bill_time
     */
    @ApiModelProperty("receive_bill_time")
    @TableField(value="receive_bill_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date receiveBillTime;

    /**
     * release_person
     */
    @ApiModelProperty("release_person")
    @TableField(value="release_person")
    private String releasePerson;
}
