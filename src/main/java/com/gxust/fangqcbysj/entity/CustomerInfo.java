package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description 客户信息表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("customerinfo")
public class CustomerInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * customer_code
     */
    @ApiModelProperty("customer_code")
    @TableId(value="customer_code",type = IdType.INPUT)
    private String customerCode;

    /**
     * address
     */
    @ApiModelProperty("address")
    @TableField(value="address")
    private String address;

    /**
     * customer
     */
    @ApiModelProperty("customer")
    @TableField(value="customer")
    private String customer;

    /**
     * customer_type
     */
    @ApiModelProperty("customer_type")
    @TableField(value="customer_type")
    private String customerType;

    /**
     * email
     */
    @ApiModelProperty("email")
    @TableField(value="email")
    private String email;

    /**
     * enterprise_property
     */
    @ApiModelProperty("enterprise_property")
    @TableField(value="enterprise_property")
    private String enterpriseProperty;

    /**
     * enterprise_size
     */
    @ApiModelProperty("enterprise_size")
    @TableField(value="enterprise_size")
    private String enterpriseSize;

    /**
     * fax
     */
    @ApiModelProperty("fax")
    @TableField(value="fax")
    private String fax;

    /**
     * linkman
     */
    @ApiModelProperty("linkman")
    @TableField(value="linkman")
    private String linkman;

    /**
     * linkman_mobile
     */
    @ApiModelProperty("linkman_mobile")
    @TableField(value="linkman_mobile")
    private String linkmanMobile;

    /**
     * phone
     */
    @ApiModelProperty("phone")
    @TableField(value="phone")
    private String phone;

    /**
     * post_code
     */
    @ApiModelProperty("post_code")
    @TableField(value="post_code")
    private String postCode;
}
