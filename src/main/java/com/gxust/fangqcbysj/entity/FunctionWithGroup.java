package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description 功能与组表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName(value = "functionwithgroup")
public class FunctionWithGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 功能id
     */
    @ApiModelProperty("功能id")
    @TableField(value = "function_id")
    private Integer functionId;

    /**
     * 组id
     */
    @ApiModelProperty("组id")
    @TableField(value = "group_id")
    private Integer groupId;
}