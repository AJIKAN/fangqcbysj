package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;


import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 管理费用表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("managefee")
public class ManageFee implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * house_rent
     */
    @ApiModelProperty("house_rent")
    @TableField(value="house_rent")
    private Double houseRent;

    /**
     * office_fee
     */
    @ApiModelProperty("office_fee")
    @TableField(value="office_fee")
    private Double officeFee;

    /**
     * other_payout
     */
    @ApiModelProperty("other_payout")
    @TableField(value="other_payout")
    private Double otherPayout;

    /**
     * payout_month
     */
    @ApiModelProperty("payout_month")
    @TableField(value="payout_month")
    private String payoutMonth;

    /**
     * phone_fee
     */
    @ApiModelProperty("phone_fee")
    @TableField(value="phone_fee")
    private Double phoneFee;

    /**
     * water_elec_fee
     */
    @ApiModelProperty("water_elec_fee")
    @TableField(value="water_elec_fee")
    private Double waterElecFee;

    /**
     * write_date
     */
    @ApiModelProperty("write_date")
    @TableField(value="write_date")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date writeDate;
}
