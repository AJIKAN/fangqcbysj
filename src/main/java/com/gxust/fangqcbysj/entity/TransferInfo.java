package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gxust.fangqcbysj.utils.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 中转信息表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("transferinfo")
public class TransferInfo implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * after_transfer_bill
     */
    @ApiModelProperty("after_transfer_bill")
    @TableField(value="after_transfer_bill")
    private String afterTransferBill;

    /**
     * check_time
     */
    @ApiModelProperty("check_time")
    @TableField(value="check_time")
    private Date checkTime;

    /**
     * description
     */
    @ApiModelProperty("description")
    @TableField(value="description")
    private String description;

    /**
     * goods_bill_code
     */
    @ApiModelProperty("goods_bill_code")
    @TableField(value="goods_bill_code")
    @Excel(name = "货运单编号")
    private String goodsBillCode;

    /**
     * transfer_addr
     */
    @ApiModelProperty("transfer_addr")
    @TableField(value="transfer_addr")
    private String transferAddr;

    /**
     * transfer_check
     */
    @ApiModelProperty("transfer_check")
    @TableField(value="transfer_check")
    @Excel(name = "验货人")
    private String transferCheck;

    /**
     * transfer_company
     */
    @ApiModelProperty("transfer_company")
    @TableField(value="transfer_company")
    @Excel(name = "中转公司")
    private String transferCompany;

    /**
     * transfer_fee
     */
    @ApiModelProperty("transfer_fee")
    @TableField(value="transfer_fee")
    private Double transferFee;

    /**
     * transfer_station
     */
    @ApiModelProperty("transfer_station")
    @TableField(value="transfer_station")
    @Excel(name = "中转地")
    private String transferStation;

    /**
     * transfer_station_tel
     */
    @ApiModelProperty("transfer_station_tel")
    @TableField(value="transfer_station_tel")
    private String transferStationTel;
}
