package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gxust.fangqcbysj.utils.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("cargoreceipt")
public class CargoReceipt implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * goods_revert_bill_code
     */
    @ApiModelProperty("goods_revert_bill_code")
    @Excel(name = "货运单回执单编号")
    @TableId(value="goods_revert_bill_code",type = IdType.INPUT)
    private String goodsRevertBillCode;

    /**
     * accept_station
     */
    @ApiModelProperty("accept_station")
    @TableField(value="accept_station")
    private String acceptStation;

    /**
     * all_carriage
     */
    @ApiModelProperty("all_carriage")
    @TableField(value="all_carriage")
    private Double allCarriage;

    /**
     * arrive_time
     */
    @ApiModelProperty("arrive_time")
    @TableField(value="arrive_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date arriveTime;

    /**
     * back_bill_state
     */
    @ApiModelProperty("back_bill_state")
    @TableField(value="back_bill_state")
    @Excel(name = "回执单状态")
    private String backBillState;

    /**
     * carriage_banlance_mode
     */
    @ApiModelProperty("carriage_banlance_mode")
    @TableField(value="carriage_banlance_mode")
    private String carriageBanlanceMode;

    /**
     * carriage_mode
     */
    @ApiModelProperty("carriage_mode")
    @TableField(value="carriage_mode")
    private String carriageMode;

    /**
     * carry_goods_bill_deposit
     */
    @ApiModelProperty("carry_goods_bill_deposit")
    @TableField(value="carry_goods_bill_deposit")
    private Double carryGoodsBillDeposit;

    /**
     * carry_goods_insurance
     */
    @ApiModelProperty("carry_goods_insurance")
    @TableField(value="carry_goods_insurance")
    private Double carryGoodsInsurance;

    /**
     * deal_goods_station
     */
    @ApiModelProperty("deal_goods_station")
    @TableField(value="deal_goods_station")
    private String dealGoodsStation;

    /**
     * dispatch_service_fee
     */
    @ApiModelProperty("dispatch_service_fee")
    @TableField(value="dispatch_service_fee")
    private Double dispatchServiceFee;

    /**
     * driver_id
     */
    @ApiModelProperty("driver_id")
    @TableField(value="driver_id")
    private String driverId;

    /**
     * if_balance
     */
    @ApiModelProperty("if_balance")
    @TableField(value="if_balance")
    private String ifBalance;

    /**
     * insurance
     */
    @ApiModelProperty("insurance")
    @TableField(value="insurance")
    private Double insurance;

    /**
     * linkman_phone
     */
    @ApiModelProperty("linkman_phone")
    @TableField(value="linkman_phone")
    private String linkmanPhone;

    /**
     * load_station
     */
    @ApiModelProperty("load_station")
    @TableField(value="load_station")
    private String loadStation;

    /**
     * receive_goods_detail_addr
     */
    @ApiModelProperty("receive_goods_detail_addr")
    @TableField(value="receive_goods_detail_addr")
    private String receiveGoodsDetailAddr;

    /**
     * receive_goods_linkman
     */
    @ApiModelProperty("receive_goods_linkman")
    @TableField(value="receive_goods_linkman")
    @Excel(name = "收货联系人")
    private String receiveGoodsLinkman;

    /**
     * remark
     */
    @ApiModelProperty("remark")
    @TableField(value="remark")
    private String remark;

    /**
     * sign_time
     */
    @ApiModelProperty("sign_time")
    @TableField(value="sign_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date signTime;

    /**
     * start_advance
     */
    @ApiModelProperty("start_advance")
    @TableField(value="start_advance")
    private Double startAdvance;

    /**
     * start_carry_time
     */
    @ApiModelProperty("start_carry_time")
    @TableField(value="start_carry_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date startCarryTime;
}
