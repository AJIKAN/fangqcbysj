package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gxust.fangqcbysj.entity.Vo.PageListVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description 中转公司信息表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("transfercominfo")
public class TransferComInfo implements Serializable{

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * city
     */
    @ApiModelProperty("city")
    @TableField(value="city")
    private String city;

    /**
     * company_name
     */
    @ApiModelProperty("company_name")
    @TableField(value="company_name")
    private String companyName;

    /**
     * detail_address
     */
    @ApiModelProperty("detail_address")
    @TableField(value="detail_address")
    private String detailAddress;

    /**
     * link_phone
     */
    @ApiModelProperty("link_phone")
    @TableField(value="link_phone")
    private String linkPhone;
}
