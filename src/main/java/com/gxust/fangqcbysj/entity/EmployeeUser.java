package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description 职员用户关系
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("employeeuser")
public class EmployeeUser implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id", type = IdType.AUTO)
    private Integer id;

    /**
     * 职员id
     */
    @ApiModelProperty("职员id")
    @TableField(value="employee_id")
    private Integer employeeId;

    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    @TableField(value="user_id")
    private Integer userId;
}
