package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 货物回执信息表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("goodsreceiptinfo")
public class GoodsReceiptInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * check_goods_record
     */
    @ApiModelProperty("check_goods_record")
    @TableField(value="check_goods_record")
    private String checkGoodsRecord;

    /**
     * driver_name
     */
    @ApiModelProperty("driver_name")
    @TableField(value="driver_name")
    private String driverName;

    /**
     * goods_revert_code
     */
    @ApiModelProperty("goods_revert_code")
    @TableField(value="goods_revert_code")
    private String goodsRevertCode;

    /**
     * rceive_goods_date
     */
    @ApiModelProperty("rceive_goods_date")
    @TableField(value="rceive_goods_date")
    private Date rceiveGoodsDate;

    /**
     * receive_goods_person
     */
    @ApiModelProperty("receive_goods_person")
    @TableField(value="receive_goods_person")
    private String receiveGoodsPerson;
}
