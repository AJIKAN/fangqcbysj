package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description 投诉信息表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("complaintinfo")
public class ComplaintInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * appeal_content
     */
    @ApiModelProperty("appeal_content")
    @TableField(value="appeal_content")
    private String appealContent;

    /**
     * appeal_date
     */
    @ApiModelProperty("appeal_date")
    @TableField(value="appeal_date")
    private String appealDate;

    /**
     * call_back_date
     */
    @ApiModelProperty("call_back_date")
    @TableField(value="call_back_date")
    private String callBackDate;

    /**
     * customer
     */
    @ApiModelProperty("customer")
    @TableField(value="customer")
    private String customer;

    /**
     * deal_date
     */
    @ApiModelProperty("deal_date")
    @TableField(value="deal_date")
    private String dealDate;

    /**
     * deal_person
     */
    @ApiModelProperty("deal_person")
    @TableField(value="deal_person")
    private String dealPerson;

    /**
     * deal_result
     */
    @ApiModelProperty("deal_result")
    @TableField(value="deal_result")
    private String dealResult;

    /**
     * goods_bill_code
     */
    @ApiModelProperty("goods_bill_code")
    @TableField(value="goods_bill_code")
    private String goodsBillCode;

    /**
     * if_callback
     */
    @ApiModelProperty("if_callback")
    @TableField(value="if_callback")
    private String ifCallback;

    /**
     * if_handle
     */
    @ApiModelProperty("if_handle")
    @TableField(value="if_handle")
    private String ifHandle;
}
