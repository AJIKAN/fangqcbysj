package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 回告信息表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("callbackinfo")
public class CallbackInfo implements Serializable {
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * bill_id
     */
    @ApiModelProperty("bill_id")
    @TableField(value="bill_id")
    private String billId;

    /**
     * bill_type
     */
    @ApiModelProperty("bill_type")
    @TableField(value="bill_type")
    private String billType;

    /**
     * content
     */
    @ApiModelProperty("content")
    @TableField(value="content")
    private String content;

    /**
     * dial_no
     */
    @ApiModelProperty("dial_no")
    @TableField(value="dial_no")
    private String dialNo;

    /**
     * finally_dial_time
     */
    @ApiModelProperty("finally_dial_time")
    @TableField(value="finally_dial_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date finallyDialTime;

    /**
     * goods_bill_id
     */
    @ApiModelProperty("goods_bill_id")
    @TableField(value="goods_bill_id")
    private String goodsBillId;

    /**
     * locked
     */
    @ApiModelProperty("locked")
    @TableField(value="locked")
    private String locked;

    /**
     * success
     */
    @ApiModelProperty("success")
    @TableField(value="success")
    private String success;

    /**
     * type
     */
    @ApiModelProperty("type")
    @TableField(value="type")
    private String type;

    /**
     * write_time
     */
    @ApiModelProperty("write_time")
    @TableField(value="write_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date writeTime;

    /**
     * writer
     */
    @ApiModelProperty("writer")
    @TableField(value="writer")
    private String writer;
}
