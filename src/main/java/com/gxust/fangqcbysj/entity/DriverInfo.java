package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 司机信息表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("driverinfo")
public class DriverInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id", type = IdType.INPUT)
    private String id;

    /**
     * address
     */
    @ApiModelProperty("address")
    @TableField(value="address")
    private String address;

    /**
     * allow_carry_volume
     */
    @ApiModelProperty("allow_carry_volume")
    @TableField(value="allow_carry_volume")
    private Double allowCarryVolume;

    /**
     * allow_carry_weight
     */
    @ApiModelProperty("allow_carry_weight")
    @TableField(value="allow_carry_weight")
    private Double allowCarryWeight;

    /**
     * birthday
     */
    @ApiModelProperty("birthday")
    @TableField(value="birthday")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date birthday;

    /**
     * biz_licence
     */
    @ApiModelProperty("biz_licence")
    @TableField(value="biz_licence")
    private String bizLicence;

    /**
     * car_dept
     */
    @ApiModelProperty("car_dept")
    @TableField(value="car_dept")
    private String carDept;

    /**
     * car_dept_tel
     */
    @ApiModelProperty("car_dept_tel")
    @TableField(value="car_dept_tel")
    private String carDeptTel;

    /**
     * car_frame_no
     */
    @ApiModelProperty("car_frame_no")
    @TableField(value="car_frame_no")
    private String carFrameNo;

    /**
     * car_length
     */
    @ApiModelProperty("car_length")
    @TableField(value="car_length")
    private String carLength;

    /**
     * car_no
     */
    @ApiModelProperty("car_no")
    @TableField(value="car_no")
    private String carNo;

    /**
     * car_type
     */
    @ApiModelProperty("car_type")
    @TableField(value="car_type")
    private String carType;

    /**
     * car_width
     */
    @ApiModelProperty("car_width")
    @TableField(value="car_width")
    private String carWidth;

    /**
     * company_car
     */
    @ApiModelProperty("company_car")
    @TableField(value="company_car")
    private String companyCar;

    /**
     * drive_licence
     */
    @ApiModelProperty("drive_licence")
    @TableField(value="drive_licence")
    private String driveLicence;

    /**
     * driver_name
     */
    @ApiModelProperty("driver_name")
    @TableField(value="driver_name")
    private String driverName;

    /**
     * engine_no
     */
    @ApiModelProperty("engine_no")
    @TableField(value="engine_no")
    private String engineNo;

    /**
     * gender
     */
    @ApiModelProperty("gender")
    @TableField(value="gender")
    private String gender;

    /**
     * goods_height
     */
    @ApiModelProperty("goods_height")
    @TableField(value="goods_height")
    private String goodsHeight;

    /**
     * id_card
     */
    @ApiModelProperty("id_card")
    @TableField(value="id_card")
    private String idCard;

    /**
     * insurance_card
     */
    @ApiModelProperty("insurance_card")
    @TableField(value="insurance_card")
    private String insuranceCard;

    /**
     * phone
     */
    @ApiModelProperty("phone")
    @TableField(value="phone")
    private String phone;

    /**
     * remark
     */
    @ApiModelProperty("remark")
    @TableField(value="remark")
    private String remark;

    /**
     * run_licence
     */
    @ApiModelProperty("run_licence")
    @TableField(value="run_licence")
    private String runLicence;

    /**
     * state
     */
    @ApiModelProperty("state")
    @TableField(value="state")
    private String state;
}
