package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("cityexpand")
public class CityExpand implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * city_id
     */
    @ApiModelProperty("city_id")
    @TableField(value="city_id")
    private Integer cityId;

    /**
     * range_city
     */
    @ApiModelProperty("range_city")
    @TableField(value="range_city")
    private String rangeCity;
}
