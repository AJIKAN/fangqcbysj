package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 方全朝
 * @Description 员工工资表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("employeewage")
public class EmployeeWage implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id", type = IdType.AUTO)
    private Integer id;

    /**
     * allowance
     */
    @ApiModelProperty("allowance")
    @TableField(value="allowance")
    private Double allowance;

    /**
     * basic_wage
     */
    @ApiModelProperty("basic_wage")
    @TableField(value="basic_wage")
    private Double basicWage;

    /**
     * date
     */
    @ApiModelProperty("date")
    @TableField(value="date")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GTM+8")
    private Date date;

    /**
     * employee
     */
    @ApiModelProperty("employee")
    @TableField(value="employee")
    private String employee;

    /**
     * employee_code
     */
    @ApiModelProperty("employee_code")
    @TableField(value="employee_code")
    private String employeeCode;

    /**
     * station_wage
     */
    @ApiModelProperty("station_wage")
    @TableField(value="station_wage")
    private Double stationWage;
}
