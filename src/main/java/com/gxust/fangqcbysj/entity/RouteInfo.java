package com.gxust.fangqcbysj.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @author 方全朝
 * @Description 地区城市表
 * @Date 2021/3/29
 */
@Data
@Accessors(chain = true)
@TableName("routeInfo")
public class RouteInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;

    /**
     * distance
     */
    @ApiModelProperty("distance")
    @TableField(value="distance")
    private Double distance;

    /**
     * end_station
     */
    @ApiModelProperty("end_station")
    @TableField(value="end_station")
    private Integer endStation;

    /**
     * fetch_time
     */
    @ApiModelProperty("fetch_time")
    @TableField(value="fetch_time")
    private Double fetchTime;

    /**
     * pass_station
     */
    @ApiModelProperty("pass_station")
    @TableField(value="pass_station")
    private String passStation;

    /**
     * start_station
     */
    @ApiModelProperty("start_station")
    @TableField(value="start_station")
    private Integer startStation;
}
