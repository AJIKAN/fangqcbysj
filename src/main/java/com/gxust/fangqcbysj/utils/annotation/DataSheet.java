package com.gxust.fangqcbysj.utils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 设置sheet的一些基本属性
 * @author zhouliangfei
 * @version v 1.0
 * @date 2016/11/7 11:58
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface DataSheet {

    String name() default "";

    //所有数据填充cell背景颜色,不包含头列
    short bgColor() default 0x09;

    //统一的字体、字体大小、和高度，头列可以自定义高度
    String fontName() default "宋体"; //统一字体

    short fontSize() default 12; //字体大小

    short height() default (short) (1.2 * 256); //高度

    //是否使用Group的头做为注解，Excel建不起作用，只作为数据索引
    boolean isGroupTitle() default false;

    //注解定义，
    short color() default 13; //标题头颜色 HSSFColor.HSSFColorPredefined.YELLOW.getIndex()
}
