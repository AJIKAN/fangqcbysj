package com.gxust.fangqcbysj.utils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 行
 * @author zhouliangfei
 * @version v 1.0
 * @date 2017-05-19 14:37
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface GroupRow {
    Group[] group();

    //组列显示的高度。。可以自己设定，一般会高点，不设置使用默认的
    short height() default (short) (1.2 * 256);
}
