package com.gxust.fangqcbysj.utils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 队列进行分组，便于查看
 * @author zhouliangfei
 * @version v 1.0
 * @date 2016/11/7 13:41
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Group {

    String name() default "";   //列填充值

    int firstRow() default 0;
    int lastRow() default 0;
    int firstCol() default 0;  //组合列开始列
    int lastCol() default 0;   //组合列结束列
}
