package com.gxust.fangqcbysj.utils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标题头分组列表
 * @author zhouliangfei
 * @version v 1.0
 * @date 2016/11/7 12:32
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface GroupCell {

    //group占用行数
    int rowCount();

    GroupRow[] row();
}
