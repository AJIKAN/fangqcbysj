package com.gxust.fangqcbysj.utils;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * @author 方全朝
 * @Description 华通短信服务
 * @Date 2021/5/12
 */
public class HttpRequest {
    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url 发送请求的 URL
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！" + e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public static void sendSms(String phone,String content) throws UnsupportedEncodingException {
        content = URLEncoder.encode(content);
        //拼接参数
        String postData = "type=send&username=13725248726&password=70AE32150BD9711040693376D3B66946&gwid=0fde1964&mobile=" + phone + "&message=" + content + "";
        String url = "http://jk.106txt.com/smsUTF8.aspx";
        //发送并把结果赋给result,返回一个XML信息,解析xml 信息判断
        String result = HttpRequest.sendPost(url, postData);
        System.out.println("result = " + result);
    }

    //调用方式
    public static void main(String[] args) {
        //手机号  可以多个手机号
        String phones = "13725248726";
        //短信内容
        String content = null;
        try {
            content = URLEncoder.encode("【物流管理系统】尊敬的客户，您单号为HY123456的货物已送达，请您及时接收!", "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //拼接参数
        String postData = "type=send&username=13725248726&password=70AE32150BD9711040693376D3B66946&gwid=0fde1964&mobile=" + phones + "&message=" + content + "";
        String url = "http://jk.106txt.com/smsUTF8.aspx";
        //发送并把结果赋给result,返回一个XML信息,解析xml 信息判断
        String result = HttpRequest.sendPost(url, postData);
        System.out.println("result = " + result);
    }
}
