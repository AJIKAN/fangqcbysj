package com.gxust.fangqcbysj.utils;

import redis.clients.jedis.Jedis;

/**
 * @Author 方全朝
 * @Description Redis工具类
 * @Date 2021/3/14
 */
public class RedisUtils {

    /**
     * 实例化jedis对象
     */
    private static final Jedis JEDIS = new Jedis("127.0.0.1",6379);

    /**
     * 向redis中存储数据
     *
     */
    public static void setRedis(String key,String value){
        //登录密码
        JEDIS.auth("123456");
        //设置数据存储到jedis中
        JEDIS.set(key,value);
        //关闭资源
        JEDIS.close();
    }

    /**
     * 在redis中通过key获取value
     *
     * @param key key
     * @return value
     */
    public static String getRedis(String key){
        //登录密码
        JEDIS.auth("123456");
        //设置数据存储到jedis中
        String value = JEDIS.get(key);
        //关闭资源
        JEDIS.close();
        return value;
    }

    /**
     * 根据key删除redis里面的内容
     *
     * @param key key
     * @return String
     */
    public static String deleteByKey(String key){
        //登录密码
        JEDIS.auth("123456");
        try {
            JEDIS.del(key);
        } catch (Exception e) {
            //如果有异常就返回error
            return "error";
        }
        //关闭redis
        JEDIS.close();
        //如果没有异常就直接返回ok
        return "ok";
    }

    public static void main(String[] args) {
        // redis测试redis测试
        RedisUtils.setRedis("DemoKey", "DemoValue");
        System.out.println(RedisUtils.getRedis("DemoKey"));
        // RedisUtils.deleteByKey("DemoKey");
        System.out.println(RedisUtils.getRedis("DemoKey"));
    }
}
