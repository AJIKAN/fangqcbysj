package com.gxust.fangqcbysj.utils;


import com.gxust.fangqcbysj.utils.annotation.DataSheet;
import com.gxust.fangqcbysj.utils.annotation.Excel;
import com.gxust.fangqcbysj.utils.annotation.GroupCell;
import com.gxust.fangqcbysj.utils.annotation.GroupRow;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 导入导出类解析
 *
 * @author zhouliangfei
 * @version v 1.0
 * @date 2016/12/15 14:32
 **/
@Slf4j
public class ExcelBeanResolver {

    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    private static final String DEFAULT_AMOUNT_FORMAT = "0.00";
    private static final String DEFAULT_STRING_FORMAT = "@";

    //是否有缓存策略，每个导入导出对象，我只解析一次就好了。。为啥每次都要解析
    private String sheetName; //sheetName
    private boolean isGroupTitle;
    private int rowCount = 0; //标题头占用行数

    //统一的背景颜色、字体、字体大小、行高度
    private short bgColor;  //数据填充cell背景颜色,不包含头列
    private short fontSize; //统一字体大小
    private short height;   //统一高度
    private String fontName; //统一字体

    private short titleColor = HSSFColor.HSSFColorPredefined.YELLOW.getIndex(); //标题头颜色

    //组标题头
    private GroupRow[] groupRows;

    //注解列表
    private List<Excel> importColumnList = new ArrayList<>();
    private List<Excel> exportColumnList = new ArrayList<>();
    //属性列表
    private List<Field> importFieldList = new ArrayList<>();
    private List<Field> exportFieldList = new ArrayList<>();

    private Map<String, Excel> columnMapByName = new HashMap<>();
    private Map<String, Field> fieldMapByName = new HashMap<>();


    //已解析Bean
    private static Map<String, ExcelBeanResolver> resolverMap = new HashMap<>();

    /**
     * 解析excel Bean信息
     *
     * @param clazz
     * @throws Exception
     */
    public ExcelBeanResolver resolver(Class<?> clazz) {
        String beanName = clazz.getName();
        ExcelBeanResolver ebr = resolverMap.get(beanName);
        if (ebr == null) {
            synchronized (clazz) {
                ebr = resolverMap.get(beanName);
                if (ebr != null) {
                    return ebr;
                }

                //1、sheet信息、标题组设置、数据列
                dataSheet(clazz).group(clazz).column(clazz);

                //缓存起来
                resolverMap.put(beanName, this);
                return this;
            }
        }
        return ebr;
    }


    /**
     * sheet表格样式解析初始化
     *
     * @param clazz 解析对象
     * @throws Exception
     */
    private ExcelBeanResolver dataSheet(Class<?> clazz) {
        //必需要DataSheet注解
        DataSheet dataSheet = clazz.getAnnotation(DataSheet.class);
        if (dataSheet == null) {
            bgColor = 0x09;
            fontName = "宋体";
            fontSize = 12;
            height = (short) (1.2 * 256);
        } else {
            sheetName = dataSheet.name();
            bgColor = dataSheet.bgColor();
            fontName = dataSheet.fontName();
            fontSize = dataSheet.fontSize();
            height = dataSheet.height();
            isGroupTitle = dataSheet.isGroupTitle();
            titleColor = dataSheet.color();
        }
        return this;
    }

    /**
     * 标题组解析
     *
     * @param clazz 解析对象
     */
    private ExcelBeanResolver group(Class<?> clazz) {
        GroupCell groupCell = clazz.getAnnotation(GroupCell.class);
        if (groupCell != null) {
            groupRows = groupCell.row();
            rowCount = groupCell.rowCount();
        }
        return this;
    }

    /**
     * 列解析
     *
     * @param clazz 解析对象
     */
    private ExcelBeanResolver column(Class<?> clazz) {
        //获取所有成员信息
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            Excel column = field.getAnnotation(Excel.class);
            if (column != null) {
                //字段类型（0：导出导入；1：仅导出；2：仅导入）
                if (Excel.Type.IMPORT == column.type()) {
                    importColumnList.add(column);
                    importFieldList.add(field);
                } else if (Excel.Type.EXPORT == column.type()) {
                    exportColumnList.add(column);
                    exportFieldList.add(field);
                } else {
                    importColumnList.add(column);
                    importFieldList.add(field);
                    exportColumnList.add(column);
                    exportFieldList.add(field);
                }
                columnMapByName.put(column.name(), column);
                fieldMapByName.put(column.name(), field);
            }
        }
        return this;
    }

    //字体设置
    public Font font(Workbook wb) {
        Font font = wb.createFont();
        font.setFontName(fontName);
        font.setFontHeightInPoints(fontSize);
        return font;
    }

    /**
     * 为cell设置一些默认的属性
     *
     * @param style
     * @return CellStyle
     */
    public CellStyle defaultBorder(CellStyle style) {
        style.setBorderBottom(BorderStyle.THIN); // 下边框
        style.setBorderLeft(BorderStyle.THIN);// 左边框
        style.setBorderTop(BorderStyle.THIN);// 上边框
        style.setBorderRight(BorderStyle.THIN);// 右边框
        return style;
    }

    /**
     * 标题头样式设定
     *
     * @param bgColor 可以设定头自定义的颜色
     */
    public CellStyle defaultHeadStyle(CellStyle style, Font font, short bgColor) {
        CellStyle headCellStyle = defaultBorder(style);
        headCellStyle.setFillForegroundColor(bgColor);
        headCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);  //填充单元格
        headCellStyle.setAlignment(HorizontalAlignment.CENTER);//水平居中
        headCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);//垂直居中

        font.setBold(true);//粗体显示
        headCellStyle.setFont(font);
        return headCellStyle;
    }


    public CellStyle dataCellStyle(Workbook wb) {
        CellStyle dataCellStyle = defaultBorder(wb.createCellStyle());
        dataCellStyle.setFillForegroundColor(bgColor);
        dataCellStyle.setFont(font(wb));
        return dataCellStyle;
    }

    public boolean isGroupTitle() {
        return isGroupTitle;
    }

    public int rowCount() {
        return rowCount;
    }

    public String sheetName() {
        return sheetName;
    }

    public short bgColor() {
        return bgColor;
    }

    public short fontSize() {
        return fontSize;
    }

    public short height() {
        return height;
    }

    public String fontName() {
        return fontName;
    }

    public GroupRow[] groupRows() {
        return groupRows;
    }

    public List<Excel> columnList(Excel.Type type) {
        if (Excel.Type.EXPORT == type) {
            return exportColumnList;
        }
        return importColumnList;
    }

    public List<Field> fieldList(Excel.Type type) {
        if (Excel.Type.EXPORT == type) {
            return exportFieldList;
        }
        return importFieldList;
    }

    public Excel getColumn(String name) {
        return columnMapByName.get(name);
    }

    public Field getField(String name) {
        return fieldMapByName.get(name);
    }

    public short getTitleColor() {
        return this.titleColor;
    }

}
