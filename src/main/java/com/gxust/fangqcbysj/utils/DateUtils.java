package com.gxust.fangqcbysj.utils;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 时间工具类
 *
 * @author zhouliangfei
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils {
	
	public static final String[] DAY_31_MONTH= {"01","03","05","07","08","10","12"};
	public static final String[] DAY_30_MONTH= {"04","06","09","11"};
	public static final String FEBRUARY= "02";

    public static String YYYY = "yyyy";

    public static String YYYY_MM = "yyyy-MM";

    public static String YYYY_MM_DD = "yyyy-MM-dd";

    public static String YYYYMMDD = "yyyyMMdd";

    public static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM" };

    /**
     * 获取当前Date型日期
     *
     * @return Date() 当前日期
     */
    public static Date getNowDate() {
        return new Date();
    }

    /**
     * 获取当前日期, 默认格式为yyyy-MM-dd
     *
     * @return String
     */
    public static String getDate() {
        return dateTimeNow(YYYY_MM_DD);
    }

    public static final String getTime() {
        return dateTimeNow(YYYY_MM_DD_HH_MM_SS);
    }

    public static final String dateTimeNow() {
        return dateTimeNow(YYYYMMDDHHMMSS);
    }

    public static final String dateTimeNow(final String format) {
        return parseDateToStr(format, new Date());
    }

    public static final String dateTime(final Date date) {
        return parseDateToStr(YYYY_MM_DD, date);
    }

    public static final String parseDateToStr(final String format, final Date date) {
        return new SimpleDateFormat(format).format(date);
    }

    public static final Date dateTime(final String format, final String ts) {
        try {
            return new SimpleDateFormat(format).parse(ts);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 日期路径 即年/月/日 如2018/08/08
     */
    public static final String datePath() {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyy/MM/dd");
    }

    /**
     * 日期路径 即年/月/日 如20180808
     */
    public static final String dateTime() {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyyMMdd");
    }

    /**
     * 日期型字符串转化为日期 格式
     */
    public static String format(Date date, String format) {
        if (date == null) {
            return null;
        }
        try {
            return DateFormatUtils.format(date, format);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 日期型字符串转化为日期 格式
     */
    public static Date parseDate(Object str) {
        if (str == null) {
            return null;
        }
        try {
            return parseDate(str.toString(), parsePatterns);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 获取服务器启动时间
     */
    public static Date getServerStartDate() {
        long time = ManagementFactory.getRuntimeMXBean().getStartTime();
        return new Date(time);
    }

    /**
     * 计算两个时间差
     */
    public static String getDatePoor(Date endDate, Date nowDate) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return day + "天" + hour + "小时" + min + "分钟";
    }



    /**
     * 获取当前月第一天
     * @return yyyy-mm-dd
     * */
    public static String getCurrentMonthFirstDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return DateFormatUtils.format(calendar.getTime(), YYYY_MM_DD);
    }

    /**
     * 设置为月份第一天
     * @return yyyy-mm-dd
     * */
    public static Date setCurrentMonthFirstDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取当前日
     * @return yyyy-mm-dd
     * */
    public static Date getCurrentDate() {
        try {
            return parseDate(getCurrentDay(), YYYY_MM_DD);
        } catch (ParseException e) {
        }
        return null;
    }

    /**
     * 获取当前日
     * @return yyyy-mm-dd
     * */
    public static String getCurrentDay() {
        return DateFormatUtils.format(new Date(), YYYY_MM_DD);
    }

    /**
     * 获取当前日期过去 pastDay 天
     * @param pastDay 过去多少天
     * @return yyyy-mm-dd
     * */
    public static String getPastDayDateString(int pastDay) {
        return DateFormatUtils.format(getPastDayDate(pastDay), YYYY_MM_DD);
    }

    /**
     * 获取当前日期过去 pastDay 天
     * @param pastDay 过去多少天
     * @return yyyy-mm-dd
     * */
    public static Date getPastDayDate(int pastDay) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DAY_OF_YEAR, 1 - pastDay);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    //选择时间是否为最近一个月
    public static boolean isLatestMonth(Date begin, Date end) {
        Date date = getPastDayDate(30); //当前时间-30天
        return false;
    }

    /**
     * 设置时间为月份第一天
     * @param date
     * @return boolean
     * */
    public static Date setFirstDayByMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime() ;
    }

    public static Date setDefaultBegin(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime() ;
    }

    public static Date setDefaultEnd(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime() ;
    }

    /**
     * 设置月份为最后一天
     * @param date
     * @return boolean
     * */
    public static Date setLastDayByMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime() ;
    }
	
	/**
     * 得到两个日期的相隔多少个月
     * @param date1     日期1
     * @param date2     日期2
     * @return          多少个月
     */
    public static int calculateMonthIn(Date date1, Date date2) {
        Calendar cal1 = new GregorianCalendar();
        cal1.setTime(date1);
        Calendar cal2 = new GregorianCalendar();
        cal2.setTime(date2);
        int c = (cal1.get(Calendar.YEAR) - cal2.get(Calendar.YEAR)) * 12
                + cal1.get(Calendar.MONTH) - cal2.get(Calendar.MONTH);
        return c;
    }

    /**
     * 得到两个日期的相隔多少天
     * @param date1     日期1 开始
     * @param date2     日期2 结束
     * @return int      多少天
     */
    public static int dateDiffDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        long diffDay = (cal2.getTimeInMillis() - cal1.getTimeInMillis())  / (1000 * 60 * 60 * 24);
        return (int) diffDay + 1;
    }

    /**
     * 得到两个日期的相隔多少个月
     * @param date1     日期1  开始
     * @param date2     日期2 结束
     * @return          多少个月
     */
    public static int dateDiffMonth(Date date1, Date date2) {
        Calendar cal1 = new GregorianCalendar();
        cal1.setTime(date1);
        Calendar cal2 = new GregorianCalendar();
        cal2.setTime(date2);
        int c = (cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR)) * 12
                + cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
        return c + 1;
    }
    
    /**
     * @param year 年份
     * @param month 月份 
     * 根据年份月份获取出当前年月的最后一天
     * @return
     */
    @Deprecated
    public static String monthLastDay(String year,String month) {
    	if(FEBRUARY.equals(month)) {
    		int newYear = Integer.valueOf(year);
    		if((newYear%4==0&&newYear%100!=0)||(newYear%400 == 0)) {
    			return year+"-"+month+"-29";
    		}else {
    			return year+"-"+month+"-28";
    		}
    	}else if(Arrays.asList(DAY_31_MONTH).contains(month)){
    		return year+"-"+month+"-31";
    	}else if(Arrays.asList(DAY_30_MONTH).contains(month)){
    		return year+"-"+month+"-30";
    	}
    	return "1970-01-01";
    }

    @Deprecated
    public static Date startDate(String year,String month) {
    	String startTimeStr = year+"-"+month+"-01 00:00:00";
    	return parseDate(startTimeStr);
    }

    @Deprecated
    public static Date endDate(String year,String month) {
    	String lastDayStr =  monthLastDay(year,month);
    	String lastTimeStr = lastDayStr + " 23:59:59";
    	return parseDate(lastTimeStr);
    }

    /**
     * 是否在两个时间之间
     * @param date 比对的时间
     * @param begin
     * @param end
     * @return boolean 在两个时间时间
     * */
    public static boolean between(Date date, Date begin, Date end) {
        if (date == null) {
            return false;
        } else if (begin == null && end == null) {
            return true;
        } else if (begin == null && end != null) {
            return date.compareTo(end) <= 0;
        } else if (begin != null && end == null) {
            return begin.compareTo(date) <= 0;
        }
        return begin.compareTo(date) <= 0 &&  date.compareTo(end) <= 0;
    }


    /**
     * 判断两个时间是否同一年月
     * @param date1
     * @param date2
     * @return boolean
     * */
    public static boolean isSameMonth(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            return false;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        return calendar.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) && calendar.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH);
    }


    /**
     * 获取月份最大天
     * @param date
     * @return boolean
     * */
    public static int getMonthMaxDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH) ;
    }


    /**
     * 设置为月份第一天
     * @param date
     * @return  Date yyyy-MM-dd HH:mm:ss
     * */
    public static Date setMonthFirstDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 设置月份最后一天
     * @param date
     * @return Date yyyy-MM-dd HH:mm:ss
     * */
    public static Date setMonthLastDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime() ;
    }

    /**
     * 设置月份最后一天
     * @param date
     * @return Date yyyy-MM-dd
     * */
    public static Date setMonthLastDayByDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime() ;
    }


    /**
     * 设置月份最后一天
     * @param date
     * @return Date yyyy-MM-dd
     * */
    public static Date setDefaultDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime() ;
    }

    public static void main(String[] args) throws Exception {
        //System.out.println(dateDiffMonth(DateUtils.parseDate("2020-05-01", "yyyy-MM-dd"), DateUtils.parseDate("2020-07-15", "yyyy-MM-dd")));

        System.out.println(getMonthMaxDay(new Date()));
        System.out.println(getMonthMaxDay(DateUtils.parseDate("2020-02-01", "yyyy-MM-dd")));
    }
}
