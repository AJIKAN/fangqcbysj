package com.gxust.fangqcbysj.utils;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

import java.util.HashMap;

/**
 * @Description: 测试短信发送
 * @Author: 方全朝
 * @Date: 2021/5/7
 */
class SendSms {
    public static void main(String[] args) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4GEqegHD8mSbz5uWXQbn", "vZFcQLmlOSa3aiEpfRLjQllR6Z36Jm");
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        // 不要动
        request.setMethod(MethodType.POST);
        // 不要动
        request.setDomain("dysmsapi.aliyuncs.com");
        // 不要动
        request.setVersion("2017-05-25");
        // 不要动
        request.setAction("SendSms");
        // 不要动
        request.putQueryParameter("RegionId","cn-hangzhou");
        request.putQueryParameter("PhoneNumbers","18275806425");
        request.putQueryParameter("SignName","紫荆牙科");
        // 通知
        //request.putQueryParameter("TemplateCode","SMS_209821009");
        // 验证码
        request.putQueryParameter("TemplateCode","SMS_216840959");
        HashMap<String,Object> map = new HashMap<>();
        map.put("code","KH123456");
        map.put("name","HY123456");
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(map));
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
}
