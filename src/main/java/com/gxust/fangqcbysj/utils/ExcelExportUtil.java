package com.gxust.fangqcbysj.utils;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.List;

/**
 * excel导出
 *
 * @author zhouliangfei
 * @version v1.0.0
 * @date 2019-05-28 21:08
 */
@Slf4j
public class ExcelExportUtil {

    public static <T> void export(List<T> dataList, Class<T> clazz, String sheetName,
                                  HttpServletRequest request, HttpServletResponse response) {
        ExcelUtil util = new ExcelUtil<>(clazz);
        Workbook workbook = util.exportExcel(dataList, sheetName);
        try (OutputStream os = response.getOutputStream()) {
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/octet-stream;charset=utf-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            response.setHeader("Content-Disposition", "attachment;fileName=" + HeaderUtil.getFileDownloadHeader(request, sheetName) + ".xls");
            workbook.write(os);
            os.flush();
            /*FileOutputStream fileInputStream = new FileOutputStream(new File("E://result"+sheetName+System.currentTimeMillis()+".xlsx"));
            workbook.write(fileInputStream);
            fileInputStream.close();*/

        } catch (Exception e) {
            log.error("{} excel export error", sheetName, e);
        } finally {
            IOUtils.closeQuietly(workbook);
        }
    }

    //导出合并行
    public static <T> void exportRowMerge(List<T> dataList, Class<T> clazz, String sheetName, int sheetIndex, int startRow, int[] cellIndexs,
                                          HttpServletRequest request, HttpServletResponse response) {
        ExcelUtil util = new ExcelUtil<>(clazz);
        Workbook workbook = util.exportExcel(dataList, sheetName);
        //合并行
        ExcelRowMergeUtil.mergeRow(workbook, sheetIndex, startRow, cellIndexs);
        try (OutputStream os = response.getOutputStream()) {
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/octet-stream;charset=utf-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            response.setHeader("Content-Disposition", "attachment;fileName=" + HeaderUtil.getFileDownloadHeader(request, sheetName) + ".xls");
            workbook.write(os);
            os.flush();
        } catch (Exception e) {
            log.error("{} excel export error", sheetName, e);
        } finally {
            IOUtils.closeQuietly(workbook);
        }
    }

    public static <T> void export(Workbook workbook, String fileName, HttpServletRequest request, HttpServletResponse response) {
        try (OutputStream os = response.getOutputStream()) {
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/octet-stream;charset=utf-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            response.setHeader("Content-Disposition", "attachment;fileName=" + HeaderUtil.getFileDownloadHeader(request, fileName) + ".xls");
            workbook.write(os);
            os.flush();
        } catch (Exception e) {
            log.error("{} excel export error", fileName, e);
        } finally {
            IOUtils.closeQuietly(workbook);
        }
    }
}
