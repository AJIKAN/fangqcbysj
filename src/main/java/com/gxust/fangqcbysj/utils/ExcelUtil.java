package com.gxust.fangqcbysj.utils;

import com.gxust.fangqcbysj.utils.annotation.Excel;
import com.gxust.fangqcbysj.utils.annotation.Group;
import com.gxust.fangqcbysj.utils.annotation.GroupRow;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Excel相关处理
 *
 * @author zhouliangfei
 */
public class ExcelUtil<T> {
    private static final Logger log = LoggerFactory.getLogger(ExcelUtil.class);

    /**
     * Excel sheet最大行数，默认65536
     */
    public static final int sheetSize = 6000; //65500;

    //首行高
    private static int FIRST_LINE_DEFAULT_HEIGHT = 16;

    //判断是否数字正则
    private static final Pattern pattern = Pattern.compile("^-?\\d+(\\.\\d+)?$");

    //工作表名称
    private String sheetName;

    //导出类型（EXPORT:导出数据；IMPORT：导入模板）
    private Excel.Type type;

    //工作薄对象
    private Workbook wb;

    //工作表对象
    private Sheet sheet;

    //对象索引属性
    private static String INDEX_FIELD = "index";
    private Field indexField;

    /**
     * 实体对象
     */
    public Class<T> clazz;
    private ExcelBeanResolver resolver;

    public ExcelUtil(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void init(String sheetName, Excel.Type type) {
        //解析类型
        resolver = new ExcelBeanResolver();
        resolver = resolver.resolver(this.clazz);
        this.sheetName = resolver.sheetName();
        if (!StringUtils.isEmpty(sheetName)) {
            this.sheetName = sheetName;
        }
        this.type = type; //导入还是导出
    }

    //写数据开始索引
    private int getWriteIndex() {
        if (resolver.isGroupTitle()) {
            return resolver.rowCount() - 1;
        }
        return resolver.rowCount();
    }

    //写标题头开始索引
    private int getWriteTitleIndex() {
        return resolver.rowCount();
    }


    //创建标题组
    private void createGroup() {
        if (resolver.groupRows() == null) {
            return;
        }

        //样式
        Font font = resolver.font(this.wb);
        CellStyle cellStyle = resolver.defaultHeadStyle(wb.createCellStyle(), font, resolver.getTitleColor());
        int cols = resolver.fieldList(type).size();
        //默认从第一行开始
        int starRow = 0;
        for (GroupRow groupRow : resolver.groupRows()) {
            Row row = sheet.createRow(starRow); //创建一行
            row.setHeight(groupRow.height()); //设置行高
            Group[] groups = groupRow.group();

            //合并列每个列的 cell样式, 就算合并了。。还是需要一个一个设置样式
            for (int i = 0; i < cols; i++) {
                Cell groupCell = row.createCell(i);
                groupCell.setCellStyle(cellStyle);
                sheet.setColumnWidth(i, (int) ((16 + 0.72) * 256));
            }

            //遍历组。设置组信息
            for (Group group : groups) {
                //只占一列的不合并
                if (group.lastRow() - group.firstRow() > 0 || group.lastCol() - group.firstCol() > 0) {
                    //合并cell
                    CellRangeAddress cellRangeAddress = new CellRangeAddress(group.firstRow(), group.lastRow(),
                            group.firstCol(), group.lastCol());
                    sheet.addMergedRegion(cellRangeAddress);
                }

                //最终显示设置，上面主要为了添加边框
                Cell headGroupCell = row.createCell(group.firstCol());
                headGroupCell.setCellType(CellType.STRING);
                headGroupCell.setCellValue(group.name());
                headGroupCell.setCellStyle(cellStyle);
            }
            starRow++;
        }
    }

    /**
     * 对list数据源将其里面的数据导入到excel表单
     *
     * @return 结果
     */
    private void createTitle(int dataSize) {
        if (resolver.isGroupTitle()) {
            return;
        }
        //获取导出属性列表
        List<Field> fields = resolver.fieldList(type);
        Cell cell = null; // 产生单元格
        // 产生一行
        Row row = sheet.createRow(getWriteTitleIndex());
        // 写入各个字段的列头名称
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            Excel attr = field.getAnnotation(Excel.class);
            // 创建列
            cell = row.createCell(i);
            // 设置列中写入内容为String类型
            cell.setCellType(CellType.STRING);
            CellStyle cellStyle = wb.createCellStyle();
            if (attr.name().indexOf("注：") >= 0) {
                Font font = resolver.font(this.wb);
                font.setColor(HSSFFont.COLOR_RED);
                resolver.defaultHeadStyle(cellStyle, font, resolver.getTitleColor());
                sheet.setColumnWidth(i, 6000);
            } else {
                Font font = resolver.font(this.wb);
                resolver.defaultHeadStyle(cellStyle, font, resolver.getTitleColor());
                // 设置列宽
                sheet.setColumnWidth(i, (int) ((attr.width() + 0.72) * 256));
                row.setHeight((short) (FIRST_LINE_DEFAULT_HEIGHT * 20));
            }
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            cellStyle.setWrapText(true);
            cell.setCellStyle(cellStyle);

            // 写入列名
            cell.setCellValue(attr.name());

            // 如果设置了提示信息则鼠标放上去提示.
            int endRow = dataSize;
            if (StringUtils.isNotEmpty(attr.prompt()) && endRow > 0) {
                // 这里默认设了2-101列提示.
                setHSSFPrompt(sheet, "", attr.prompt(), getWriteIndex(), getWriteIndex() + endRow, i, i);
            }
            // 如果设置了combo属性则本列只能选择不能输入
            if (attr.combo().length > 0 && endRow > 0) {
                // 这里默认设了2-101列只能选择不能输入.
                setHSSFValidation(sheet, attr.combo(), getWriteIndex(), getWriteIndex() + endRow, i, i);
            }
        }
    }


    public Workbook exportExcel(List<T> list) {
        return exportExcel(list);
    }

    /**
     * 对list数据源将其里面的数据导入到excel表单
     *
     * @param list      导出数据集合
     * @param sheetName 工作表的名称
     * @return 结果
     */
    public Workbook exportExcel(List<T> list, String sheetName) {
        //初始化一下
        this.init(sheetName, Excel.Type.EXPORT);
        //创建表格
        createWorkbook();
        //生成数据列样式
        Map<String, CellStyle> styleMap = genDataCellStyle();
        //数据大于指定行数，分sheet
        double sheetNo = Math.ceil(list.size() / sheetSize);
        for (int index = 0; index <= sheetNo; index++) {
            createSheet(sheetNo, index);
            //创建标题组
            createGroup();

            int startNo = index * sheetSize;
            int endNo = Math.min(startNo + sheetSize, list.size());
            //创建标题头
            createTitle(endNo);
            //导出数据
            exportDataToExcel(index, list, styleMap, startNo, endNo);
        }
        return wb;
    }

    /**
     * 填充excel数据
     *
     * @param index 序号
     */
    private void exportDataToExcel(int index, List<T> list, Map<String, CellStyle> styleMap, int startNo, int endNo) {
        //获取导出属性列表
        List<Field> fields = resolver.fieldList(type);
        // 写入各条记录,每条记录对应excel表中的一行
        Row row = null;
        Cell cell = null;
        CellStyle cs = null;
        Field field = null;
        Excel attr = null;
        for (int i = startNo; i < endNo; i++) {
            row = sheet.createRow(i + 1 - startNo + +getWriteIndex());
            // 得到导出对象.
            T vo = (T) list.get(i);
            for (int j = 0; j < fields.size(); j++) {
                // 获得field.
                field = fields.get(j);
                // 设置实体类私有属性可访问
                field.setAccessible(true);
                attr = field.getAnnotation(Excel.class);
                try {
                    // 设置行高
                    row.setHeight((short) (attr.height() * 20));
                    // 根据Excel中设置情况决定是否导出,有些情况需要保持为空,希望用户填写这一列.
                    if (attr.isExport()) {
                        // 创建cell
                        cell = row.createCell(j);
                        cs = styleMap.get(attr.name());
                        if (cs != null) {
                            cell.setCellStyle(cs);
                        }
                        if (vo == null) {
                            // 如果数据存在就填入,不存在填入空格.
                            cell.setCellValue("");
                            continue;
                        }

                        // 用于读取对象中的属性
                        Object value = getTargetValue(vo, field, attr);
                        if (value == null) {
                            continue;
                        }
                        String dateFormat = attr.dateFormat();
                        String readConverterExp = attr.readConverterExp();
                        if (StringUtils.isNotEmpty(dateFormat)) {
                            cell.setCellValue(DateUtils.parseDateToStr(dateFormat, (Date) value));
                        } else if (StringUtils.isNotEmpty(readConverterExp)) {
                            cell.setCellValue(convertByExp(String.valueOf(value), readConverterExp));
                        } else {
                            cell.setCellType(CellType.STRING);
                            // 如果数据存在就填入,不存在填入空格.
                            cell.setCellValue(value == null ? attr.defaultValue() : value + attr.suffix());
                        }
                    }
                } catch (Exception e) {
                    log.error("导出Excel失败{}", e.getMessage());
                }
            }
        }
    }


    //生成每列样式
    private Map<String, CellStyle> genDataCellStyle() {
        CellStyle cs = null;
        Font font = resolver.font(this.wb);
        Map<String, CellStyle> cellStyleMap = new HashMap<>();
        for (Excel excel : resolver.columnList(type)) {
            cs = wb.createCellStyle();
            cs.setFont(font);
            cs.setFillBackgroundColor(HSSFColorPredefined.WHITE.getIndex());
            cs.setAlignment(excel.hAlign());//水平居中
            cs.setVerticalAlignment(excel.vAlign());//垂直居中
            resolver.defaultBorder(cs);
            cellStyleMap.put(excel.name(), cs);
        }
        return cellStyleMap;
    }

    //===================================================导入部分代码


    /**
     * 对excel表单默认第一个索引名转换成list
     *
     * @param is 输入流
     * @return 转换后集合
     */
    public List<T> importExcel(InputStream is) throws Exception {
        return importExcel(StringUtils.EMPTY, is, 1);
    }

    public List<T> importExcel(InputStream is, int startRow) throws Exception {
        return importExcel(StringUtils.EMPTY, is, startRow);
    }

    public List<T> importExcel(String sheetName, InputStream is) throws Exception {
        return importExcel(sheetName, is, 1);
    }

    /**
     * 对excel表单指定表格索引名转换成list
     *
     * @param sheetName 表格索引名
     * @param is        输入流
     * @return 转换后集合
     */
    public List<T> importExcel(String sheetName, InputStream is, int startRow) throws Exception {
        //初始化一下
        this.init(sheetName, Excel.Type.IMPORT);
        this.wb = WorkbookFactory.create(is);
        Sheet sheet = null;
        if (StringUtils.isNotEmpty(sheetName)) {
            // 如果指定sheet名,则取指定sheet中的内容.
            sheet = wb.getSheet(sheetName);
        } else {
            // 如果传入的sheet名不存在则默认指向第1个sheet.
            sheet = wb.getSheetAt(0);
        }

        if (sheet == null) {
            throw new IOException("文件sheet不存在");
        }

        List<T> list = new ArrayList<T>();
        int rows = sheet.getPhysicalNumberOfRows();
        if (rows <= 0) {
            return list;
        }
        // 默认序号
        int serialNum = 0;
        //导入熟悉列表
        List<Field> importFieldList = resolver.fieldList(this.type);
        // 定义一个map用于存放列的序号和field.
        Map<Integer, Field> fieldsMap = new HashMap<Integer, Field>();
        for (int col = 0; col < importFieldList.size(); col++) {
            Field field = importFieldList.get(col);
            Excel attr = field.getAnnotation(Excel.class);
            if (attr != null && (attr.type() == Excel.Type.ALL || attr.type() == type)) {
                // 设置类的私有字段属性可访问.
                field.setAccessible(true);
                fieldsMap.put(++serialNum, field);
            }
        }
        //获取index属性
        indexField = ReflectUtils.getAccessibleField(clazz.newInstance(), INDEX_FIELD);
        T entity = null;
        Row row = null;
        //判断导入摸板是否正确
        // 获取表头
        row = sheet.getRow(startRow - 1);
        int cell = serialNum;
        entity = clazz.newInstance();
        resolver.resolver(entity.getClass());
        List<Excel> excels = resolver.columnList(this.type);
        if (cell != excels.size()) {
            throw new Exception("模板不正确");
        }
        for (int column = 0; column < cell; column++) {
            Object val = this.getCellValue(row, column);
            if (!excels.get(column).name().equals(val)) {
                throw new Exception("模板不正确");
            }
        }
        for (int i = startRow; i < rows; i++) {
            // 从第2行开始取数据,默认第一行是表头.
            row = sheet.getRow(i);
            int cellNum = serialNum;
            // 如果不存在实例则新建.
            entity = clazz.newInstance();
            //索引属性不为空的时候,写数据行
            if (indexField != null) {
                indexField.setAccessible(true);
                indexField.set(entity, i);
            }
            int emptyCells = 0; //空格列
            for (int column = 0; column < cellNum; column++) {
                Object val = this.getCellValue(row, column);
                if (val == null) {
                    emptyCells++;
                    continue;
                }
                // 从map中得到对应列的field.
                Field field = fieldsMap.get(column + 1);
                // 取得类型,并根据对象类型设置值.
                Class<?> fieldType = field.getType();
                if (String.class == fieldType) {
                    String s = ConvertUtil.toStr(val);
                    if (StringUtils.endsWith(s, ".0")) {
                        val = StringUtils.substringBefore(s, ".0");
                    } else {
                        val = ConvertUtil.toStr(val);
                    }
                } else if ((Integer.TYPE == fieldType) || (Integer.class == fieldType)) {
                    val = ConvertUtil.toInt(val);
                } else if ((Long.TYPE == fieldType) || (Long.class == fieldType)) {
                    val = ConvertUtil.toLong(val);
                } else if ((Double.TYPE == fieldType) || (Double.class == fieldType)) {
                    val = ConvertUtil.toDouble(val);
                } else if ((Float.TYPE == fieldType) || (Float.class == fieldType)) {
                    val = ConvertUtil.toFloat(val);
                } else if (Date.class == fieldType) {
                    if (val instanceof String) {
                        val = DateUtils.parseDate(val);
                    } else if (val instanceof Double) {
                        val = org.apache.poi.ss.usermodel.DateUtil.getJavaDate((Double) val);
                    }
                } else if (BigDecimal.class == fieldType) {
                    if (val instanceof String) {
                        if (StringUtils.isEmpty((String) val)) {
                            continue;
                        }

                        if (!pattern.matcher(val.toString()).matches()) {
                            throw new Exception("数值类型不正确，请检查摸板！");
                        }
                        val = new BigDecimal(String.valueOf(val));
                    } else {
                        val = ConvertUtil.toBigDecimal(val);
                    }
                }
                if (fieldType != null) {
                    Excel attr = field.getAnnotation(Excel.class);
                    String propertyName = field.getName();
                    if (StringUtils.isNotEmpty(attr.targetAttr())) {
                        propertyName = field.getName() + "." + attr.targetAttr();
                    } else if (StringUtils.isNotEmpty(attr.readConverterExp())) {
                        val = reverseByExp(String.valueOf(val), attr.readConverterExp());
                    }
                    ReflectUtils.invokeSetter(entity, propertyName, val);
                }
            }

            //遇到了空行直接退出
            if (emptyCells >= cellNum) {
                break;
            }

            if (entity != null) {
                list.add(entity);
            }
        }
        return list;
    }

    /**
     * 设置单元格上提示
     *
     * @param sheet         要设置的sheet.
     * @param promptTitle   标题
     * @param promptContent 内容
     * @param firstRow      开始行
     * @param endRow        结束行
     * @param firstCol      开始列
     * @param endCol        结束列
     * @return 设置好的sheet.
     */
    public static Sheet setHSSFPrompt(Sheet sheet, String promptTitle, String promptContent, int firstRow, int endRow,
                                      int firstCol, int endCol) {
        // 构造constraint对象
        DVConstraint constraint = DVConstraint.createCustomFormulaConstraint("DD1");
        // 四个参数分别是：起始行、终止行、起始列、终止列
        CellRangeAddressList regions = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);
        // 数据有效性对象
        HSSFDataValidation dataValidationView = new HSSFDataValidation(regions, constraint);
        dataValidationView.createPromptBox(promptTitle, promptContent);
        sheet.addValidationData(dataValidationView);
        return sheet;
    }

    /**
     * 设置某些列的值只能输入预制的数据,显示下拉框.
     *
     * @param sheet    要设置的sheet.
     * @param textList 下拉框显示的内容
     * @param firstRow 开始行
     * @param endRow   结束行
     * @param firstCol 开始列
     * @param endCol   结束列
     * @return 设置好的sheet.
     */
    public static Sheet setHSSFValidation(Sheet sheet, String[] textList, int firstRow, int endRow, int firstCol,
                                          int endCol) {
        // 加载下拉列表内容
        DVConstraint constraint = DVConstraint.createExplicitListConstraint(textList);
        // 设置数据有效性加载在哪个单元格上,四个参数分别是：起始行、终止行、起始列、终止列
        CellRangeAddressList regions = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);
        // 数据有效性对象
        HSSFDataValidation dataValidationList = new HSSFDataValidation(regions, constraint);
        sheet.addValidationData(dataValidationList);
        return sheet;
    }

    /**
     * 解析导出值 0=男,1=女,2=未知
     *
     * @param propertyValue 参数值
     * @param converterExp  翻译注解
     * @return 解析后值
     * @throws Exception
     */
    public static String convertByExp(String propertyValue, String converterExp) throws Exception {
        try {
            String[] convertSource = converterExp.split(",");
            for (String item : convertSource) {
                String[] itemArray = item.split("=");
                if (itemArray[0].equals(propertyValue)) {
                    return itemArray[1];
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return propertyValue;
    }

    /**
     * 反向解析值 男=0,女=1,未知=2
     *
     * @param propertyValue 参数值
     * @param converterExp  翻译注解
     * @return 解析后值
     * @throws Exception
     */
    public static String reverseByExp(String propertyValue, String converterExp) throws Exception {
        try {
            String[] convertSource = converterExp.split(",");
            for (String item : convertSource) {
                String[] itemArray = item.split("=");
                if (itemArray[1].equals(propertyValue)) {
                    return itemArray[0];
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return propertyValue;
    }

    /**
     * 编码文件名
     */
    public String encodingFilename(String filename) {
        filename = UUID.randomUUID().toString() + "_" + filename + ".xls";
        return filename;
    }

    /**
     * 获取下载路径
     *
     * @param filename 文件名称
     */
    public String getAbsoluteFile(String filename) {
        String downloadPath = "/download/" + filename;
        File desc = new File(downloadPath);
        if (!desc.getParentFile().exists()) {
            desc.getParentFile().mkdirs();
        }
        return downloadPath;
    }

    /**
     * 获取bean中的属性值
     *
     * @param vo    实体对象
     * @param field 字段
     * @param excel 注解
     * @return 最终的属性值
     * @throws Exception
     */
    private Object getTargetValue(T vo, Field field, Excel excel) throws Exception {
        Object o = field.get(vo);
        if (StringUtils.isNotEmpty(excel.targetAttr())) {
            String target = excel.targetAttr();
            if (target.indexOf(".") > -1) {
                String[] targets = target.split("[.]");
                for (String name : targets) {
                    o = getValue(o, name);
                }
            } else {
                o = getValue(o, target);
            }
        }
        return o;
    }

    /**
     * 以类的属性的get方法方法形式获取值
     *
     * @param o
     * @param name
     * @return value
     * @throws Exception
     */
    private Object getValue(Object o, String name) throws Exception {
        if (StringUtils.isNotEmpty(name)) {
            Class<?> clazz = o.getClass();
            String methodName = "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
            Method method = clazz.getMethod(methodName);
            o = method.invoke(o);
        }
        return o;
    }


    /**
     * 创建一个工作簿
     */
    public void createWorkbook() {
        this.wb = new HSSFWorkbook();
    }

    /**
     * 创建工作表
     *
     * @param sheetNo sheet数量
     * @param index   序号
     */
    public void createSheet(double sheetNo, int index) {
        this.sheet = wb.createSheet();
        // 设置工作表的名称.
        if (sheetNo == 0) {
            wb.setSheetName(index, sheetName);
        } else {
            wb.setSheetName(index, sheetName + index);
        }
    }

    /**
     * 获取单元格值
     *
     * @param row    获取的行
     * @param column 获取单元格列号
     * @return 单元格值
     */
    public Object getCellValue(Row row, int column) {
        if (row == null) {
            return row;
        }
        Object val = null;
        try {
            Cell cell = row.getCell(column);
            if (cell != null) {
                if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                    val = cell.getNumericCellValue();
                    if (HSSFDateUtil.isCellDateFormatted(cell)) {
                        val = DateUtil.getJavaDate((Double) val); // POI Excel 日期格式转换
                    } else {
                        if ((Double) val % 1 > 0) {
                            val = new DecimalFormat("0.00").format(val);
                        } else {
                            val = new DecimalFormat("0").format(val);
                        }
                    }
                } else if (cell.getCellTypeEnum() == CellType.STRING) {
                    val = cell.getStringCellValue();
                } else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
                    val = cell.getBooleanCellValue();
                } else if (cell.getCellTypeEnum() == CellType.ERROR) {
                    val = cell.getErrorCellValue();
                }

            }
        } catch (Exception e) {
            return val;
        }
        return val;
    }
}