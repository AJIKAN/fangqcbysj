package com.gxust.fangqcbysj.controller;

import com.gxust.fangqcbysj.entity.CityExpand;
import com.gxust.fangqcbysj.entity.Region;
import com.gxust.fangqcbysj.entity.RouteInfo;
import com.gxust.fangqcbysj.entity.dto.CityExpandDto;
import com.gxust.fangqcbysj.service.CityExpandService;
import com.gxust.fangqcbysj.service.RouteInfoService;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping(value = "/route")
@Slf4j
public class RouteController {

    @Autowired
    private CityExpandService cityExpandService;

    @Autowired
    private RouteInfoService routeInfoService;


    @ApiOperation(value = "新增城市扩充信息", notes = "新增城市扩充信息")
    @RequestMapping(value = "/addExpand", method = RequestMethod.POST)
    public ResultJson addExpand(@RequestBody CityExpand cityExpand) {
        boolean flag = false;
        flag = cityExpandService.addExpand(cityExpand);
        if (!flag) {
            return ResultJson.failed();
        }else {
            routeInfoService.generateRoute();
            return ResultJson.ok();
        }
    }

    @ApiOperation(value = "删除一条城市扩充信息", notes = "通过 id 删除一条城市扩充信息")
    @RequestMapping(value = "/deleteExpand", method = RequestMethod.DELETE)
    public ResultJson deleteExpand(@RequestParam("id") int id) {
        boolean flag = false;
        flag = cityExpandService.deleteExpand(id);
        if (!flag) {
            return ResultJson.failed();
        }else {
            routeInfoService.generateRoute();
            return ResultJson.ok();
        }

    }
    @ApiOperation(value = "更新一条城市信息", notes = "通过 id 更新一条城市扩充信息")
    @RequestMapping(value = "/updateExpand", method = RequestMethod.PUT)
    public ResultJson updateExpand(@RequestBody CityExpand cityExpand) {
        boolean flag = false;
        flag = cityExpandService.updateExpand(cityExpand);
        if (!flag) {
            return ResultJson.failed();
        }
        routeInfoService.generateRoute();
        return ResultJson.ok();
    }

    @ApiOperation(value = "得到所有城市", notes = "返回所有的城市信息")
    @RequestMapping(value = "/findAllRegions", method = RequestMethod.GET)
    public List<Region> findAllRegion() {
        List<Region> regions = cityExpandService.findAllRegions();
        return regions;
    }

    @ApiOperation(value = "得到无范围的城市", notes = "返回无范围的城市信息")
    @RequestMapping(value = "/findLeftRegions", method = RequestMethod.GET)
    public List<Region> findLeftRegions() {
        List<Region> regions = cityExpandService.findLeftRegions();
        return regions;
    }

    @ApiOperation(value = "得到所有的城市范围信息", notes = "得到所有的城市范围信息")
    @RequestMapping(value = "/findAllExpands", method = RequestMethod.GET)
    public ResultJson findAllExpands(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        List<CityExpandDto> page = cityExpandService.findAllExpands(pageNo,pageSize);
        ResultJson result = new ResultJson(200, "SUCCESS", page.size(), page);
        return result;
    }

    @ApiOperation(value = "得到一条的城市范围信息", notes = "通过 id 得到一条城市范围信息")
    @RequestMapping(value = "/findExpand", method = RequestMethod.GET)
    public CityExpand findExpandById(@RequestParam("id") int id) {
        CityExpand cityExpand = cityExpandService.findById(id);
        return cityExpand;
    }

    @ApiOperation(value = "得到所有线路信息", notes = "得到所有的线路信息")
    @RequestMapping(value = "/findAllRoutes", method = RequestMethod.GET)
    public List<RouteInfo> findAllRouteInfos() {
        List<RouteInfo> routeInfos = routeInfoService.findAllRouteInfos();
        return routeInfos;
    }
}
