package com.gxust.fangqcbysj.controller;

import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.service.*;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping(value = "/monitor")
@Slf4j
public class MoniterController {

    @Autowired
    private GoodsBillService goodsBillService;

    @Autowired
    private CustomerAmountService customerAmountService;

    @Autowired
    private DriverAmountService driverAmountService;

    @Autowired
    private ContactsServiceService contactsServiceService;

    @Autowired
    private LineOverallService lineOverallService;

    @Autowired
    private CarCostService carCostService;

    @ApiOperation(value = "预期未到运单", notes = "预期未到运单")
    @RequestMapping(value = "/selectUnArrive", method = RequestMethod.GET)
    public ResultJson selectUnArrive() {
        List<GoodsBill> goodsbills = goodsBillService.selectAllUnArrive();
        ResultJson result = new ResultJson(200, "SUCCESS", goodsbills.size(), goodsbills);
        return result;
    }

    @ApiOperation(value = "滞留未取运单", notes = "滞留未取运单")
    @RequestMapping(value = "/selectUnTake", method = RequestMethod.GET)
    public ResultJson selectUnTake() {
        List<GoodsBill> goodsbills = goodsBillService.selectAllUnTake();
        ResultJson result = new ResultJson(200, "SUCCESS", goodsbills.size(), goodsbills);
        return result;
    }

    @ApiOperation(value = "打印客户用量", notes = "打印客户用量")
    @RequestMapping(value = "/selectCusAcount", method = RequestMethod.GET)
    public ResultJson selectCusAcount() {
        List<CustomerAmount> customerAmounts = customerAmountService.selectAllCusAcount();
        ResultJson result = new ResultJson(200, "SUCCESS", customerAmounts.size(), customerAmounts);
        return result;
    }


    @ApiOperation(value = "打印司机用量",notes = "打印司机用量")
    @RequestMapping(value = "/selectDriAcount", method = RequestMethod.GET)
    public ResultJson selectDriAcount() {
        List<DriverAmount> driverAmounts = driverAmountService.selectAllDriAcount();
        ResultJson result = new ResultJson(200, "SUCCESS", driverAmounts.size(), driverAmounts);
        return result;
    }


    @ApiOperation(value = "打印往来业务用量",notes = "打印往来业务用量")
    @RequestMapping(value = "/printContactsService", method = RequestMethod.GET)
    public ResultJson printContactsService() {
        List<ContactsService> contactsServices = contactsServiceService.printAllContactsService();
        ResultJson result = new ResultJson(200, "SUCCESS", contactsServices.size(), contactsServices);
        return result;
    }


    @ApiOperation(value = "打印往来业务用量-查询",notes = "打印往来业务用量-查询")
    @RequestMapping(value = "/selectContactsServiceByCode", method = RequestMethod.GET)
    public ContactsService selectContactsServiceByCode(@RequestParam("goodsBillCode") String goodsBillCode) {
        System.out.println("controller: " + goodsBillCode);
        ContactsService contactsService = contactsServiceService.selectByGoodsBillCode(goodsBillCode);
        return contactsService;
    }

    @ApiOperation(value = "打印专线整体",notes = "打印专线整体")
    @RequestMapping(value = "/printLineOverall", method = RequestMethod.GET)
    public ResultJson printLineOverall() {
        List<LineOverall> lineOveralls = lineOverallService.printAllLineOverall();
        ResultJson result = new ResultJson(200, "SUCCESS", lineOveralls.size(), lineOveralls);
        return result;
    }


    @ApiOperation(value = "打印车辆成本",notes = "打印车辆成本")
    @RequestMapping(value = "/printCar", method = RequestMethod.GET)
    public ResultJson printCar() {
        List<CarCost> carCosts = carCostService.printAllCarCost();
        ResultJson result = new ResultJson(200, "SUCCESS", carCosts.size(), carCosts);
        return result;
    }

    // /**
    //  * 打印车辆成本-查询
    //  */
    // @RequestMapping(value = "/selectCarCostByCode/{driverCode}", method = RequestMethod.GET)
    // public CarCost selectCarCostByCode(@PathVariable("driverCode") String driverCode) {
    //     CarCost carCost = moniterService.selectByCode(driverCode);
    //     return carCost;
    // }
}
