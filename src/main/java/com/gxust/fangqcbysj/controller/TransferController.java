package com.gxust.fangqcbysj.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.entity.Vo.TransferInfoVo;
import com.gxust.fangqcbysj.service.TransferInfoService;
import com.gxust.fangqcbysj.utils.ExcelExportUtil;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@RestController
@RequestMapping("/transfer")
@Slf4j
public class TransferController {

    @Autowired
    private TransferInfoService transferInfoService;

    @ApiOperation(value = "添加中转公司信息")
    @RequestMapping(value = "/addTransferCompany", method = RequestMethod.POST)
    public ResultJson addTransferCompany(@RequestBody TransferComInfo transferComInfo) {
        boolean flag = false;
        flag = transferInfoService.addCompany(transferComInfo);
        if (flag != false) {
            return ResultJson.ok();
        } else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "添加中转信息")
    @RequestMapping(value = "/addInfo", method = RequestMethod.POST)
    public ResultJson addTransfer(@RequestBody TransferInfo transferInfo) {
        boolean flag = false;
        flag = transferInfoService.addTransferInfo(transferInfo);
        if (flag != false) {
            return ResultJson.ok();
        }
        return ResultJson.failed();
    }

    @ApiOperation(value = "查询中转公司信息-分页")
    @RequestMapping(value = "/findByPage", method = RequestMethod.GET)
    public ResultJson findByPage(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize, TransferComInfo transferComInfo) {
        Page<TransferComInfo> page = transferInfoService.findAllByPage(pageNo, pageSize, transferComInfo);
        ResultJson result = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }


    @ApiOperation(value = "查询运单的中转详情")
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public TransferComInfo detail(@RequestParam("goodsBillCode") String goodsBillCode) {
        TransferComInfo transferComInfo = transferInfoService.findByGoodsBillCode(goodsBillCode);
        return transferComInfo;

    }

    @ApiOperation(value = "中转回告所需数据")
    @RequestMapping(value = "/findOnWayBills", method = RequestMethod.GET)
    public ResultJson findOnWayBills() {
        List<GoodsBill> list = transferInfoService.findOnWayBills();
        ResultJson result = new ResultJson(200, "SUCCESS", list.size(), list);
        return result;

    }

    @ApiOperation(value = "查询所有的中转信息")
    @RequestMapping(value = "/findInfoByPage", method = RequestMethod.POST)
    public ResultJson findInfoByPage(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize, TransferInfoVo transferInfoVo) {
        IPage<TransferInfo> page = transferInfoService.findInfoByPage(pageNo, pageSize, transferInfoVo);
        ResultJson result = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;

    }

    @ApiOperation(value = "导出所有的中转历史",notes = "导出所有的中转历史")
    @RequestMapping(value = "/findInfoByPage/export", method = RequestMethod.POST)
    public void export(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize, HttpServletRequest request , HttpServletResponse response,TransferInfoVo transferInfoVo){
        IPage<TransferInfo> page = transferInfoService.findInfoByPage(pageNo, pageSize, transferInfoVo);
        ExcelExportUtil.export(page.getRecords(),TransferInfo.class,"中转历史",request,response);
    }

    @ApiOperation(value = "查询所有用户的到货回执")
    @RequestMapping(value = "/findCusRes", method = RequestMethod.POST)
    public ResultJson findCusRes(@RequestParam("receiveGoodsPerson") String receiveGoodsPerson, @RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        IPage<CustomerReceiptInfo> page = transferInfoService.findCusRecPage(pageNo,pageSize,receiveGoodsPerson);
        ResultJson result = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }
}
