package com.gxust.fangqcbysj.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.service.ExtraIncomeService;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping("/check")
@Slf4j
public class CheckController {

    @Autowired
    private ExtraIncomeService extraIncomeService;

    /**
     * 录入营业外收入
     */
    @ApiOperation(value = "录入营业外收入",notes = "录入营业外收入")
    @RequestMapping(value = "/addExtraIncome", method = RequestMethod.POST, produces = "application/json")
    public ResultJson add(@RequestBody ExtraIncome extraIncome) {
        boolean flag = false;
        flag = extraIncomeService.addExtraIncome(extraIncome);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 查询所有营业外收入
     */
    @ApiOperation(value = "查询所有营业外收入",notes = "查询所有营业外收入")
    @RequestMapping(value = "/selectExtraIncome", method = RequestMethod.GET)
    public ResultJson selectAllExtra(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<ExtraIncome> page = extraIncomeService.selectAllExtra(pageNo, pageSize);
        ResultJson result = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    /**
     * 录入财务费用
     */
    @ApiOperation(value = "录入财务费用",notes = "录入财务费用")
    @RequestMapping(value = "/addFinanceFee", method = RequestMethod.POST, produces = "application/json")
    public ResultJson add(@RequestBody FinanceFee financeFee) {
        boolean flag = false;
        flag = extraIncomeService.addFinanceFee(financeFee);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 查询所有财务费用
     */
    @ApiOperation(value = "查询所有财务费用",notes = "查询所有财务费用")
    @RequestMapping(value = "/selectFinanceFee", method = RequestMethod.GET)
    public ResultJson selectAllFinance(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<FinanceFee> page = extraIncomeService.selectAllFinance(pageNo, pageSize);
        ResultJson result  = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    /**
     * 录入管理费用
     */
    @ApiOperation(value = "录入管理费用",notes = "录入管理费用")
    @RequestMapping(value = "/addManageFee", method = RequestMethod.POST, produces = "application/json")
    public ResultJson add(@RequestBody ManageFee manageFee) {
        boolean flag = false;
        flag = extraIncomeService.addManageFee(manageFee);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 查询所有管理费用
     */
    @ApiOperation(value = "查询所有管理费用",notes = "查询所有管理费用")
    @RequestMapping(value = "/selectManageFee", method = RequestMethod.GET)
    public ResultJson selectAllManageFee(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<ManageFee> page = extraIncomeService.selectAllManage(pageNo, pageSize);
        ResultJson result  = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    /**
     * 根据id查询管理费用
     */
    @ApiOperation(value = "根据id查询管理费用",notes = "根据id查询管理费用")
    @RequestMapping(value = "/findManageFee", method = RequestMethod.GET)
    public ManageFee selectByMId(@RequestParam("id")int id) {
        ManageFee manageFee = extraIncomeService.findManageFee(id);
        return manageFee;
    }

    /**
     * 录入员工工资
     */
    @ApiOperation(value = "录入员工工资",notes = "录入员工工资")
    @RequestMapping(value = "/addWage", method = RequestMethod.POST, produces = "application/json")
    public ResultJson add(@RequestBody EmployeeWage wage) {
        boolean flag = false;
        flag = extraIncomeService.addWage(wage);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 查询所有员工工资
     */
    @ApiOperation(value = "查询所有员工工资",notes = "查询所有员工工资")
    @RequestMapping(value = "/selectWage", method = RequestMethod.GET)
    public ResultJson selectAllWage(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<EmployeeWage> page = extraIncomeService.selectAllWage(pageNo, pageSize);
        ResultJson result  = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    /**
     * 根据员工编号查询员工工资
     */
    @ApiOperation(value = "根据员工编号查询员工工资",notes = "根据员工编号查询员工工资")
    @RequestMapping(value = "/findWage", method = RequestMethod.GET)
    public EmployeeWage selectByEmployeeCode(@RequestParam("employeeCode") String employeeCode) {
        EmployeeWage wage= extraIncomeService.selectByEmployeeCode(employeeCode);
        return wage;
    }

    /**
     * 查询当前月报
     */
    @ApiOperation(value = "查询当前月报",notes = "查询当前月报")
    @RequestMapping(value = "/selectIncomeMonthly", method = RequestMethod.GET)
    public IncomeMonthlyTemp selectAll() {
        IncomeMonthlyTemp incomeMonthlyTemp = extraIncomeService.selectIncomeMonthly();
        return incomeMonthlyTemp;
    }
}
