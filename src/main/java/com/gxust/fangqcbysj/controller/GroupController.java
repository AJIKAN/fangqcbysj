package com.gxust.fangqcbysj.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxust.fangqcbysj.dto.FunctionWithGroupDto;
import com.gxust.fangqcbysj.entity.Function;
import com.gxust.fangqcbysj.entity.FunctionWithGroup;
import com.gxust.fangqcbysj.entity.UserGroup;
import com.gxust.fangqcbysj.mapper.FunctionWithGroupMapper;
import com.gxust.fangqcbysj.service.FunctionService;
import com.gxust.fangqcbysj.service.FunctionWithGroupService;
import com.gxust.fangqcbysj.service.UserGroupService;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping(value = "/group")
@Slf4j
public class GroupController {

    @Autowired
    private UserGroupService userGroupService;

    @Autowired
    private FunctionService functionService;

    @Autowired
    private FunctionWithGroupService functionWithGroupService;

    @ApiOperation(value ="添加用户组",notes = "添加用户组")
    @RequestMapping(value = "/addGroup", method = RequestMethod.POST, produces = "application/json")
    public ResultJson addNewGroup(@RequestBody UserGroup userGroup) {
        boolean flag = false;
        flag = userGroupService.addGroup(userGroup);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "给用户组授权",notes = "给用户组授权")
    @RequestMapping(value = "/addNewFunc", method = RequestMethod.POST)
    public ResultJson addNewFunc(@RequestBody FunctionWithGroupDto functionWithGroupDto) {
        boolean flag = false;
        flag = userGroupService.addFuncGro(functionWithGroupDto);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "删除用户组",notes = "删除用户组")
    @RequestMapping(value = "/deleteGroup", method = RequestMethod.DELETE, produces = "application/json")
    public ResultJson deleteGroup(@RequestParam("id") int id) {
        boolean flag = false;
        flag = userGroupService.delFuncGro(id);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "查询用户组",notes = "查询用户组")
    @RequestMapping(value = "/selectAllGroup", method = RequestMethod.GET)
    public ResultJson selectAllGroup(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<UserGroup> page = userGroupService.findAllByPage(pageNo, pageSize);
        ResultJson result = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    @ApiOperation(value = "查询用户组详情",notes = "查询用户组详情")
    @RequestMapping(value = "/selectGroup", method = RequestMethod.GET)
    public ResultJson selectGroupById(@RequestParam("id") int id) {
        UserGroup userGroup = userGroupService.findOne(id);
        return ResultJson.ok(userGroup);
    }

    @ApiOperation(value = "用户组更新", notes = "根据用户组 id 更新用户组的描述")
    @RequestMapping(value = "/updateDescription", method = RequestMethod.PUT)
    public ResultJson updateDescription(@RequestParam("id") int id, @RequestParam("description") String description) {
        boolean flag = false;
        flag = userGroupService.updateUserGroup(id, description);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "查询所有用户组供添加职工使用", notes = "查询所有用户组")
    @RequestMapping(value = "/selectAllUserGroup", method = RequestMethod.GET)
    public List<UserGroup> selectAll() {
        List<UserGroup> groups = userGroupService.findAll();
        return groups;
    }

    @ApiOperation(value = "查询所有功能")
    @RequestMapping(value = "/selectAllFunction", method = RequestMethod.GET)
    public List<Function> selectAllFunction() {
        List<Function> functions = functionService.findAllFunction();
        return functions;
    }

    @ApiOperation(value = "查询所选组功能")
    @RequestMapping(value = "/selectFunctionByGroup", method = RequestMethod.GET)
    public List<FunctionWithGroup> selectFunctionByGroup(@RequestParam("groupId") int groupId) {
        List<FunctionWithGroup> functions = functionWithGroupService.findAllFunctionWithGroups(groupId);
        return functions;
    }

    @ApiOperation(value = "查询权限")
    @RequestMapping(value = "/selectFunc", method = RequestMethod.GET)
    public List<FunctionWithGroup> selectFunc(@RequestParam("loginId") String loginId) {
        List<FunctionWithGroup> functions = functionWithGroupService.findByLoginId(loginId);
        return functions;
    }
}
