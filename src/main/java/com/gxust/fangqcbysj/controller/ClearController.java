package com.gxust.fangqcbysj.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.entity.Vo.TransferInfoVo;
import com.gxust.fangqcbysj.service.DriverClearService;
import com.gxust.fangqcbysj.utils.ExcelExportUtil;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping("/clear")
@Slf4j
public class ClearController {

    @Autowired
    private DriverClearService driverClearService;

    /**
     * 司机结算-返回未结的所有实体(实体中能填的属性都填好)
     */
    @ApiOperation(value = "司机结算-返回未结的所有实体(实体中能填的属性都填好)",notes = "司机结算-返回未结的所有实体(实体中能填的属性都填好)")
    @RequestMapping(value = "/selectClearDriver", method = RequestMethod.GET)
    public ResultJson selectClearDri(@RequestParam("eventName") String eventName) {
        List<DriverClear> list = driverClearService.selectDrclear(eventName);
        ResultJson result = new ResultJson(200, "SUCCESS", list.size(), list);
        return result;
    }

    /**
     * 司机结算-通过订单编号查询单个实体的已填所有信息
     */
    @ApiOperation(value = "司机结算-通过订单编号查询单个实体的已填所有信息",notes = "司机结算-通过订单编号查询单个实体的已填所有信息")
    @RequestMapping(value = "/selectDriverClearByCode", method = RequestMethod.GET)
    public DriverClear selectDriverClearByCode(@RequestParam("goodsBillCode") String goodsBillCode) {
        DriverClear driverClear = driverClearService.selectByCargoReceiptCode(goodsBillCode);
        return driverClear;
    }

    /**
     * 司机结算（前台返回一个完整的实体）
     */
    @ApiOperation(value = "司机结算（前台返回一个完整的实体）",notes = "司机结算（前台返回一个完整的实体）")
    @RequestMapping(value = "/addDriClear", method = RequestMethod.PUT, produces = "application/json")
    public ResultJson addDriClear(@RequestBody DriverClear driverClear) {
        boolean flag = false;
        flag = driverClearService.driClear(driverClear);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 客户结算-返回未结的所有实体(实体中能填的属性都填好)
     */
    @ApiOperation(value = "客户结算-返回未结的所有实体(实体中能填的属性都填好)" ,notes = "客户结算-返回未结的所有实体(实体中能填的属性都填好)")
    @RequestMapping(value = "/selectclearCus", method = RequestMethod.GET)
    public ResultJson selecClearCus(@RequestParam("eventName") String eventName) {
        List<CustomerBillClear> list = driverClearService.selectCusclear(eventName);
        ResultJson result = new ResultJson(200, "SUCCESS", list.size(), list);
        return result;
    }

    @ApiOperation(value = "导出货运单结算历史",notes = "导出货运单结算历史")
    @RequestMapping(value = "/findInfoByPage/export", method = RequestMethod.POST)
    public void export(@RequestParam("eventName") String eventName , HttpServletRequest request , HttpServletResponse response){
        List<CustomerBillClear> list = driverClearService.selectCusclear(eventName);
        ExcelExportUtil.export(list,CustomerBillClear.class,"货运单结算历史",request,response);
    }

    /**
     * 客户结算-通过订单编号查询单个实体的已填所有信息
     */
    @ApiOperation(value = "客户结算-通过订单编号查询单个实体的已填所有信息",notes = "客户结算-通过订单编号查询单个实体的已填所有信息")
    @RequestMapping(value = "/selectCustomerBillClearByCode", method = RequestMethod.GET)
    public CustomerBillClear selectCustomerBillClearByCode(@RequestParam("goodsBillCode") String goodsBillCode) {
        CustomerBillClear customerBillClear = driverClearService.selectByBillCode(goodsBillCode);
        return customerBillClear;
    }

    /**
     * 客户结算（前台返回一个完整的实体）
     */
    @ApiOperation(value = "客户结算（前台返回一个完整的实体）",notes = "客户结算（前台返回一个完整的实体）")
    @RequestMapping(value = "/addCusClear", method = RequestMethod.PUT, produces = "application/json")
    public ResultJson addCusClear(@RequestBody CustomerBillClear customerBillClear) {
        boolean flag = false;
        flag = driverClearService.cusClear(customerBillClear);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 代收结算-返回未结的所有实体(实体中能填的属性都填好)
     */
    @ApiOperation(value = "代收结算-返回未结的所有实体(实体中能填的属性都填好)",notes = "代收结算-返回未结的所有实体(实体中能填的属性都填好)")
    @RequestMapping(value = "/selectClearHelp", method = RequestMethod.GET)
    public ResultJson selectClearHelp(@RequestParam("eventName") String eventName) {
        List<ProxyFeeClear> list = driverClearService.selectHelpclear(eventName);
        ResultJson result = new ResultJson(200, "SUCCESS", list.size(), list);
        return result;
    }

    /**
     * 代收结算-通过订单编号查询单个实体的已填所有信息
     */
    @ApiOperation(value = "代收结算-通过订单编号查询单个实体的已填所有信息",notes = "代收结算-通过订单编号查询单个实体的已填所有信息")
    @RequestMapping(value = "/selectHelpBillClearByCode", method = RequestMethod.GET)
    public ProxyFeeClear selectHelpBillClearByCode(@RequestParam("goodsBillCode") String goodsBillCode) {
        ProxyFeeClear proxyFeeClear = driverClearService.selectByGoodsBillCode(goodsBillCode);
        return proxyFeeClear;
    }

    /**
     * 代收结算（前台返回一个完整的实体）
     */
    @ApiOperation(value = "添加代收结算",notes = "添加代收结算")
    @RequestMapping(value = "/addCHelpClear", method = RequestMethod.PUT, produces = "application/json")
    public ResultJson addCHelpClear(@RequestBody ProxyFeeClear proxyFeeClear) {
        boolean flag = false;
        flag = driverClearService.helpClear(proxyFeeClear);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 杂费结算  add
     */
    @ApiOperation(value = "添加杂费结算",notes = "添加杂费结算")
    @RequestMapping(value = "/addExtraClear", method = RequestMethod.POST)
    public ResultJson addExtraClear(@RequestBody ExtraClear extraClear) {
        boolean flag = false;
        flag = driverClearService.saveExtraClear(extraClear);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 杂费结算  select
     */
    @ApiOperation(value = " 查询杂费结算",notes = "查询杂费结算")
    @RequestMapping(value = "/selectAllExtraClear", method = RequestMethod.GET)
    public ResultJson selectAllExtraClear(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<ExtraClear> page = driverClearService.selectAllExtraClearByPage(pageNo, pageSize);
        ResultJson result = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }
}
