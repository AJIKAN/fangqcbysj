package com.gxust.fangqcbysj.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.service.GoodsBillService;
import com.gxust.fangqcbysj.utils.ExcelExportUtil;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping("/goodsBill")
@Slf4j
public class GoodsBillController {

    @Autowired
    private GoodsBillService goodsBillService;

    @ApiOperation(value = "查询一个司机的所有运单(中转|到货)")
    @RequestMapping(value = "/transferGoods", method = RequestMethod.GET)
    public ResultJson transferGoods(@RequestParam("driverId") String driverId) {
        List<GoodsBill> list = goodsBillService.transferGoods("未到车辆", driverId);
        ResultJson result = new ResultJson(200, "SUCCESS", list.size(), list);
        return result;
    }

    @ApiOperation(value = "查询一个司机的所有到货运单")
    @RequestMapping(value = "/arriveGoods", method = RequestMethod.GET)
    public ResultJson arriveGoods(@RequestParam("driverId") String driverId) {
        List<GoodsBill> list = goodsBillService.arriveGoods("未到车辆", driverId);
        ResultJson result = new ResultJson(200, "SUCCESS", list.size(), list);
        return result;

    }

    /**
     * 填写一份货运单合同
     */
    @ApiOperation(value = "添加货运单", notes = "添加一个新货运单")
    @RequestMapping(value = "/addGoodsBill", method = RequestMethod.POST, produces = "application/json")
    public Map<?, ?> addGoodsBill(@RequestBody GoodsBill goodsBill) {
        return goodsBillService.addGoodsBill(goodsBill);
    }

    /**
     * 添加货物
     */
    @ApiOperation(value = "添加货物",notes = "添加货物")
    @RequestMapping(value = "/addGoods", method = RequestMethod.POST, produces = "application/json")
    public ResultJson addGoods(@RequestParam("goodsBillDetailId") String goodsBillDetailId,@RequestBody CargoReceiptDetail cargoReceiptDetail) {
        boolean flag = false;
        flag = goodsBillService.addGoods(goodsBillDetailId, cargoReceiptDetail);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 查询所有运单
     */
    @ApiOperation(value = "查询所有运单",notes = "查询所有运单")
    @RequestMapping(value = "/selectByEvent", method = RequestMethod.GET)
    public ResultJson selectAllGoodsBills(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<GoodsBillEvent> page = goodsBillService.selectAllGoogsBillByPage(pageNo, pageSize);
        ResultJson result = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    @ApiOperation(value = "导出所有运单",notes = "导出所有运单")
    @RequestMapping(value = "/selectByEvent/export", method = RequestMethod.POST)
    public void export(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize, HttpServletRequest request , HttpServletResponse response){
        Page<GoodsBillEvent> page = goodsBillService.selectAllGoogsBillByPage(pageNo, pageSize);
        ExcelExportUtil.export(page.getRecords(),GoodsBillEvent.class,"所有运单",request,response);
    }

    /**
     * 查询运单状态
     */
    @ApiOperation(value = "查询运单状态",notes = "查询运单状态")
    @RequestMapping(value = "/selectByEventName", method = RequestMethod.GET)
    public ResultJson selectGoodsBillByEvent(@RequestParam("eventName") String eventName, @RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<GoodsBillEvent> page = goodsBillService.selectGoodsBillByEvent(eventName ,pageNo, pageSize);
        ResultJson result = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    /**
     * 通过id查询单个货运单
     */
    @ApiOperation(value = "通过id查询单个货运单",notes = "通过id查询单个货运单")
    @RequestMapping(value = "/selectByCode", method = RequestMethod.GET)
    public GoodsBill selectGoodsBillByCode(@RequestParam("goodsBillCode") String goodsBillCode) {
        GoodsBill goodsBill = goodsBillService.selectByGoodsBillCode(goodsBillCode);
        return goodsBill;
    }

    /**
     * 修改货运单
     */
    @ApiOperation(value = "修改货运单",notes = "修改货运单")
    @RequestMapping(value = "/updateByCode", method = RequestMethod.PUT)
    public ResultJson updateGoodsBill(@RequestBody GoodsBill goodsBill) {
        boolean flag = false;
        flag = goodsBillService.updateGoodsBill(goodsBill);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 删除货运单
     */
    @ApiOperation(value = "删除货运单",notes = "删除货运单")
    @RequestMapping(value = "/deleteByCode", method = RequestMethod.POST)
    public ResultJson deleteGoodsBill(@RequestParam("goodsBillCode") String goodsBillCode) {
        boolean flag = false;
        flag = goodsBillService.deleteGoodsBill(goodsBillCode);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "获取一个用户的待收货物")
    @RequestMapping(value = "/findWait", method = RequestMethod.GET)
    public ResultJson findWaitReceived(@RequestParam("customerCode") String customerCode) {
        List<GoodsBill> list = goodsBillService.findWaitReceive(customerCode);
        ResultJson result = new ResultJson(200, "SUCCESS", list.size(), list);
        return result;
    }

    @ApiOperation(value = "获取所有未发过 {提货 | 到货 | 中转 | 已提 | 代收} 回告的运单")
    @RequestMapping(value = "/findInform", method = RequestMethod.GET)
    public ResultJson findInform(@RequestParam("billType") String billType, @RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        List<GoodsBill> page = goodsBillService.findInformGet(billType, pageNo,pageSize);
        ResultJson result = new ResultJson(200, "SUCCESS", page.size(), page);
        return result;
    }

    @ApiOperation(value = "获取所有已发过 {提货 | 到货 | 中转 | 已提 | 代收} 回告的运单")
    @RequestMapping(value = "/findOldInform", method = RequestMethod.GET)
    public ResultJson findOldInform(@RequestParam("type") String type, @RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        List<GoodsBill> page = goodsBillService.findOldInform(type, pageNo,pageSize);
        ResultJson result = new ResultJson(200, "SUCCESS", page.size(), page);
        return result;
    }

    @ApiOperation(value = "获取已提货的运单")
    @RequestMapping(value = "/findAllGot", method = RequestMethod.GET)
    public ResultJson findAllGot(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        List<GoodsBill> page = goodsBillService.findAllGot(pageNo, pageSize);
        ResultJson result = new ResultJson(200, "SUCCESS", (int) page.size(), page);
        return result;
    }
}
