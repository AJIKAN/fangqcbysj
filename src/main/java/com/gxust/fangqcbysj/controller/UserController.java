package com.gxust.fangqcbysj.controller;

import com.gxust.fangqcbysj.entity.User;
import com.gxust.fangqcbysj.service.UserService;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author 方全朝
 * @Description 用户相关接口
 * @Date 2021/3/14
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "修改密码",notes = "修改密码")
   @RequestMapping(value = "/change", method = RequestMethod.PUT)
   public ResultJson change(@RequestParam("loginId") String loginId,@RequestParam("oldPassword") String oldPassword,@RequestParam("newPassword") String newPassword) {
       boolean flag = false;
       flag = userService.changePassword(loginId, oldPassword, newPassword);
       if (!flag) {
           return ResultJson.ok();
       }
       return ResultJson.failed();
   }

    @ApiOperation(value = "登录",notes = "登录")
    @PostMapping("/login")
    public @ResponseBody Object login(@RequestParam(value = "loginId") String loginId, @RequestParam(value = "password") String password){
        if (loginId != null && password != null){
            User user = userService.login(loginId, password);
            return ResultJson.ok(user);
        }
        return ResultJson.failed();
    }

    @ApiOperation(value="替换用户名",notes = "替换用户名")
    @PostMapping("/replace")
    public ResultJson replaceName(@RequestParam(value = "loginId") String loginId){
        String name = "";
        if (loginId != null) {
            if (loginId.contains("GL")){
                name = "管理员";
            }else if (loginId.contains("PJ")){
                name = "票据组用户";
            }else if (loginId.contains("CW")){
                name = "财务组用户";
            }else if (loginId.contains("SJ")){
                name = "司机";
            }else if (loginId.contains("KF")){
                name = "客服组用户";
            }else if (loginId.contains("KH")){
                name = "客户";
            }
        }
        return ResultJson.ok(name);
    }
}
