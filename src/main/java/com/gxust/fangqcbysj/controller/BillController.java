package com.gxust.fangqcbysj.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxust.fangqcbysj.entity.BillInfo;
import com.gxust.fangqcbysj.entity.BillRelease;
import com.gxust.fangqcbysj.entity.CargoReceipt;
import com.gxust.fangqcbysj.entity.GoodsReceiptInfo;
import com.gxust.fangqcbysj.service.BillReleaseService;
import com.gxust.fangqcbysj.utils.ExcelExportUtil;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping("/bill")
@Slf4j
public class BillController {

    @Autowired
    private BillReleaseService billReleaseService;

    @ApiOperation(value = "分发", notes = "分发 - 添加一条单据分发信息")
    @RequestMapping(value = "/addRelease", method = RequestMethod.POST)
    public ResultJson addRelease(@RequestBody BillRelease billRelease) {
        boolean flag = false;
        flag = billReleaseService.addRelease(billRelease);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "到货", notes = "到货 - 添加一条货物到货回执信息")
    @RequestMapping(value = "/addArrived", method = RequestMethod.POST)
    public ResultJson addArrived(@RequestBody GoodsReceiptInfo goodsReceiptInfo) {
        boolean flag = false;
        flag = billReleaseService.addGoodsReceipt(goodsReceiptInfo);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "分页查询单据信息", notes = "分页查询单据信息")
    @RequestMapping(value = "/findByPage", method = RequestMethod.GET)
    public ResultJson findAllByPage(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<BillInfo> page = billReleaseService.findAllByPage(pageNo, pageSize);
        ResultJson result  = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    @ApiOperation(value = "导出单据信息",notes = "导出单据信息")
    @RequestMapping(value = "/findByPage/export", method = RequestMethod.POST)
    public void export(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize, HttpServletRequest request , HttpServletResponse response){
        Page<BillInfo> page = billReleaseService.findAllByPage(pageNo, pageSize);
        ExcelExportUtil.export(page.getRecords(),BillInfo.class,"单据信息",request,response);
    }

    @ApiOperation(value = "查询未分发的运单信息", notes = "查询未分发的运单信息")
    @RequestMapping(value = "/findNotRelease", method = RequestMethod.GET)
    public ResultJson findNotRelease(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<BillInfo> page = billReleaseService.findNotRelease(pageNo, pageSize);
        ResultJson result  = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }
}
