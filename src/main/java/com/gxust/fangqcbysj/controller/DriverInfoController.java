package com.gxust.fangqcbysj.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxust.fangqcbysj.entity.DriverInfo;
import com.gxust.fangqcbysj.service.DriverInfoService;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping("/driverInfo")
@Slf4j
public class DriverInfoController {

    @Autowired
    private DriverInfoService driverInfoService;

    @ApiOperation(value = "新增司机信息", notes = "前台信息封装后进行添加司机信息操作")
    @RequestMapping(value = "/addDriver", method = RequestMethod.POST)
    public ResultJson addNewDriver(@RequestBody DriverInfo driverInfo) {
        boolean flag = false;
        flag = driverInfoService.addNewDriver(driverInfo);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "删除一个司机信息", notes = "通过 id 删除司机信息")
    @RequestMapping(value = "/deleteDriver", method = RequestMethod.DELETE)
    public ResultJson delete(@RequestParam("id") String id) {
        boolean flag = false;
        flag = driverInfoService.deleteById(id);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "修改一个司机信息", notes = "通过 id 修改司机信息")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResultJson update(@RequestParam("id") String id,@RequestBody DriverInfo driverInfo) {
        boolean flag = false;
        flag = driverInfoService.updateDriverById(id, driverInfo);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "分页查询司机", notes = "通过页码和limit查询司机信息")
    @RequestMapping(value = "/selectAllByPage", method = RequestMethod.GET)
    public ResultJson selectAllByPage(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<DriverInfo> page = driverInfoService.selectAllEmpByPage(pageNo, pageSize);
        ResultJson result = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    @ApiOperation(value = "查询一个司机信息", notes = "通过 id 查询司机信息")
    @RequestMapping(value = "/selectById", method = RequestMethod.GET)
    public DriverInfo selectById(@RequestParam("id") String id) {
        DriverInfo driverInfo = driverInfoService.findDriverById(id);
        return driverInfo;
    }

    @ApiOperation(value = "查询所有司机 id", notes = "查询所有司机 id")
    @RequestMapping(value = "/selectAllId", method = RequestMethod.GET)
    public List<String> selectAllId() {
        List<String> list = driverInfoService.findDriverAllId();
        return list;
    }
}
