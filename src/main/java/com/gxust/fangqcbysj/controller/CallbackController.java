package com.gxust.fangqcbysj.controller;

import com.gxust.fangqcbysj.entity.CallbackInfo;
import com.gxust.fangqcbysj.service.CallbackInfoService;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping("/callback")
@Slf4j
public class CallbackController {

    @Autowired
    private CallbackInfoService callbackInfoService;

    @ApiOperation(value = "提货回告-新单回告")
    @RequestMapping(value = "/addInfo", method = RequestMethod.POST)
    public ResultJson addInfo(@RequestBody CallbackInfo callbackInfo) {
        boolean flag = false;
        flag = callbackInfoService.addInfo(callbackInfo);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "查询一条回告信息")
    @RequestMapping(value = "/findDetail", method = RequestMethod.GET)
    public CallbackInfo findDetail(@RequestParam("goodsBillId") String goodsBillId, @RequestParam("type") String type) {
        CallbackInfo callbackInfo = callbackInfoService.findDetail(goodsBillId, type);
        return callbackInfo;
    }
}
