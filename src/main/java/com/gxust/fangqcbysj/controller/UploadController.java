package com.gxust.fangqcbysj.controller;

import com.gxust.fangqcbysj.utils.CommonUtils;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author 方全朝
 * @Description
 * @Date 2021/3/14
 */
public class UploadController {

    /**
     * 图片上传接口
     *
     * @return
     */
    @ApiOperation(value = "图片/文件 上传接口", httpMethod = "POST")
    @PostMapping("/uploadFile")
    public ResultJson uploadFile(@RequestParam("upload") MultipartFile upload){
        String path = CommonUtils.uploadFile(upload);
        System.out.println("图片url路径：" + path + "");
        return ResultJson.ok(path);
    }
}
