package com.gxust.fangqcbysj.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxust.fangqcbysj.entity.CargoReceipt;
import com.gxust.fangqcbysj.entity.CargoReceiptDetail;
import com.gxust.fangqcbysj.entity.GoodsBill;
import com.gxust.fangqcbysj.entity.GoodsBillEvent;
import com.gxust.fangqcbysj.service.CargoReceiptService;
import com.gxust.fangqcbysj.utils.ExcelExportUtil;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping("/cargoReceipt")
@Slf4j
public class CargoReceiptController {

    @Autowired
    private CargoReceiptService cargoReceiptService;

    @ApiOperation(value = "添加货运回执单主表", notes = "添加货运回执单主表")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json")
    public ResultJson add(@RequestBody CargoReceipt cargoReceipt) {
        boolean flag = false;
        flag = cargoReceiptService.addCargoReceipt(cargoReceipt);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 查询货运回执单编号
     */
    @ApiOperation(value = "查询货运回执单编号",notes = "查询货运回执单编号")
    @RequestMapping(value = "/selectCode", method = RequestMethod.GET)
    public List<?> selectAllCodes() {
        List<String> codes = cargoReceiptService.selectAllCodes();
        return codes;
    }

    @ApiOperation(value = "查询未被安排的货运单")
    @RequestMapping(value = "/selectLeftCodes", method = RequestMethod.GET)
    public List<?> selectLeftCodes() {
        List<?> codes = cargoReceiptService.selectLeftCodes();
        return codes;
    }

    /**
     * 通过货运回执单查询客户信息
     */
    @ApiOperation(value = "通过货运回执单查询客户信息",notes = "通过货运回执单查询客户信息")
    @RequestMapping(value = "/findGoodsBill", method = RequestMethod.GET)
    public GoodsBill selectGoodsBill(@RequestParam("goodsRevertBillCode") String goodsRevertBillCode) {
        GoodsBill goodsBill = cargoReceiptService.selectGoodsBill(goodsRevertBillCode);
        return goodsBill;
    }

    /**
     * 查询所有运单
     */
    @ApiOperation(value = "查询所有运输合同")
    @RequestMapping(value = "/selectAll", method = RequestMethod.GET)
    public ResultJson selectAll(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<CargoReceipt> page = cargoReceiptService.selectAll(pageNo, pageSize);
        ResultJson result  = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    @ApiOperation(value = "导出所有运输合同",notes = "导出所有运输合同")
    @RequestMapping(value = "/selectAll/export", method = RequestMethod.POST)
    public void export(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize, HttpServletRequest request , HttpServletResponse response){
        Page<CargoReceipt> page = cargoReceiptService.selectAll(pageNo, pageSize);
        ExcelExportUtil.export(page.getRecords(),CargoReceipt.class,"运输合同",request,response);
    }

    /**
     * 查询运单状态
     */
    @ApiOperation(value = "根据状态查询运输合同")
    @RequestMapping(value = "/selectByEvent", method = RequestMethod.GET)
    public ResultJson selectByEvent(@RequestParam("backBillState") String backBillState, @RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<CargoReceipt> page = cargoReceiptService.selectByEvent(backBillState, pageNo, pageSize);
        ResultJson result  = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    /**
     * 通过id查询单个货运单
     */
    @ApiOperation(value = "通过id查询单个货运单",notes = "通过id查询单个货运单")
    @RequestMapping(value = "/selectByCode", method = RequestMethod.GET)
    public CargoReceipt selectByCode(@RequestParam("goodsRevertBillCode") String goodsRevertBillCode) {
        CargoReceipt cargoReceipt = cargoReceiptService.selectByCode(goodsRevertBillCode);
        return cargoReceipt;
    }

    /**
     * 修改货运回执单
     */
    @ApiOperation(value = "修改货运回执单",notes = "修改货运回执单")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResultJson update(@RequestBody CargoReceipt cargoReceipt) {
        boolean flag = false;
        flag = cargoReceiptService.updateCargoReceipt(cargoReceipt);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 提交货运回执单
     */
    @ApiOperation(value = "提交货运回执单",notes = "提交货运回执单")
    @RequestMapping(value = "/submit", method = RequestMethod.PUT)
    public ResultJson submit(@RequestBody CargoReceipt cargoReceipt) {
        boolean flag = false;
        flag = cargoReceiptService.submitCargoReceipt(cargoReceipt);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    /**
     * 删除货运回执单
     */
    @ApiOperation(value = "删除货运回执单",notes = "删除货运回执单")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = "application/json")
    public ResultJson delete(@RequestParam("goodsRevertBillCode") String goodsRevertBillCode) {
        boolean flag = false;
        flag = cargoReceiptService.deleteCargoReceipt(goodsRevertBillCode);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "查询某个司机的所有运输合同", notes = "通过 id 查询司机的所有运单")
    @RequestMapping(value = "/findByDriver", method = RequestMethod.GET)
    public List<CargoReceipt> findByDriverId(@RequestParam("id") String driverId) {
        List<CargoReceipt> cargoReceipts = cargoReceiptService.findByDriverId(driverId);
        return cargoReceipts;
    }

    @ApiOperation(value = "查询一个司机的所有未到运单", notes = "通过 id 和状态查询司机的所有未到运单")
    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public ResultJson findByDriverId(@RequestParam("driverId") String driverId, @RequestParam("state") String backBillState, @RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<CargoReceipt> page = cargoReceiptService.findByDriverIdAndState(driverId, backBillState, pageNo, pageSize);
        ResultJson result  = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    @ApiOperation(value = "查询货运回执单号")
    @RequestMapping(value = "/findRevertId", method = RequestMethod.GET)
    public String findGoodsRevertCode(@RequestParam("goodsBillCode") String goodsBillCode) {
        CargoReceiptDetail cargoReceiptDetail = cargoReceiptService.findByGoodsBillCode(goodsBillCode);
        return cargoReceiptDetail.getGoodsRevertBillId();
    }
}
