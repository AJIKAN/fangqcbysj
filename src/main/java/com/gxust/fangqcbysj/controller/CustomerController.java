package com.gxust.fangqcbysj.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxust.fangqcbysj.entity.CustomerInfo;
import com.gxust.fangqcbysj.entity.Vo.CustomerReceiptInfoVo;
import com.gxust.fangqcbysj.service.CustomerInfoService;
import com.gxust.fangqcbysj.service.CustomerReceiptInfoService;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping("/customer")
@Slf4j
public class CustomerController {

    @Autowired
    private CustomerReceiptInfoService customerReceiptInfoService;

    @Autowired
    private CustomerInfoService customerInfoService;


    @ApiOperation(value = "添加顾客回执")
    @RequestMapping(value = "/addCusRec", method = RequestMethod.POST)
    public ResultJson addCusRec(@RequestBody CustomerReceiptInfoVo customerReceiptInfoVo) {
        boolean flag = false;
        flag = customerReceiptInfoService.addCustomerReceiptInfo(customerReceiptInfoVo);
        if (flag != false) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "添加顾客", notes = "添加一个新顾客")
    @RequestMapping(value = "/addCus", method = RequestMethod.POST, produces = "application/json")
    public ResultJson addNewCus(@RequestBody CustomerInfo customer) {
        boolean flag = false;
        flag = customerInfoService.addCustomer(customer);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "删除客户",notes = "删除客户")
    @RequestMapping(value = "/deleteCus", method = RequestMethod.DELETE, produces = "application/json")
    public ResultJson deleteCus(@RequestParam("customerCode") String customerCode) {
        boolean flag = false;
        flag = customerInfoService.deleteCustomer(customerCode);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "客户查询",notes = "客户查询")
    @RequestMapping(value = "/selectAllCus", method = RequestMethod.GET)
    public ResultJson selectAllCus(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<CustomerInfo> page = customerInfoService.selectAllCusByPage(pageNo, pageSize);
        ResultJson result = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    @ApiOperation(value = "查询客户详情",notes = "查询客户详情")
    @RequestMapping(value = "/selectCusByCode", method = RequestMethod.GET)
    public CustomerInfo selectCusByCode(@RequestParam("customerCode") String customerCode) {
        CustomerInfo customerInfo = customerInfoService.selectByCustomerCode(customerCode);
        return customerInfo;
    }

    @ApiOperation(value = "填写接货单，查询客户",notes = "填写接货单，查询客户")
    @RequestMapping(value = "/selectAllCusCode", method = RequestMethod.GET)
    public List<?> selectAllCusCode() {
        List<String> cusCodes = customerInfoService.selectAllCusCode();
        return cusCodes;
    }

    @ApiOperation(value = "顾客信息更新", notes = "根据顾客 id 更新顾客的信息")
    @RequestMapping(value = "/updateCustomerInfo", method = RequestMethod.PUT)
    public ResultJson updateCustomerInfo(@RequestParam("customerCode") String customerCode,@RequestBody CustomerInfo customerInfo) {
        boolean flag = false;
        flag = customerInfoService.updateCustomer(customerCode, customerInfo);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

}
