package com.gxust.fangqcbysj.controller;

import com.gxust.fangqcbysj.entity.GoodsReceiptInfo;
import com.gxust.fangqcbysj.service.GoodsReceiptInfoService;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping(value = "/goodsReceipt")
@Slf4j
public class GoodsReceiptController {

    @Autowired
    private GoodsReceiptInfoService goodsReceiptInfoService;

    @ApiOperation(value = "新增一条司机回执信息", notes = "新增一条司机回执信息")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResultJson add(@RequestBody GoodsReceiptInfo goodsReceiptInfo) {
        boolean flag = false;
        flag = goodsReceiptInfoService.addGoodsReceipt(goodsReceiptInfo);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }
}
