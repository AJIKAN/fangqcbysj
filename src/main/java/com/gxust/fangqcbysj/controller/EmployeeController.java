package com.gxust.fangqcbysj.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxust.fangqcbysj.dto.EmployeeDto;
import com.gxust.fangqcbysj.entity.Employee;
import com.gxust.fangqcbysj.service.EmployeeService;
import com.gxust.fangqcbysj.service.UserService;
import com.gxust.fangqcbysj.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@RestController
@RequestMapping("/employee")
@Slf4j
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    UserService userService;

    @ApiOperation(value = "添加职员", notes = "添加一个新职员工")
    @RequestMapping(value = "/addEmp", method = RequestMethod.POST, produces = "application/json")
    public ResultJson addNewEmp(@RequestBody EmployeeDto employeeDto) {
        boolean flag = false;
        flag = employeeService.addEmployee(employeeDto);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "删除员工", notes = "删除员工")
    @RequestMapping(value = "/deleteEmp", method = RequestMethod.DELETE, produces = "application/json")
    public ResultJson deleteEmp(@Param("employeeCode") String employeeCode) {
        boolean flag = false;
        flag = employeeService.deleteEmployee(employeeCode);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }

    @ApiOperation(value = "查询员工",notes = "查询员工")
    @RequestMapping(value = "/selectAllEmp", method = RequestMethod.GET)
    public ResultJson selectAllEmp(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize) {
        Page<Employee> page = employeeService.selectAllEmpByPage(pageNo, pageSize);
        ResultJson result = new ResultJson(200, "SUCCESS", (int) page.getTotal(), page.getRecords());
        return result;
    }

    @ApiOperation(value = "查询员工详情", notes = "查询员工详情")
    @RequestMapping(value = "/selectEmpByCode", method = RequestMethod.GET)
    public Map<?, ?> selectEmpByCode(@RequestParam("employeeCode") String employeeCode) {
        Employee employee = employeeService.selectByEmployeeCode(employeeCode);
        Map<String, Object> map = new HashMap<>();
        map.put("employee", employee);
        boolean flag = userService.ifExist(employeeCode);
        map.put("condition", flag == true ? 1 : 0);
        return map;
    }

    @ApiOperation(value = "更新员工",notes = "更新员工")
    @RequestMapping(value = "/updateEmp", method = RequestMethod.PUT)
    public ResultJson updateEmp(@RequestBody EmployeeDto employeeDto) {
        boolean flag = false;
        flag = employeeService.updateEmployee(employeeDto);
        if (flag == true) {
            return ResultJson.ok();
        }else {
            return ResultJson.failed();
        }
    }
}
