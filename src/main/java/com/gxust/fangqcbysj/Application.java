package com.gxust.fangqcbysj;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author 方全朝
 * @Description 主启动
 * @Date 2021/3/14
 */
@SpringBootApplication
@MapperScan(basePackages = "com.gxust.fangqcbysj.mapper")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
