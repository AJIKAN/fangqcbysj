package com.gxust.fangqcbysj.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/4/24
 */
@Data
public class EmployeeDto {

    /**
     * employee_code
     */
    @ApiModelProperty("employee_code")
    @TableId(value="employee_code")
    private String employeeCode;

    /**
     * birthday
     */
    @ApiModelProperty("birthday")
    @TableField(value="birthday")
    private Date birthday;

    /**
     * department
     */
    @ApiModelProperty("department")
    @TableField(value="department")
    private String department;

    /**
     * employee_name
     */
    @ApiModelProperty("employee_name")
    @TableField(value="employee_name")
    private String employeeName;

    /**
     * gender
     */
    @ApiModelProperty("gender")
    @TableField(value="gender")
    private String gender;

    /**
     * position
     */
    @ApiModelProperty("position")
    @TableField(value="position")
    private String position;

    private int condition;
}
