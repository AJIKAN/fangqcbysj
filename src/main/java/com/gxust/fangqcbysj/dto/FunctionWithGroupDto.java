package com.gxust.fangqcbysj.dto;

import lombok.Data;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/4/24
 */
@Data
public class FunctionWithGroupDto {

    int groupId;

    int[] array;
}
