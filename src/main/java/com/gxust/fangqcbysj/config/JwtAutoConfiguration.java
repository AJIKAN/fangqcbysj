package com.gxust.fangqcbysj.config;

import com.gxust.fangqcbysj.constant.JwtProperties;
import com.gxust.fangqcbysj.utils.JwtUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Configuration
@EnableConfigurationProperties(JwtProperties.class)
@ConditionalOnProperty(name = "jwt.enabled", matchIfMissing = true)
public class JwtAutoConfiguration {

    @Bean
    @Primary
    @ConditionalOnMissingBean
    public JwtUtil JwtUtil() {
        return new JwtUtil();
    }

}
