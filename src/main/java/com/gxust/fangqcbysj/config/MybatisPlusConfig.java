package com.gxust.fangqcbysj.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.core.parser.ISqlParser;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author 方全朝
 * @Description MybatisPlus 配置
 * @Date 2021/3/14
 */
@MapperScan("com.gxust.fangqcbysj.mapper")
@EnableTransactionManagement
@Configuration
public class MybatisPlusConfig {

    // @Bean
    // public OptimisticLockerInterceptor optimisticLockerInterceptor(){
    //     return new OptimisticLockerInterceptor();
    // }
    //
    // @Bean
    // public PaginationInterceptor paginationInterceptor(){
    //     PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
    //     List<ISqlParser> sqlParserList = new ArrayList<>();
    //     paginationInterceptor.setSqlParserList(sqlParserList);
    //     return paginationInterceptor;
    // }

    // @Bean(name="dataSource")
    // @ConfigurationProperties(prefix = "spring.datasource")
    // @Primary
    // public DataSource mysqlDatasource(){
    //     DataSource datasource = DataSourceBuilder.create().type(DruidDataSource.class).build();
    //     return datasource;
    // }


    @Bean
    public MybatisPlusInterceptor MybatisPlusInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        //3.4.0版本 乐观锁
        mybatisPlusInterceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        //3.4.0版本 分页配置
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return mybatisPlusInterceptor;
    }

    // @Primary
    // @Bean(name = "mybatisSqlSessionFactory")
    // public SqlSessionFactory mybatisSqlSessionFactory(@Qualifier("dataSource") DataSource dataSource)
    //         throws Exception {
    //     MybatisSqlSessionFactoryBean bean = new MybatisSqlSessionFactoryBean();
    //     bean.setDataSource(dataSource);
    //     // bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:com.gxust.fangqcbysj.mapper.xml/*.xml"));
    //     return bean.getObject();
    // }
}
