package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.*;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface ExtraIncomeService extends IService<ExtraIncome> {

    boolean addExtraIncome(ExtraIncome extraIncome);

    Page<ExtraIncome> selectAllExtra(int pageNo, int pageSize);

    boolean addFinanceFee(FinanceFee financeFee);

    Page<FinanceFee> selectAllFinance(int pageNo, int pageSize);

    boolean addManageFee(ManageFee manageFee);

    Page<ManageFee> selectAllManage(int pageNo, int pageSize);

    ManageFee findManageFee(int id);

    boolean addWage(EmployeeWage wage);

    Page<EmployeeWage> selectAllWage(int pageNo, int pageSize);

    EmployeeWage selectByEmployeeCode(String employeeCode);

    IncomeMonthlyTemp selectIncomeMonthly();

    List<ExtraIncome> selectByIncomeMonth(String incomeMonth);

    List<FinanceFee> selectByFPayoutMonth(String payoutMonth);

    List<ManageFee> selectByMPayoutMonth(String payoutMonth);

    List<EmployeeWage> selectByDate(String beginTime,String endTime);

    List<ExtraClear> selectByBalanceDate(String beginTime,String endTime);

    IncomeMonthlyTemp selectByMonth(String month);
}
