package com.gxust.fangqcbysj.service;

/**
 * @Description:
 * @Author: 方全朝
 * @Date: 2021/5/7
 */
public interface SendSms {
    public boolean send(String phoneNumber,String templateCode);
}
