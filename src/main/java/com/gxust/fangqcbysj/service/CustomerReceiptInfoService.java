package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.CustomerReceiptInfo;
import com.gxust.fangqcbysj.entity.Vo.CustomerReceiptInfoVo;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface CustomerReceiptInfoService extends IService<CustomerReceiptInfo> {

    /**
     * 添加客户回执
     * @param customerReceiptInfo customerReceiptInfo
     * @return boolean
     */
    boolean addCustomerReceiptInfo(CustomerReceiptInfoVo customerReceiptInfoVo);
}
