package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.FunctionWithGroup;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface FunctionWithGroupService extends IService<FunctionWithGroup> {

    List<FunctionWithGroup> findAllFunctionWithGroups(int groupId);

    List<FunctionWithGroup> findByLoginId(String loginId);
}
