package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.CargoReceipt;
import com.gxust.fangqcbysj.entity.CargoReceiptDetail;
import com.gxust.fangqcbysj.entity.GoodsBill;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface CargoReceiptService extends IService<CargoReceipt> {

    boolean addCargoReceipt(CargoReceipt cargoReceipt);

    List<String> selectAllCodes();

    List<?> selectLeftCodes();

    GoodsBill selectGoodsBill(String goodsRevertBillCode);

    Page<CargoReceipt>  selectAll(int pageNo, int pageSize);

    Page<CargoReceipt> selectByEvent(String backBillState,int pageNo,int pageSize);

    CargoReceipt selectByCode(String goodsRevertBillCode);

    boolean updateCargoReceipt(CargoReceipt cargoReceipt);

    boolean submitCargoReceipt(CargoReceipt cargoReceipt);

    boolean deleteCargoReceipt(String goodsRevertBillCode);

    List<CargoReceipt> findByDriverId(String driverId);

    Page<CargoReceipt> findByDriverIdAndState(String driverId,String backBillState,int pageNo,int pageSize);

    CargoReceiptDetail findByGoodsBillCode(String goodsBillCode);
}
