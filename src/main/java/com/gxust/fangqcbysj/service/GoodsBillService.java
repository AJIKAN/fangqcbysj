package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.CargoReceiptDetail;
import com.gxust.fangqcbysj.entity.GoodsBill;
import com.gxust.fangqcbysj.entity.GoodsBillEvent;

import java.util.List;
import java.util.Map;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface GoodsBillService extends IService<GoodsBill> {

    /**
     * 查询未到车辆
     * @param driverId ID
     * @return List<GoodsBill>
     */
    List<GoodsBill> transferGoods(String type, String driverId);

    /**
     * 查询未到车辆
     * @param driverId ID
     * @return List<GoodsBill>
     */
    List<GoodsBill> arriveGoods(String type, String driverId);

    /**
     * 预期未到运单
     * @return List<GoodsBill>
     */
    List<GoodsBill> selectAllUnArrive();

    /**
     * 滞留未取货运单
     * @return List<GoodsBill>
     */
    List<GoodsBill> selectAllUnTake();

    Map<?, ?> addGoodsBill(GoodsBill goodsBill);

    boolean addGoods(String goodsBillDetailId, CargoReceiptDetail cargoReceiptDetail);

    Page<GoodsBillEvent> selectAllGoogsBillByPage(int pageNo,int pageSize);

    Page<GoodsBillEvent> selectGoodsBillByEvent(String eventName ,int pageNo,int pageSize);

    GoodsBill selectByGoodsBillCode(String goodsBillCode);

    boolean updateGoodsBill(GoodsBill goodsBill);

    boolean deleteGoodsBill(String goodsBillCode);

    List<GoodsBill> findWaitReceive(String customerCode);

    List<GoodsBill> findInformGet(String billType,int pageNo,int pageSize);

    List<GoodsBill> findOldInform(String type,int pageNo,int pageSize);

    List<GoodsBill> findAllGot(int pageNo,int pageSize);
}
