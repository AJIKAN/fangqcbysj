package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.mapper.DriverInfoMapper;
import com.gxust.fangqcbysj.mapper.UserGroupMapper;
import com.gxust.fangqcbysj.mapper.UserMapper;
import com.gxust.fangqcbysj.mapper.UserWithGroupMapper;
import com.gxust.fangqcbysj.service.DriverInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.geom.QuadCurve2D;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class DriverInfoServiceImpl extends ServiceImpl<DriverInfoMapper, DriverInfo> implements DriverInfoService {

    @Autowired
    private DriverInfoMapper driverInfoMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserGroupMapper userGroupMapper;

    @Autowired
    private UserWithGroupMapper userWithGroupMapper;

    @Override
    public boolean addNewDriver(DriverInfo driverInfo) {
        driverInfo.setState("空闲");
        String driverCode = "SJ";
        while (true) {
            driverCode += randomCode();
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("id",driverCode);
            DriverInfo driverInfo1 = driverInfoMapper.selectOne(queryWrapper);
            if (driverInfo1 == null) {
                break;
            }
        }
        driverInfo.setId(driverCode);
        try {
            // 添加用户信息表
            User user = new User(driverCode, "E10ADC3949BA59ABBE56E057F20F883E", 0);
            userMapper.insert(user);
            // 添加用户与组情况
            String department = "司机组";
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("group_name",department);
            UserGroup userGroup = userGroupMapper.selectOne(queryWrapper);
            int groupId = userGroup.getId();
            UserWithGroup userWithGroup = new UserWithGroup();
            userWithGroup.setGroupId(groupId);
            userWithGroup.setUserId(driverCode);
            userWithGroupMapper.insert(userWithGroup);
            driverInfoMapper.insert(driverInfo);
            return true;
        } catch (Exception e) {
            System.err.println("司机信息表 | 用户信息表 | 用户与组表 插入失败");
            return false;
        }
    }

    @Override
    public boolean deleteById(String id) {
        DriverInfo driverInfo = driverInfoMapper.selectById(id);
        User user = userMapper.selectById(id);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id",id);
        UserWithGroup userWithGroup = userWithGroupMapper.selectOne(queryWrapper);
        try {
            driverInfoMapper.deleteById(id);
            // driverInfoDao.delete(driverInfo);
            // userDao.delete(user);
            userMapper.deleteById(id);
            // userWithGroupDao.delete(userWithGroup);
            userWithGroupMapper.delete(queryWrapper);
            return true;
        } catch (Exception e) {
            System.err.println("司机信息 | 用户表 | 用户与组表 删除失败");
            return false;
        }
    }

    @Override
    public boolean updateDriverById(String id, DriverInfo driverInfo) {
        DriverInfo oldDriverInfo = driverInfoMapper.selectById(id);
        driverInfo.setState(oldDriverInfo.getState());
        try {
            driverInfoMapper.updateById(driverInfo);
            return true;
        } catch (Exception e) {
            System.err.println("司机信息更新失败");
            return false;
        }
    }

    @Override
    public Page<DriverInfo> selectAllEmpByPage(int pageNo, int pageSize) {
        Page<DriverInfo> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        Page<DriverInfo> pages = driverInfoMapper.selectPage(page,queryWrapper);
        return pages;
    }

    @Override
    public DriverInfo findDriverById(String id) {
        return driverInfoMapper.selectById(id);
    }

    @Override
    public List<String> findDriverAllId() {
        return driverInfoMapper.selectAllId();
    }


    private String randomCode() {
        StringBuilder stringBuilder = new StringBuilder();
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < 6; i++) {
            stringBuilder.append(random.nextInt(10));
        }
        return stringBuilder.toString();
    }
}
