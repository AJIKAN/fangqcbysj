package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.BillInfo;
import com.gxust.fangqcbysj.mapper.BillInfoMapper;
import com.gxust.fangqcbysj.service.BillInfoService;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class BillInfoServiceImpl extends ServiceImpl<BillInfoMapper, BillInfo> implements BillInfoService {
}
