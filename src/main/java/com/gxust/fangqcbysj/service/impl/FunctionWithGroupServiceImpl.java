package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.FunctionWithGroup;
import com.gxust.fangqcbysj.entity.UserWithGroup;
import com.gxust.fangqcbysj.mapper.FunctionWithGroupMapper;
import com.gxust.fangqcbysj.mapper.UserWithGroupMapper;
import com.gxust.fangqcbysj.service.FunctionWithGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class FunctionWithGroupServiceImpl extends ServiceImpl<FunctionWithGroupMapper,FunctionWithGroup> implements FunctionWithGroupService {

    @Autowired
    private FunctionWithGroupMapper functionWithGroupMapper;

    @Autowired
    private UserWithGroupMapper userWithGroupMapper;

    @Override
    public List<FunctionWithGroup> findAllFunctionWithGroups(int groupId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("group_id",groupId);
        return functionWithGroupMapper.selectList(queryWrapper);
    }

    @Override
    public List<FunctionWithGroup> findByLoginId(String loginId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id",loginId);
        UserWithGroup userWithGroup = userWithGroupMapper.selectOne(queryWrapper);
        QueryWrapper queryWrapper1 = new QueryWrapper();
        queryWrapper1.eq("group_id",userWithGroup.getGroupId());
        return functionWithGroupMapper.selectList(queryWrapper1);
    }
}
