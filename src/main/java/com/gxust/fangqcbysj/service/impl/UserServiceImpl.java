package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.User;
import com.gxust.fangqcbysj.mapper.UserMapper;
import com.gxust.fangqcbysj.service.UserService;
import com.gxust.fangqcbysj.utils.Md5Utils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 * @Author 方全朝
 * @Description
 * @Date 2021/3/27
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User login(String loginId, String password) {
        User user = new User();
        if (loginId != null&& password !=null){
            QueryWrapper<User> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("login_id",loginId);
            queryWrapper.eq("password", Md5Utils.setMD5(password));
            user  = userMapper.selectOne(queryWrapper);
            if (user != null){
                user.setIfOnline(1);
                userMapper.updateById(user);
            }
            return user;
        }
        return user;
    }

    @Override
    public Boolean changePassword(String loginId, String oldPassword, String newPassword) {
        User user = userMapper.selectById(loginId);
        String oldPasswordMD5 = Md5Utils.getMD5(oldPassword);
        if (oldPasswordMD5.equalsIgnoreCase(user.getPassword())) {
            user.setPassword(Md5Utils.setMD5(newPassword));
            userMapper.updateById(user);
            return true;
        }
        return false;
    }

    @Override
    public boolean ifExist(String loginId) {
        boolean flag = false;
        flag = userMapper.selectById(loginId) == null ? false : true;
        return flag;
    }
}
