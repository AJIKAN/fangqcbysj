package com.gxust.fangqcbysj.service.impl;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.gxust.fangqcbysj.service.SendSms;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Author: 方全朝
 * @Date: 2021/5/7
 */
@Service
public class SendSmsImpl implements SendSms {
    @Override
    public boolean send(String phoneNumber, String templateCode) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4GEqegHD8mSbz5uWXQbn", "vZFcQLmlOSa3aiEpfRLjQllR6Z36Jm");
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        // 不要动
        request.setMethod(MethodType.POST);
        // 不要动
        request.setDomain("dysmsapi.aliyuncs.com");
        // 不要动
        request.setVersion("2017-05-25");
        // 不要动
        request.setAction("SendSms");
        // 不要动
        request.putQueryParameter("RegionId","cn-hangzhou");
        request.putQueryParameter("PhoneNumbers",phoneNumber);
        request.putQueryParameter("SignName","紫荆牙科");
        request.putQueryParameter("TemplateCode",templateCode);
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
            return response.getHttpResponse().isSuccess();
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return false;
    }
}
