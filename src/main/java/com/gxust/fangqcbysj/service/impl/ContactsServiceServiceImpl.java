package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.ContactsService;
import com.gxust.fangqcbysj.entity.CustomerBillClear;
import com.gxust.fangqcbysj.entity.GoodsBill;
import com.gxust.fangqcbysj.mapper.ContactsServiceMapper;
import com.gxust.fangqcbysj.mapper.CustomerBillClearMapper;
import com.gxust.fangqcbysj.mapper.GoodsBillMapper;
import com.gxust.fangqcbysj.service.ContactsServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class ContactsServiceServiceImpl extends ServiceImpl<ContactsServiceMapper, ContactsService> implements ContactsServiceService {

    @Autowired
    private ContactsServiceMapper contactsServiceMapper;

    @Autowired
    private GoodsBillMapper goodsBillMapper;

    @Autowired
    private CustomerBillClearMapper customerBillClearMapper;

    @Override
    public List<ContactsService> printAllContactsService() {
        List<GoodsBill> goodsBills = goodsBillMapper.selectList(null);
        List<ContactsService> contactsServices = new ArrayList<>();
        for(GoodsBill goodsBill:goodsBills) {
            String goodsBillCode = goodsBill.getGoodsBillCode();
            ContactsService contactsService = new ContactsService();
            contactsService.setSendGoodsCustomer(goodsBill.getSendGoodsCustomer());
            contactsService.setGoodsBillCode(goodsBillCode);
            contactsService.setSendGoodsAddr(goodsBill.getSendGoodsAddr());
            contactsService.setReceiveGoodsAddr(goodsBill.getReceiveGoodsAddr());
            contactsService.setCarriage(goodsBill.getCarriage());
            contactsService.setInsurance(goodsBill.getInsurance());
            contactsService.setSendGoodsDate(goodsBill.getSendGoodsDate());
            // QueryWrapper<CustomerBillClear> queryWrapper = new QueryWrapper<>();
            CustomerBillClear customerBillClear = customerBillClearMapper.selectById(goodsBillCode);
            if(customerBillClear != null) {
                contactsService.setBillMoney(customerBillClear.getBillMoney());
                contactsService.setMoneyReceivable(customerBillClear.getMoneyReceivable());
                contactsService.setReceivedMoney(customerBillClear.getReceivedMoney());
                contactsServiceMapper.insert(contactsService);
                contactsServices.add(contactsService);
            }

        }
        return contactsServices;
    }

    @Override
    public ContactsService selectByGoodsBillCode(String goodsBillCode) {
        return contactsServiceMapper.selectById(goodsBillCode);
    }
}
