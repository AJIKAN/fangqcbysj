package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.TransferComInfo;
import com.gxust.fangqcbysj.mapper.TransferComInfoMapper;
import com.gxust.fangqcbysj.service.TransferComInfoService;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Service
public class TransferComInfoServiceImpl extends ServiceImpl<TransferComInfoMapper, TransferComInfo> implements TransferComInfoService{
}
