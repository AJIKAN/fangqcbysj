package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.mapper.*;
import com.gxust.fangqcbysj.service.GoodsBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.*;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Service
public class GoodsBillServiceImpl extends ServiceImpl<GoodsBillMapper, GoodsBill> implements GoodsBillService {

    @Autowired
    private GoodsBillMapper goodsBillMapper;

    @Autowired
    private TransferInfoMapper transferInfoMapper;

    @Autowired
    private CargoReceiptDetailMapper cargoReceiptDetailMapper;

    @Autowired
    private CargoReceiptMapper cargoReceiptMapper;

    @Autowired
    private BillInfoMapper billInfoMapper;

    @Autowired
    private GoodsBillEventMapper goodsBillEventMapper;

    @Autowired
    CallbackInfoMapper callbackInfoMapper;



    @Override
    public List<GoodsBill> transferGoods(String type, String driverId) {
        List<GoodsBill> goodsBills = goodsBillMapper.transferState(type, driverId);
        List<GoodsBill> result = new LinkedList<>();
        for (GoodsBill goodsBill : goodsBills) {
            String[] citys = goodsBill.getTransferStation().split(",");
            QueryWrapper<TransferInfo> transferInfoQueryWrapper = new QueryWrapper<>();
            transferInfoQueryWrapper.eq("goods_bill_code", goodsBill.getGoodsBillCode());
            transferInfoQueryWrapper.eq("transfer_station", citys[citys.length - 1]);
            TransferInfo transferInfo = transferInfoMapper.selectOne(transferInfoQueryWrapper);
            if (transferInfo == null) {
                result.add(goodsBill);
            }
        }
        return result;
    }

    @Override
    public List<GoodsBill> arriveGoods(String type, String driverId) {
        List<GoodsBill> goodsBills = goodsBillMapper.transferState(type, driverId);
        List<GoodsBill> result = new LinkedList<>();
        for (GoodsBill goodsBill : goodsBills) {
            String[] citys = goodsBill.getTransferStation().split(",");
            QueryWrapper<TransferInfo> transferInfoQueryWrapper = new QueryWrapper<>();
            transferInfoQueryWrapper.eq("goods_bill_code", goodsBill.getGoodsBillCode());
            transferInfoQueryWrapper.eq("transfer_station", citys[citys.length - 1]);
            if (transferInfoMapper.selectList(transferInfoQueryWrapper) != null) {
                result.add(goodsBill);
            }
        }
        return result;
    }

    @Override
    public List<GoodsBill> selectAllUnArrive() {
        List<GoodsBill> goodsBills = goodsBillMapper.selectList(null);
        List<GoodsBill> goodsBillsUnArrive = new ArrayList<>();
        for (GoodsBill goodsBill : goodsBills) {
            //4.5
            QueryWrapper<CargoReceiptDetail> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("goods_bill_detail_id", goodsBill.getGoodsBillCode());
            CargoReceiptDetail cargoReceiptDetail = cargoReceiptDetailMapper.selectOne(queryWrapper);
            //4.6
            QueryWrapper<CargoReceipt> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("goods_revert_bill_code", cargoReceiptDetail.getGoodsRevertBillId());
            CargoReceipt cargoReceipt = cargoReceiptMapper.selectOne(queryWrapper1);
            if (cargoReceipt == null) {
                continue;
            }
            //到货时间
            Date arriveTime = cargoReceipt.getArriveTime();
            Date startCarryTime = cargoReceipt.getStartCarryTime();
            if (arriveTime != null && startCarryTime != null) {
                long difference = arriveTime.getTime() - startCarryTime.getTime();
                //实际中转天数
                int day = (int) difference / (3600 * 24 * 1000);
                //中转次数
                int cishu = (int) (goodsBill.getTransferFee() / 1.3);
                if (day > (cishu + 1)) {
                    goodsBillsUnArrive.add(goodsBill);
                }
            }
        }

        return goodsBillsUnArrive;
    }

    @Override
    public List<GoodsBill> selectAllUnTake() {
        List<GoodsBill> goodsBills = goodsBillMapper.selectList(null);
        List<GoodsBill> goodsBillsUnTake = new ArrayList<>();
        for (GoodsBill goodsBill : goodsBills) {
            QueryWrapper<CargoReceiptDetail> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("goods_bill_detail_id", goodsBill.getGoodsBillCode());
            //4.5
            CargoReceiptDetail cargoReceiptDetail = cargoReceiptDetailMapper.selectOne(queryWrapper);
            QueryWrapper<CargoReceipt> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("goods_revert_bill_code", cargoReceiptDetail.getGoodsRevertBillId());
            //4.6
            CargoReceipt cargoReceipt = cargoReceiptMapper.selectOne(queryWrapper1);
            if (cargoReceipt == null) {
                continue;
            }
            //到货时间
            Date arriveTime = cargoReceipt.getArriveTime();
            //预计交货时间
            Date factDealDate = cargoReceipt.getStartCarryTime();
            if (arriveTime != null && factDealDate != null) {
                long difference = arriveTime.getTime() - factDealDate.getTime();
                int day = (int) difference / (3600 * 24 * 1000);
                if (day >= 3) {
                    goodsBillsUnTake.add(goodsBill);
                }
            }
        }

        return goodsBillsUnTake;
    }

    @Override
    public Map<?, ?> addGoodsBill(GoodsBill goodsBill) {
        Map<String, String> map = new HashMap<>();
        try {
            String goodsBillCode = "HY";
            while (true) {
                goodsBillCode += randomCode();
                QueryWrapper queryWrapper = new QueryWrapper();
                queryWrapper.eq("goods_bill_code", goodsBillCode);
                GoodsBill goodsBill1 = goodsBillMapper.selectOne(queryWrapper);
                if (goodsBill1 == null) {
                    break;
                }
            }
            goodsBill.setGoodsBillCode(goodsBillCode);
            goodsBill.setValidity("0");
            goodsBill.setIfAudit("0");
            goodsBillMapper.insert(goodsBill);

            BillInfo billInfo = new BillInfo();
            billInfo.setBillType("货运单");
            billInfo.setBillCode(goodsBillCode);
            billInfo.setBillState("已填");
            billInfo.setWriteDate(new Date());
            billInfoMapper.insert(billInfo);

            GoodsBillEvent goodsBillEvent = new GoodsBillEvent();
            goodsBillEvent.setGoodsBillId(goodsBillCode);
            goodsBillEvent.setEventName("待发");
            goodsBillEvent.setRemark("单据已填");
            goodsBillEvent.setOccurTime(new Date());
            goodsBillEventMapper.insert(goodsBillEvent);
            map.put("goods_bill_code", goodsBillCode);
            map.put("status", "SUCCESS");
            return map;
        } catch (Exception e) {
            System.err.println("货运单 | 单据 | 货运单事件表 插入失败！");
            map.put("status", "ERROR");
            return map;
        }
    }

    @Override
    public boolean addGoods(String goodsBillDetailId, CargoReceiptDetail cargoReceiptDetail) {
        try {
            String goodsRevertBillId = "HZ";
            while (true) {
                goodsRevertBillId += randomCode();
                QueryWrapper queryWrapper = new QueryWrapper();
                queryWrapper.eq("goods_revert_bill_id", goodsRevertBillId);
                CargoReceiptDetail cargoReceiptDetail1 = cargoReceiptDetailMapper.selectOne(queryWrapper);
                if (cargoReceiptDetail1 == null) {
                    break;
                }
            }
            cargoReceiptDetail.setGoodsRevertBillId(goodsRevertBillId);
            cargoReceiptDetailMapper.insert(cargoReceiptDetail);
            QueryWrapper queryWrapper1 = new QueryWrapper();
            queryWrapper1.eq("goods_bill_code", goodsBillDetailId);
            GoodsBill goodsBill = goodsBillMapper.selectOne(queryWrapper1);
            // GoodsBill goodsBill = goodsBillMapper.findByGoodsBillCode(goodsBillDetailId);
            goodsBill.setValidity("1");
            goodsBill.setIfAudit("1");
            goodsBillMapper.updateById(goodsBill);
            return true;
        } catch (Exception e) {
            System.err.println("货物插入失败！");
            return false;
        }
    }

    @Override
    public Page<GoodsBillEvent> selectAllGoogsBillByPage(int pageNo, int pageSize) {
        Page<GoodsBillEvent> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        Page<GoodsBillEvent> pages = goodsBillEventMapper.selectPage(page, queryWrapper);
        return pages;
    }

    @Override
    public Page<GoodsBillEvent> selectGoodsBillByEvent(String eventName, int pageNo, int pageSize) {
        Page<GoodsBillEvent> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("event_name", eventName);
        Page<GoodsBillEvent> pages = goodsBillEventMapper.selectPage(page, queryWrapper);
        return pages;
    }

    @Override
    public GoodsBill selectByGoodsBillCode(String goodsBillCode) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("goods_bill_code", goodsBillCode);
        return goodsBillMapper.selectOne(queryWrapper);
    }

    @Override
    public boolean updateGoodsBill(GoodsBill goodsBill) {
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("goods_bill_code", goodsBill.getGoodsBillCode());
        int i = goodsBillMapper.update(goodsBill, updateWrapper);
        if (i > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteGoodsBill(String goodsBillCode) {
        GoodsBillEvent goodsBillEvent = new GoodsBillEvent();
        goodsBillEvent.setGoodsBillId(goodsBillCode);
        goodsBillEvent.setEventName("删除货运单");
        goodsBillEvent.setRemark("删除货运单");
        goodsBillEvent.setOccurTime(new Date());
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("bill_code", goodsBillCode);
        BillInfo billInfo = billInfoMapper.selectOne(queryWrapper);
        billInfo.setBillState("作废");
        billInfo.setWriteDate(new Date());
        QueryWrapper queryWrapper1 = new QueryWrapper();
        queryWrapper1.eq("goods_bill_detail_id", goodsBillCode);
        CargoReceiptDetail cargoReceiptDetail = cargoReceiptDetailMapper.selectOne(queryWrapper1);
        try {
            int i = goodsBillEventMapper.updateById(goodsBillEvent);
            int j = billInfoMapper.updateById(billInfo);
            int k = cargoReceiptDetailMapper.deleteById(cargoReceiptDetail.getGoodsRevertBillId());
            int sum = i+j+k;
            if (sum >= 3) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("货运单删除失败");
            return false;
        }
    }

    @Override
    public List<GoodsBill> findWaitReceive(String customerCode) {
        return goodsBillMapper.selectGoodsBillList(customerCode);
    }

    @Override
    public List<GoodsBill> findInformGet(String billType, int pageNo, int pageSize) {
        Page<GoodsBill> page = new Page<>(pageNo, pageSize);
        System.out.println("billType = " + billType);
        return goodsBillMapper.findGoodsBillByBillType(billType, page);
    }

    @Override
    public List<GoodsBill> findOldInform(String type, int pageNo, int pageSize) {
        Page<GoodsBill> page = new Page<>(pageNo, pageSize);
        return goodsBillMapper.findGoodsBillByType(type, page);
    }

    @Override
    public List<GoodsBill> findAllGot(int pageNo, int pageSize) {
        Page<GoodsBill> page = new Page<>(pageNo, pageSize);
        return goodsBillMapper.findGoodsByAllGot(page);
    }


    private String randomCode() {
        StringBuilder stringBuilder = new StringBuilder();
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < 6; i++) {
            stringBuilder.append(random.nextInt(10));
        }
        return stringBuilder.toString();
    }
}
