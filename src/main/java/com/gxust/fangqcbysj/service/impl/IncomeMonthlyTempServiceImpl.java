package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.IncomeMonthlyTemp;
import com.gxust.fangqcbysj.mapper.IncomeMonthlyTempMapper;
import com.gxust.fangqcbysj.service.IncomeMonthlyTempService;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Service
public class IncomeMonthlyTempServiceImpl extends ServiceImpl<IncomeMonthlyTempMapper, IncomeMonthlyTemp> implements IncomeMonthlyTempService {

}
