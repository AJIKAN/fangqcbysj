package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.dto.FunctionWithGroupDto;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.mapper.*;
import com.gxust.fangqcbysj.service.UserGroupService;
import com.gxust.fangqcbysj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/4/24
 */
@Service
public class UserGroupServiceImpl extends ServiceImpl<UserGroupMapper, UserGroup> implements UserGroupService {

    @Autowired
    private UserGroupMapper userGroupMapper;

    @Autowired
    private FunctionWithGroupMapper functionWithGroupMapper;

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private FunctionMapper functionMapper;

    @Override
    public boolean addGroup(UserGroup userGroup) {
        int insert = userGroupMapper.insert(userGroup);
        if (insert > 0) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean addFuncGro(FunctionWithGroupDto functionWithGroupDto) {
        List<Integer> list = new ArrayList<>();
        for (int i : functionWithGroupDto.getArray()) {
            list.add(i);
        }
        QueryWrapper<FunctionWithGroup> functionWrapper = new QueryWrapper<>();
        functionWrapper.eq("group_id",functionWithGroupDto.getGroupId());
        functionWithGroupMapper.delete(functionWrapper);
        for (Integer integer : list) {
            FunctionWithGroup functionWithGroup = new FunctionWithGroup();
            functionWithGroup.setFunctionId(integer);
            functionWithGroup.setGroupId(functionWithGroupDto.getGroupId());
            functionWithGroupMapper.insert(functionWithGroup);
        }
        return true;
    }


    @Override
    public boolean delFuncGro(int id) {
        try {
            UserGroup userGroup = userGroupMapper.selectById(id);
            userGroupMapper.deleteById(id);
            UpdateWrapper updateWrapper = new UpdateWrapper();
            updateWrapper.eq("department", userGroup.getGroupName());
            Employee employee = new Employee();
            employee.setDepartment("临时组");
            employeeMapper.update(employee, updateWrapper);
            return true;
        } catch (Exception e) {
            System.err.println("用户组表删除 | 职工部门更新 失败！");
            return false;
        }
    }

    @Override
    public Page<UserGroup> findAllByPage(int pageNo, int pageSize) {
        Page<UserGroup> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        Page<UserGroup> pages = userGroupMapper.selectPage(page, queryWrapper);
        return pages;
    }

    @Override
    public UserGroup findOne(int id) {
        return userGroupMapper.selectById(id);
    }

    @Override
    public boolean updateUserGroup(int id, String description) {
        UserGroup userGroup = userGroupMapper.selectById(id);
        userGroup.setDescription(description);
        try {
            userGroupMapper.updateById(userGroup);
            return true;
        } catch (Exception e) {
            System.err.println("用户组描述更新失败！");
            return false;
        }
    }

    @Override
    public List<UserGroup> findAll() {
        return userGroupMapper.selectList(null);
    }
}
