package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.CarCost;
import com.gxust.fangqcbysj.entity.DriverAmount;
import com.gxust.fangqcbysj.entity.DriverClear;
import com.gxust.fangqcbysj.entity.DriverInfo;
import com.gxust.fangqcbysj.entity.dto.DriverClearDto;
import com.gxust.fangqcbysj.mapper.CarCostMapper;
import com.gxust.fangqcbysj.mapper.DriverAmountMapper;
import com.gxust.fangqcbysj.mapper.DriverClearMapper;
import com.gxust.fangqcbysj.mapper.DriverInfoMapper;
import com.gxust.fangqcbysj.service.CarCostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class CarCostServiceImpl extends ServiceImpl<CarCostMapper, CarCost> implements CarCostService {

    @Autowired
    private CarCostMapper carCostMapper;

    @Autowired
    private DriverInfoMapper driverInfoMapper;

    @Autowired
    private DriverClearMapper driverClearMapper;

    @Autowired
    private DriverAmountMapper driverAmountMapper;

    @Override
    public List<CarCost> printAllCarCost() {
        List<CarCost> carCosts = new ArrayList<>();
        List<DriverAmount> driverAmounts =  selectAllDriAcount();
        for(DriverAmount driverAmount : driverAmounts) {
            String driverCode = driverAmount.getDriverCode();
            QueryWrapper<CarCost> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("driver_code",driverCode);
            if(carCostMapper.selectOne(queryWrapper) == null) {
                CarCost carCost = new CarCost();
                carCost.setDriverCode(driverCode);
                // QueryWrapper<DriverInfo> queryWrapper1 = new QueryWrapper<>();
                // queryWrapper1.eq("driver_code",driverCode);
                // DriverInfo driverInfo = driverInfoMapper.selectOne(queryWrapper1);
                DriverInfo driverInfo = driverInfoMapper.selectById(driverCode);
                if(driverInfo!=null) {
                    carCost.setCarNo(driverInfo.getCarNo());
                    carCost.setCarType(driverInfo.getCarType());
                    carCost.setAllowCarryWeight(driverInfo.getAllowCarryWeight());
                    carCost.setCarWidth(driverInfo.getCarWidth());
                    carCost.setGoodsHeight(driverInfo.getGoodsHeight());
                    carCost.setCarryFeeTotal(driverAmount.getCarryFeeTotal());
                    carCost.setAddCarriageTotal(driverAmount.getAddCarriageTotal());
                    carCostMapper.insert(carCost);
                    carCosts.add(carCost);
                }
            }
            else {
                carCosts.add(carCostMapper.selectOne(queryWrapper));
            }
        }
        return carCosts;
    }

    @Override
    public List<DriverAmount> selectAllDriAcount() {
        List<DriverClearDto> driverClear = driverClearMapper.find();
        List<DriverAmount> driverAmounts = new ArrayList<>();
        for(DriverClearDto driverClearDto : driverClear) {
            DriverAmount driverAmount = new DriverAmount();
            driverAmount.setDriverCode(driverClearDto.getDriverCode());
            driverAmount.setCarryFeeTotal(Double.parseDouble(driverClearDto.getCarryFeeTotal()));
            driverAmount.setAddCarriageTotal( Double.parseDouble(driverClearDto.getAddCarriageTotal()) );
            driverAmount.setTotal(Double.parseDouble(driverClearDto.getTotal()));
            driverAmountMapper.insert(driverAmount);
            driverAmounts.add(driverAmount);
        }
        return driverAmounts;
    }
}
