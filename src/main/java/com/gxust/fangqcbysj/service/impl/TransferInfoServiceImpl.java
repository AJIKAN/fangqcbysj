package com.gxust.fangqcbysj.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.entity.Vo.TransferInfoVo;
import com.gxust.fangqcbysj.mapper.*;
import com.gxust.fangqcbysj.service.TransferInfoService;
import com.gxust.fangqcbysj.utils.DateUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Service
public class TransferInfoServiceImpl extends ServiceImpl<TransferInfoMapper, TransferInfo> implements TransferInfoService {

    @Autowired
    private TransferInfoMapper transferInfoMapper;

    @Autowired
    private TransferComInfoMapper transferComInfoMapper;

    @Autowired
    private GoodsBillMapper goodsBillMapper;

    @Autowired
    private CallbackInfoMapper callbackInfoMapper;

    @Autowired
    private CustomerReceiptInfoMapper customerReceiptInfoMapper;

    @Override
    public boolean addCompany(TransferComInfo transferComInfo) {
        try {
            TransferComInfo transferComInfoEntity = new TransferComInfo();
            BeanUtils.copyProperties(transferComInfo, transferComInfoEntity);
            transferComInfoMapper.insert(transferComInfoEntity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("中转公司信息添加失败");
            return false;
        }
    }

    @Override
    public boolean addTransferInfo(TransferInfo transferInfo) {
        try {
            int insert = transferInfoMapper.insert(transferInfo);
            if (insert > 0){
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("中转信息添加失败");
            return false;
        }
    }

    @Override
    public Page<TransferComInfo> findAllByPage(int pageNo, int pageSize, TransferComInfo transferComInfo) {
        QueryWrapper<TransferComInfo> queryWrapper = new QueryWrapper<>();
        // queryWrapper.like("id",transferComInfo.getId() == null ? 0 : transferComInfo.getId());
        // queryWrapper.or();
        queryWrapper.like("city", transferComInfo.getCity() == null ? "" : transferComInfo.getCity());
        queryWrapper.or();
        queryWrapper.like("company_name", transferComInfo.getCompanyName() == null ? "" : transferComInfo.getCompanyName());
        queryWrapper.or();
        queryWrapper.like("detail_address", transferComInfo.getDetailAddress() == null ? "" : transferComInfo.getDetailAddress());
        Page<TransferComInfo> page = new Page<>(pageNo, pageSize);
        Page<TransferComInfo> pages = transferComInfoMapper.selectPage(page, queryWrapper);
        return pages;
    }


    @Override
    public TransferComInfo findByGoodsBillCode(String goodsBillCode) {
        GoodsBill goodsBill = goodsBillMapper.selectById(goodsBillCode);
        String[] citys = goodsBill.getTransferStation().split(",");
        TransferComInfo transferComInfo = new TransferComInfo();
        for (String string : citys) {
            QueryWrapper<TransferInfo> transferInfoQueryWrapper = new QueryWrapper<>();
            transferInfoQueryWrapper.eq("goods_bill_code", goodsBillCode);
            transferInfoQueryWrapper.eq("transfer_station", string);
            if (transferInfoMapper.selectList(transferInfoQueryWrapper) != null) {
                QueryWrapper<TransferComInfo> transferComInfoQueryWrapper = new QueryWrapper<>();
                transferComInfoQueryWrapper.eq("city", string);
                transferComInfo = transferComInfoMapper.selectOne(transferComInfoQueryWrapper);
                break;
            }
        }
        return transferComInfo;
    }

    @Override
    public List<GoodsBill> findOnWayBills() {
        List<GoodsBill> list = goodsBillMapper.findOnWayBills();
        List<GoodsBill> result = new ArrayList<>();
        for (GoodsBill goodsBill : list) {
            QueryWrapper<CallbackInfo> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("goods_bill_id", goodsBill.getGoodsBillCode());
            queryWrapper.eq("type", "中转回告");
            CallbackInfo callbackInfo = callbackInfoMapper.selectOne(queryWrapper);
            if (callbackInfo == null) {
                result.add(goodsBill);
            }
        }
        return result;
    }

    @Override
    public IPage<TransferInfo> findInfoByPage(int pageNo, int pageSize, TransferInfoVo transferInfoVo) {
        QueryWrapper<TransferInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("after_transfer_bill", transferInfoVo.getAfterTransferBill());
        queryWrapper.or();
        try {
            queryWrapper.like("check_time", transferInfoVo.getCheckTime() == null ? "" : DateUtil.parse(transferInfoVo.getCheckTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        queryWrapper.or();
        queryWrapper.like("description", transferInfoVo.getDescription() == null ? "" : transferInfoVo.getDescription());
        queryWrapper.or();
        queryWrapper.like("goods_bill_code", transferInfoVo.getGoodsBillCode() == null ? "" : transferInfoVo.getGoodsBillCode());
        queryWrapper.or();
        queryWrapper.like("goods_bill_code", transferInfoVo.getGoodsBillCode() == null ? "" : transferInfoVo.getGoodsBillCode());
        queryWrapper.or();
        queryWrapper.like("transfer_addr", transferInfoVo.getTransferAddr() == null ? "" : transferInfoVo.getTransferAddr());
        queryWrapper.or();
        queryWrapper.like("transfer_check", transferInfoVo.getTransferCheck() == null ? "" : transferInfoVo.getTransferCheck());
        queryWrapper.or();
        queryWrapper.like("transfer_company", transferInfoVo.getTransferCompany() == null ? "" : transferInfoVo.getTransferCompany());
        queryWrapper.or();
        queryWrapper.like("transfer_fee", transferInfoVo.getTransferFee() == null ? "" : transferInfoVo.getTransferFee());
        queryWrapper.or();
        queryWrapper.like("transfer_station", transferInfoVo.getTransferStationTel() == null ? "" : transferInfoVo.getTransferStationTel());
        queryWrapper.or();
        queryWrapper.like("transfer_station_tel", transferInfoVo.getTransferStationTel() == null ? "" : transferInfoVo.getTransferStationTel());
        Page<TransferInfo> page = new Page<>(pageNo, pageSize);
        Page<TransferInfo> pages = transferInfoMapper.selectPage(page, queryWrapper);
        pages.getRecords().stream().forEach(transferInfo -> transferInfo.setCheckTime(DateUtil.superParseDate(DateUtil.getDateFormat(transferInfo.getCheckTime()))));
        return pages;
    }

    @Override
    public IPage<CustomerReceiptInfo> findCusRecPage(int pageNo,int pageSize,String receiveGoodsPerson) {
        QueryWrapper<CustomerReceiptInfo> queryWrapper = new QueryWrapper();
        queryWrapper.like("receive_goods_person",receiveGoodsPerson);
        Page<CustomerReceiptInfo> page = new Page<>(pageNo, pageSize);
        Page<CustomerReceiptInfo> customerReceiptInfoPage = customerReceiptInfoMapper.selectPage(page, queryWrapper);
        return customerReceiptInfoPage;
    }
}
