package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.mapper.CustomerInfoMapper;
import com.gxust.fangqcbysj.mapper.UserGroupMapper;
import com.gxust.fangqcbysj.mapper.UserMapper;
import com.gxust.fangqcbysj.mapper.UserWithGroupMapper;
import com.gxust.fangqcbysj.service.CustomerInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class CustomerInfoServiceImpl extends ServiceImpl<CustomerInfoMapper, CustomerInfo> implements CustomerInfoService {

    @Autowired
    private CustomerInfoMapper customerInfoMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserWithGroupMapper userWithGroupMapper;

    @Autowired
    private UserGroupMapper userGroupMapper;

    @Override
    public boolean addCustomer(CustomerInfo customer) {
        try {
            String customerCode = "KH";
            while (true) {
                customerCode += randomCode();
                QueryWrapper queryWrapper = new QueryWrapper();
                queryWrapper.eq("customer_code",customerCode);
                CustomerInfo customerInfo = customerInfoMapper.selectOne(queryWrapper);
                if (customerInfo == null) {
                    break;
                }
            }
            // 添加用户信息表
            User user = new User(customerCode, "E10ADC3949BA59ABBE56E057F20F883E", 0);
            userMapper.insert(user);
            // 添加用户与组情况
            String department="客户组";
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("group_name",department);
            UserGroup userGroup = userGroupMapper.selectOne(queryWrapper);
            int groupId = userGroup.getId();
            UserWithGroup userWithGroup = new UserWithGroup();
            userWithGroup.setGroupId(groupId);
            userWithGroup.setUserId(customerCode);
            userWithGroupMapper.insert(userWithGroup);
            // 添加客户表
            customer.setCustomerCode(customerCode);
            customerInfoMapper.insert(customer);
            return true;
        } catch (Exception e) {
            System.err.println("客户 | 用户组关系表 | 用户 信息插入失败！");
            return false;
        }
    }

    @Override
    public boolean deleteCustomer(String customerCode) {
        // 共删除3张表
        try {
            // 用户表
            User user = userMapper.selectById(customerCode);
            userMapper.deleteById(customerCode);

            // 用户与组表
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("user_id",customerCode);
            UserWithGroup userWithGroup = userWithGroupMapper.selectOne(queryWrapper);
            userWithGroupMapper.deleteById(userWithGroup.getId());

            // 客户表
            CustomerInfo customer = new CustomerInfo();
            customer.setCustomerCode(customerCode);
            customerInfoMapper.deleteById(customerCode);
            return true;
        } catch (Exception e) {
            System.err.println("客户 | 用户组关系表 | 用户 信息删除失败！");
            return false;
        }
    }

    @Override
    public Page<CustomerInfo> selectAllCusByPage(int pageNo, int pageSize) {
        Page<CustomerInfo> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        Page<CustomerInfo> pages = customerInfoMapper.selectPage(page,queryWrapper);
        return pages;
    }

    @Override
    public CustomerInfo selectByCustomerCode(String customerCode) {
        return customerInfoMapper.selectById(customerCode);
    }

    @Override
    public List<String> selectAllCusCode() {
        // List<String> all = new ArrayList<>();
        // List<CustomerInfo> customerInfoList = customerInfoMapper.selectList(null);
        // all.addAll(customerInfoList);
        return customerInfoMapper.findAll();
    }

    @Override
    public boolean updateCustomer(String customerCode, CustomerInfo customerInfo) {
        CustomerInfo temp = customerInfoMapper.selectById(customerCode);
        customerInfo.setCustomerCode(customerCode);
        customerInfo.setCustomer(temp.getCustomer());
        try {
            customerInfoMapper.updateById(customerInfo);
            return true;
        } catch (Exception e) {
            System.err.println("顾客信息更新失败");
            return false;
        }
    }


    private String randomCode() {
        StringBuilder stringBuilder = new StringBuilder();
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < 6; i++) {
            stringBuilder.append(random.nextInt(10));
        }
        return stringBuilder.toString();
    }
}
