package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.CustomerAmount;
import com.gxust.fangqcbysj.entity.dto.GoodsBillDto;
import com.gxust.fangqcbysj.mapper.CustomerAmountMapper;
import com.gxust.fangqcbysj.mapper.GoodsBillMapper;
import com.gxust.fangqcbysj.service.CustomerAmountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class CustomerAmountServiceImpl extends ServiceImpl<CustomerAmountMapper, CustomerAmount> implements CustomerAmountService {

    @Autowired
    private CustomerAmountMapper customerAmountMapper;

    @Autowired
    private GoodsBillMapper goodsBillMapper;

    @Override
    public List<CustomerAmount> selectAllCusAcount() {
        List<GoodsBillDto> strings = goodsBillMapper.find();
        List<CustomerAmount> customerAmounts = new ArrayList<>();
        for (GoodsBillDto goodsBillDto : strings) {
            CustomerAmount customerAmount = new CustomerAmount();
            customerAmount.setSendGoodsCustomer(goodsBillDto.getSendGoodsCustomer());
            customerAmount.setCarriageTotal(Double.parseDouble(goodsBillDto.getCarriageTotal()));
            customerAmount.setInsuranceTotal(Double.parseDouble(goodsBillDto.getInsuranceTotal()));
            customerAmount.setPieceAmountTotal(Integer.parseInt(goodsBillDto.getPieceAmountTotal()));
            customerAmountMapper.insert(customerAmount);
            customerAmounts.add(customerAmount);
        }
        // for(GoodsBillDto string : strings) {
        //     CustomerAmount customerAmount = new CustomerAmount();
        //     customerAmount.setSendGoodsCustomer( (String)string[0] );
        //     customerAmount.setCarriageTotal( (double)string[1] );
        //     customerAmount.setInsuranceTotal( (double)string[2] );
        //     customerAmount.setPieceAmountTotal( Integer.parseInt(String.valueOf(string[3])));
        //     customerAmountMapper.insert(customerAmount);
        //     customerAmounts.add(customerAmount);
        // }
        return customerAmounts;
    }
}
