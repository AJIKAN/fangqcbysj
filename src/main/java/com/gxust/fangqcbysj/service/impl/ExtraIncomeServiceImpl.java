package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.mapper.*;
import com.gxust.fangqcbysj.service.ExtraIncomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class ExtraIncomeServiceImpl extends ServiceImpl<ExtraIncomeMapper, ExtraIncome> implements ExtraIncomeService {

    @Autowired
    private ExtraIncomeMapper extraIncomeMapper;

    @Autowired
    private FinanceFeeMapper financeFeeMapper;

    @Autowired
    private ManageFeeMapper manageFeeMapper;

    @Autowired
    private EmployeeWageMapper employeeWageMapper;

    @Autowired
    private CargoReceiptMapper cargoReceiptMapper;

    @Autowired
    private ExtraClearMapper extraClearMapper;

    @Autowired
    private IncomeMonthlyTempMapper incomeMonthlyTempMapper;

    @Override
    public boolean addExtraIncome(ExtraIncome extraIncome) {
        try {
            extraIncomeMapper.insert(extraIncome);
            return true;
        } catch (Exception e) {
            System.err.println("营业外收入添加失败");
            return false;
        }
    }

    @Override
    public Page<ExtraIncome> selectAllExtra(int pageNo, int pageSize) {
        Page<ExtraIncome> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        Page<ExtraIncome> pages = extraIncomeMapper.selectPage(page,queryWrapper);
        return pages;
    }

    @Override
    public boolean addFinanceFee(FinanceFee financeFee) {
        try {
            int insert = financeFeeMapper.insert(financeFee);
            if (insert > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            System.err.println("财务费用添加失败");
            return false;
        }
    }

    @Override
    public Page<FinanceFee> selectAllFinance(int pageNo, int pageSize) {
        Page<FinanceFee> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        Page<FinanceFee> pages = financeFeeMapper.selectPage(page,queryWrapper);
        return pages;
    }

    @Override
    public boolean addManageFee(ManageFee manageFee) {
        try {
            manageFeeMapper.insert(manageFee);
            return true;
        } catch (Exception e) {
            System.err.println("管理费用添加失败");
            return false;
        }
    }

    @Override
    public Page<ManageFee> selectAllManage(int pageNo, int pageSize) {
        Page<ManageFee> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        Page<ManageFee> pages = manageFeeMapper.selectPage(page,queryWrapper);
        return pages;
    }

    @Override
    public ManageFee findManageFee(int id) {
        return manageFeeMapper.selectById(id);
    }

    @Override
    public boolean addWage(EmployeeWage wage) {
        try {
            employeeWageMapper.insert(wage);
            return true;
        } catch (Exception e) {
            System.err.println("员工工资添加失败");
            return false;
        }
    }

    @Override
    public Page<EmployeeWage> selectAllWage(int pageNo, int pageSize) {
        Page<EmployeeWage> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        Page<EmployeeWage> pages = employeeWageMapper.selectPage(page,queryWrapper);
        return pages;
    }

    @Override
    public EmployeeWage selectByEmployeeCode(String employeeCode) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("employee_code",employeeCode);
        return employeeWageMapper.selectOne(queryWrapper);
    }

    @Override
    public IncomeMonthlyTemp selectIncomeMonthly() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+08:00"));
        String cMonth = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        String cYear = String.valueOf(calendar.get(Calendar.YEAR));
        if (cMonth.length() == 1) {
            cMonth = "0" + cMonth;
        }
        String cTime = cYear + "-" + cMonth;
        String bTime = cTime + "-01";
        String eTime = cTime + "-31";
        double carriageMoney = 0;
        double insuranceMoney = 0;
        double unbizIncome = 0;
        double fFee = 0;
        double officeFee = 0;
        double houseRent = 0;
        double waterElecFee = 0;
        double phoneFee = 0;
        double other = 0;
        double wage = 0;
        double balanceMoney = 0;
        double carCarriage = 0;
        double payOut = 0;
        double income = 0;
        // 运费、保险费（营业收入）
        List<CargoReceipt> cargoReceipts = selectBySignTime(bTime, eTime);
        System.out.println(1);
        for (int i = 0; i < cargoReceipts.size(); i++) {
            carriageMoney += cargoReceipts.get(i).getAllCarriage();
            insuranceMoney += cargoReceipts.get(i).getInsurance();
        }
        // 非营业收入
        List<ExtraIncome> extraIncomes = selectByIncomeMonth(cTime);
        System.out.println(1);
        for (int i = 0; i < extraIncomes.size(); i++) {
            unbizIncome += extraIncomes.get(i).getMoney();
        }
        // 收入
        income = carriageMoney + insuranceMoney + unbizIncome;
        // 财务费用
        List<FinanceFee> financeFees = selectByFPayoutMonth(cTime);
        System.out.println(1);
        for (int i = 0; i < financeFees.size(); i++) {
            fFee += financeFees.get(i).getFee();
        }
        // 管理费用
        List<ManageFee> manageFees = selectByMPayoutMonth(cTime);
        System.out.println(1);
        for (int i = 0; i < manageFees.size(); i++) {
            officeFee += manageFees.get(i).getOfficeFee();
            houseRent += manageFees.get(i).getHouseRent();
            waterElecFee += manageFees.get(i).getWaterElecFee();
            phoneFee += manageFees.get(i).getPhoneFee();
            other += manageFees.get(i).getOtherPayout();
        }
        // 工资
        List<EmployeeWage> employeeWages = selectByDate(bTime, eTime);
        System.out.println(1);
        for (int i = 0; i < employeeWages.size(); i++) {
            wage += (employeeWages.get(i).getBasicWage() + employeeWages.get(i).getAllowance()
                    + employeeWages.get(i).getStationWage());
        }
        // 经营费用（搬运费、车运费）
        List<ExtraClear> extraClears = selectByBalanceDate(bTime, eTime);
        System.out.println(1);
        for (int i = 0; i < extraClears.size(); i++) {
            balanceMoney += extraClears.get(i).getBalanceMoney();
        }
        carCarriage = 500;
        // 支出
        payOut = carCarriage + balanceMoney + wage + officeFee + houseRent + waterElecFee + phoneFee + officeFee + fFee;
        try {
            IncomeMonthlyTemp iMonthlyTemp = selectByMonth(cTime);
            if (iMonthlyTemp == null) {
                iMonthlyTemp = new IncomeMonthlyTemp();
            }
            iMonthlyTemp.setMonth(cTime);
            iMonthlyTemp.setCarriageMoney(carriageMoney);
            iMonthlyTemp.setInsuranceMoney(insuranceMoney);
            iMonthlyTemp.setBizIncome(insuranceMoney + carriageMoney);
            iMonthlyTemp.setUnbizIncome(unbizIncome);
            iMonthlyTemp.setIncome(income);
            iMonthlyTemp.setFinanceFee(fFee);
            iMonthlyTemp.setOfficeFee(officeFee);
            iMonthlyTemp.setHouseRent(houseRent);
            iMonthlyTemp.setWaterElecFee(waterElecFee);
            iMonthlyTemp.setPhoneFee(phoneFee);
            iMonthlyTemp.setOther(other);
            iMonthlyTemp.setManageFee(officeFee + houseRent + waterElecFee + phoneFee + other);
            iMonthlyTemp.setWage(wage);
            iMonthlyTemp.setConveyWage(balanceMoney);
            iMonthlyTemp.setCarCarriage(carCarriage);
            iMonthlyTemp.setBizFee(carCarriage + balanceMoney);
            iMonthlyTemp.setPayout(payOut);
            iMonthlyTemp.setProfit(income - payOut);
            incomeMonthlyTempMapper.insert(iMonthlyTemp);
            return iMonthlyTemp;
        } catch (Exception e) {
            System.err.println("月报信息添加失败");
            return null;
        }
    }

    @Override
    public List<ExtraIncome> selectByIncomeMonth(String incomeMonth) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("income_month",incomeMonth);
        return extraIncomeMapper.selectList(queryWrapper);
    }

    @Override
    public List<FinanceFee> selectByFPayoutMonth(String payoutMonth) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("payout_month",payoutMonth);
        return financeFeeMapper.selectList(queryWrapper);
    }

    @Override
    public List<ManageFee> selectByMPayoutMonth(String payoutMonth) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("payout_month",payoutMonth);
        return manageFeeMapper.selectList(queryWrapper);
    }

    @Override
    public List<EmployeeWage> selectByDate(String beginTime, String endTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = null;
        java.util.Date date1 = null;
        try {
            date = df.parse(beginTime);
            date1 = df.parse(endTime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return employeeWageMapper.findByDate(date, date1);
    }

    @Override
    public List<ExtraClear> selectByBalanceDate(String beginTime, String endTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = null;
        java.util.Date date1 = null;
        try {
            date = df.parse(beginTime);
            date1 = df.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return extraClearMapper.findByBalanceDate(date, date1);
    }

    @Override
    public IncomeMonthlyTemp selectByMonth(String month) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("month",month);
        return incomeMonthlyTempMapper.selectOne(queryWrapper);
    }

    public List<CargoReceipt> selectBySignTime(String beginTime, String endTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = null;
        java.util.Date date1 = null;
        try {
            date = df.parse(beginTime);
            date1 = df.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cargoReceiptMapper.findBySignTime(date, date1);
    }
}
