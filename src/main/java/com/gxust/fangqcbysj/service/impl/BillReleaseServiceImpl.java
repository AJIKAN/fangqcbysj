package com.gxust.fangqcbysj.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.mapper.*;
import com.gxust.fangqcbysj.service.BillReleaseService;
import com.gxust.fangqcbysj.utils.HttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class BillReleaseServiceImpl extends ServiceImpl<BillReleaseMapper, BillRelease> implements BillReleaseService {

    @Autowired
    private BillReleaseMapper billReleaseMapper;

    @Autowired
    private GoodsBillEventMapper goodsBillEventMapper;

    @Autowired
    CargoReceiptDetailMapper cargoReceiptDetailMapper;

    @Autowired
    private CargoReceiptMapper cargoReceiptMapper;

    @Autowired
    private GoodsReceiptInfoMapper goodsReceiptInfoMapper;

    @Autowired
    private GoodsBillMapper goodsBillMapper;

    @Autowired
    private BillInfoMapper billInfoMapper;


    @Override
    public boolean addRelease(BillRelease billRelease) {
        billRelease.setBillType("货运单");
        String billCode = billRelease.getBillCode();
        try {
            billReleaseMapper.insert(billRelease);
            UpdateWrapper updateWrapper = new UpdateWrapper();
            updateWrapper.eq("goods_bill_id",billCode);
            updateWrapper.set("event_name","未到");
            updateWrapper.set("occur_time",new Date());
            GoodsBillEvent goodsBillEvent = new GoodsBillEvent();
            goodsBillEvent.setEventName("未到");
            goodsBillEvent.setOccurTime(new Date());
            goodsBillEventMapper.update(goodsBillEvent,updateWrapper);
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("goods_bill_detail_id",billCode);
            String goodsRevertBillId = cargoReceiptDetailMapper.selectOne(queryWrapper).getGoodsRevertBillId();
            UpdateWrapper updateWrapper1 = new UpdateWrapper();
            updateWrapper1.eq("goods_revert_bill_code",goodsRevertBillId);
            CargoReceipt cargoReceipt = new CargoReceipt();
            cargoReceipt.setStartCarryTime(billRelease.getReceiveBillTime()).setDriverId(billRelease.getReceiveBillPerson()).setBackBillState("未到车辆");
            cargoReceiptMapper.update(cargoReceipt,updateWrapper1);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("单据明细表插入失败 | 货运单 & 货运回执信息 更新失败");
            return false;
        }
    }

    @Override
    public boolean addGoodsReceipt(GoodsReceiptInfo goodsReceiptInfo) {
        String goodsRevertBillId = goodsReceiptInfo.getGoodsRevertCode();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("goods_revert_bill_id",goodsRevertBillId);
        CargoReceiptDetail cargoReceiptDetail = cargoReceiptDetailMapper.selectOne(queryWrapper);
        String billId = cargoReceiptDetail.getGoodsBillDetailId();
        try {
            goodsReceiptInfoMapper.insert(goodsReceiptInfo);
            UpdateWrapper updateWrapper = new UpdateWrapper();
            updateWrapper.eq("goods_bill_id",billId);
            GoodsBillEvent goodsBillEvent = new GoodsBillEvent();
            goodsBillEvent.setEventName("未结").setOccurTime(new Date());
            goodsBillEventMapper.update(goodsBillEvent,updateWrapper);
            UpdateWrapper updateWrapper1 = new UpdateWrapper();
            updateWrapper1.eq("goods_bill_code",billId);
            GoodsBill goodsBill = new GoodsBill();
            goodsBill.setFactDealDate(goodsReceiptInfo.getRceiveGoodsDate());
            goodsBillMapper.update(goodsBill,updateWrapper1);
            UpdateWrapper updateWrapper2 = new UpdateWrapper();
            updateWrapper.eq("goods_revert_bill_code",goodsRevertBillId);
            CargoReceipt cargoReceipt = new CargoReceipt();
            cargoReceipt.setArriveTime(goodsReceiptInfo.getRceiveGoodsDate()).setBackBillState("未结合同");
            cargoReceiptMapper.update(cargoReceipt,updateWrapper2);
            // 发送短信通知客户
            QueryWrapper queryWrapper1 = new QueryWrapper();
            queryWrapper1.eq("goods_bill_code",billId);
            GoodsBill goodsBill1 = goodsBillMapper.selectOne(queryWrapper1);
            String phone = goodsBill1.getReceiveGoodsCustomerTel();
            String content = "【物流管理系统】尊敬的客户，您单号为"+cargoReceiptDetail.getGoodsBillDetailId()+"的货物已送达，请您及时接收!";
            HttpRequest.sendSms(phone,content);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("货物回执信息添加失败");
            return false;
        }
    }

    @Override
    public Page<BillInfo> findAllByPage(int pageNo, int pageSize) {
        Page<BillInfo> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        Page<BillInfo> pages = billInfoMapper.selectPage(page,queryWrapper);
        return pages;
    }

    @Override
    public Page<BillInfo> findNotRelease(int pageNo, int pageSize) {
        Page<BillInfo> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("bill_state","已填");
        queryWrapper.eq("bill_type","货运单");
        QueryWrapper queryWrapper1 = new QueryWrapper();
        queryWrapper1.isNotNull("bill_code");
        List<BillRelease> billReleases = billReleaseMapper.selectList(queryWrapper1);
        List<String> collect = billReleases.stream().map(BillRelease::getBillCode).collect(Collectors.toList());
        if (CollectionUtil.isNotEmpty(collect)){
            queryWrapper.notIn("bill_code",collect);
        }
        Page<BillInfo> pages = billInfoMapper.selectPage(page,queryWrapper);
        if(pages!= null){
            return pages;
        }
        return null;
    }
}
