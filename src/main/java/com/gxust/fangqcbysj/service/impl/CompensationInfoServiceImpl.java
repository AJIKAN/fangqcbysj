package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.CompensationInfo;
import com.gxust.fangqcbysj.mapper.CompensationInfoMapper;
import com.gxust.fangqcbysj.service.CompensationInfoService;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class CompensationInfoServiceImpl extends ServiceImpl<CompensationInfoMapper, CompensationInfo> implements CompensationInfoService {
}
