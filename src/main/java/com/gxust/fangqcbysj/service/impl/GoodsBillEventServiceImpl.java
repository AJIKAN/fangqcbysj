package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.GoodsBillEvent;
import com.gxust.fangqcbysj.mapper.GoodsBillEventMapper;
import com.gxust.fangqcbysj.service.GoodsBillEventService;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class GoodsBillEventServiceImpl extends ServiceImpl<GoodsBillEventMapper, GoodsBillEvent> implements GoodsBillEventService {
}
