package com.gxust.fangqcbysj.service.impl;

import com.gxust.fangqcbysj.service.IMapApiService;
import com.gxust.fangqcbysj.utils.LocationUtils;
import org.springframework.stereotype.Service;

/**
 * @Author 方全朝
 * @Description
 * @Date 2021/3/14
 */
@Service
public class MapApiServiceImpl implements IMapApiService {

    /**
     * 根据两个地址返回两个地址的直线距离/公里（两个地点经纬度计算出来的直线距离）
     *
     * @param address
     * @param otherAddress
     * @return
     */
    @Override
    public Double getDistanceByTwoPlace(String address, String otherAddress) {
        // 判空，要求我们的地址参数不能为空
        if (address != null && !"".equals(address) && otherAddress != null && !"".equals(otherAddress)) {
            // 返回最终地址 / 公里
            return LocationUtils.getDistance(address, otherAddress) > 0 ? LocationUtils.getDistance(address, otherAddress) : 0.0;
        } else {
            return 0.0;
        }
    }
}
