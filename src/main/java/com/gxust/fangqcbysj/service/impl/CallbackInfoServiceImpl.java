package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.CallbackInfo;
import com.gxust.fangqcbysj.mapper.CallbackInfoMapper;
import com.gxust.fangqcbysj.service.CallbackInfoService;
import com.gxust.fangqcbysj.utils.HttpRequest;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Date;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class CallbackInfoServiceImpl extends ServiceImpl<CallbackInfoMapper, CallbackInfo> implements CallbackInfoService {

    @Autowired
    private CallbackInfoMapper callbackInfoMapper;

    @Override
    public boolean addInfo(CallbackInfo callbackInfo) {
        try {
            String billId = randomCode();
            callbackInfo.setBillId(billId);
            callbackInfo.setBillType("");
            callbackInfo.setFinallyDialTime(new Date());
            callbackInfo.setLocked("0");
            callbackInfo.setSuccess("1");
            int insert = callbackInfoMapper.insert(callbackInfo);
            if (insert > 0) {
                //String content = callbackInfo.getContent();
                StringBuilder content = new StringBuilder();
                if (callbackInfo.getType().equals("提货回告")){
                    content.append("【物流管理系统】尊敬的客户，您单号为"+callbackInfo.getGoodsBillId()+"的货物已送达，请您及时接收!");
                }
                if (callbackInfo.getType().equals("到货回告")){
                    content.append("【物流管理系统】尊敬的客户，您单号为"+callbackInfo.getGoodsBillId()+"的货物已送达，请您及时接收!");
                }
                if (callbackInfo.getType().equals("中转回告")){
                    content.append("【物流管理系统】尊敬的客户，您单号为"+callbackInfo.getGoodsBillId()+"的货物已中转!");
                }
                if (callbackInfo.getType().equals("已提回告")){
                    content.append("【物流管理系统】尊敬的客户，您单号为"+callbackInfo.getGoodsBillId()+"的货物已提货达，请您及时验收!");
                }
                String phone = callbackInfo.getDialNo();
                HttpRequest.sendSms(phone,content.toString());
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("插入回告失败");
            return false;
        }
    }

    @Override
    public CallbackInfo findDetail(String goodsBillId, String type) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("goods_bill_id",goodsBillId);
        queryWrapper.eq("type",type);
        return callbackInfoMapper.selectOne(queryWrapper);
    }

    private String randomCode() {
        StringBuilder stringBuilder = new StringBuilder();
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < 6; i++) {
            stringBuilder.append(random.nextInt(10));
        }
        return stringBuilder.toString();
    }
}
