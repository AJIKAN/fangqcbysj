package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.mapper.*;
import com.gxust.fangqcbysj.service.DriverClearService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/4/30
 */
@Service
@Slf4j
public class DriverClearServiceImpl extends ServiceImpl<DriverClearMapper, DriverClear> implements DriverClearService {

    @Autowired
    private DriverClearMapper driverClearMapper;

    @Autowired
    private CargoReceiptMapper cargoReceiptMapper;

    @Autowired
    private CustomerBillClearMapper customerBillClearMapper;

    @Autowired
    private GoodsBillEventMapper goodsBillEventMapper;

    @Autowired
    private GoodsBillMapper goodsBillMapper;

    @Autowired
    private ProxyFeeClearMapper proxyFeeClearMapper;

    @Autowired
    private ExtraClearMapper extraClearMapper;

    @Override
    public List<DriverClear> selectDrclear(String eventName) {
        List<DriverClear> driverCleareds = new ArrayList(); // 已结
        List<DriverClear> driverUnClears = new ArrayList(); // 未结
        if (eventName.equals("已结合同")) {
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("back_bill_state","已结合同");
            List<CargoReceipt> cargoReceipts  = cargoReceiptMapper.selectList(queryWrapper);
            for (CargoReceipt cargoReceipt : cargoReceipts) {
                QueryWrapper queryWrapper1 = new QueryWrapper();
                queryWrapper1.eq("back_bill_code",cargoReceipt.getGoodsRevertBillCode());
                DriverClear driverClear =  driverClearMapper.selectOne(queryWrapper1);
                driverCleareds.add(driverClear);
            }
            return driverCleareds;
        } else {
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("back_bill_state","未结合同");
            List<CargoReceipt> cargoReceipts  = cargoReceiptMapper.selectList(queryWrapper);
            for (CargoReceipt cargoReceipt : cargoReceipts) {
                QueryWrapper queryWrapper1 = new QueryWrapper();
                queryWrapper1.eq("back_bill_code",cargoReceipt.getGoodsRevertBillCode());
                DriverClear temp =  driverClearMapper.selectOne(queryWrapper1);
                if (temp == null) {
                    DriverClear driverClear = new DriverClear();
                    driverClear.setBackBillCode(cargoReceipt.getGoodsRevertBillCode());
                    driverClear.setDriverCode(cargoReceipt.getDriverId());
                    driverClear.setBalanceType("结出");
                    driverClear.setPrepayMoney(cargoReceipt.getStartAdvance()); // 预付金额
                    driverClear.setBindInsurance(cargoReceipt.getCarryGoodsInsurance()); // 定装保证金
                    driverClear.setDispatchServiceFee(cargoReceipt.getDispatchServiceFee()); // 配载服务费
                    driverClear.setInsurance(cargoReceipt.getInsurance());
                    driverClearMapper.insert(driverClear);
                    driverUnClears.add(driverClear);
                } else {
                    driverUnClears.add(temp);
                }
            }
            return driverUnClears;
        }
    }

    @Override
    public DriverClear selectByCargoReceiptCode(String goodsBillCode) {
        return driverClearMapper.selectById(goodsBillCode);
    }

    @Override
    public boolean driClear(DriverClear driverClear) {
        try {
            double carryFee = driverClear.getCarryFee(); // 承运费
            double bindInsurance = driverClear.getBindInsurance();// 定装保证金
            double addCarriage = driverClear.getAddCarriage(); // 加运费
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("goods_revert_bill_code",driverClear.getBackBillCode());
            double allCarriage = cargoReceiptMapper.selectOne(queryWrapper).getAllCarriage();
            double balance = carryFee + bindInsurance + addCarriage - driverClear.getPayedMoney(); // 余额
            driverClear.setBalance(balance);
            double money = allCarriage + bindInsurance + addCarriage;
            if (money != driverClear.getPayedMoney()) {
                driverClearMapper.insert(driverClear);
            } else {
                driverClearMapper.updateById(driverClear);
                CargoReceipt cargoReceipt = new CargoReceipt();
                cargoReceipt.setBackBillState("已结合同");
                UpdateWrapper updateWrapper = new UpdateWrapper();
                updateWrapper.eq("goods_revert_bill_code",driverClear.getBackBillCode());
                cargoReceiptMapper.update(cargoReceipt,updateWrapper);
            }
            return true;
        } catch (Exception e) {
            System.err.println("司机结算 插入失败！");
            return false;
        }
    }

    @Override
    public List<CustomerBillClear> selectCusclear(String eventName) {
        List<CustomerBillClear> customerBillCleareds = new ArrayList(); // 已结
        List<CustomerBillClear> customerBillUnClears = new ArrayList(); // 未结
        if (eventName.equals("已结运单")) {
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("event_name","已结运单");
            List<GoodsBillEvent> goodsBillEvents = goodsBillEventMapper.selectList(queryWrapper);
            for (GoodsBillEvent goodsBillEvent : goodsBillEvents) {
                CustomerBillClear customerBillClear = customerBillClearMapper.selectById(goodsBillEvent.getGoodsBillId());
                customerBillCleareds.add(customerBillClear);
            }
            return customerBillCleareds;
        } else {
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("event_name","未结");
            //queryWrapper.eq("event_name","未到");
            List<GoodsBillEvent> goodsBillEvents = goodsBillEventMapper.selectList(queryWrapper);
            for (GoodsBillEvent goodsBillEvent : goodsBillEvents) {
                GoodsBill goodsBill = goodsBillMapper.selectById(goodsBillEvent.getGoodsBillId());
                CustomerBillClear temp = customerBillClearMapper.selectById(goodsBillEvent.getGoodsBillId());
                if (temp == null) {
                    CustomerBillClear customerBillClear = new CustomerBillClear();
                    customerBillClear.setCustomerCode(goodsBill.getSendGoodsCustomerNo()); // 客户编号
                    customerBillClear.setGoodsBillCode(goodsBillEvent.getGoodsBillId()); // 货运单编号
                    customerBillClear.setCarriageReduceFund(goodsBill.getReduceFund()); // 减款
                    customerBillClear.setInsurance(goodsBill.getInsurance()); // 保险费
                    customerBillClear.setPayKickback(goodsBill.getPayKickback()); // 付回扣
                    customerBillClear.setCarryGoodsFee(goodsBill.getCarryGoodsFee());// 送货费
                    customerBillClear.setBalanceType("结入"); // 结算类型
                    customerBillClearMapper.insert(customerBillClear);
                    customerBillUnClears.add(customerBillClear);
                } else {
                    customerBillUnClears.add(temp);
                }

            }
            return customerBillUnClears;
        }
    }

    @Override
    public CustomerBillClear selectByBillCode(String goodsBillCode) {
        return customerBillClearMapper.selectById(goodsBillCode);
    }

    @Override
    public boolean cusClear(CustomerBillClear customerBillClear) {
        try {
            double billMoney = customerBillClear.getBillMoney(); // 本单
            double insurance = customerBillClear.getInsurance(); // 保险费
            double carriageReduceFund = customerBillClear.getCarriageReduceFund(); // 减款
            double prepayMoney = customerBillClear.getPrepayMoney(); // 预付金额
            double receivedMoney = customerBillClear.getReceivedMoney(); // 已收
            double moneyReceivable = billMoney + insurance - carriageReduceFund - prepayMoney; // 应收
            customerBillClear.setMoneyReceivable(moneyReceivable);
            double money = billMoney + insurance - carriageReduceFund;
            if (money != receivedMoney) {
                customerBillClearMapper.insert(customerBillClear);
            } else {
                customerBillClearMapper.updateById(customerBillClear);
                UpdateWrapper updateWrapper = new UpdateWrapper();
                updateWrapper.eq("goods_bill_id",customerBillClear.getGoodsBillCode());
                GoodsBillEvent goodsBillEvent = new GoodsBillEvent();
                goodsBillEvent.setEventName("已结运单");
                goodsBillEventMapper.update(goodsBillEvent,updateWrapper);
            }
            return true;
        } catch (Exception e) {
            System.err.println("客户结算 插入失败！");
            return false;
        }
    }

    @Override
    public List<ProxyFeeClear> selectHelpclear(String eventName) {
        List<ProxyFeeClear> proxyFeeCleareds = new ArrayList(); // 已结
        List<ProxyFeeClear> proxyFeeUnClears = new ArrayList(); // 未结
        if (eventName.equals("已结代收")) {
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("event_name","已结运单");
            List<GoodsBillEvent> goodsBillEvents = goodsBillEventMapper.selectList(queryWrapper);
            for (GoodsBillEvent goodsBillEvent : goodsBillEvents) {
                // 找到货运单主表
                GoodsBill goodsBill = goodsBillMapper.selectById(goodsBillEvent.getGoodsBillId());
                if (goodsBill != null){
                    // 是代收
                    if (goodsBill.getHelpAcceptPayment() != 0) {
                        QueryWrapper queryWrapper1 = new QueryWrapper();
                        queryWrapper1.eq("goods_bill_code",goodsBillEvent.getGoodsBillId());
                        ProxyFeeClear proxyFeeClear = proxyFeeClearMapper.selectOne(queryWrapper1);
                        if (proxyFeeClear != null) {
                            proxyFeeCleareds.add(proxyFeeClear);
                        }
                    }
                }
            }
            return proxyFeeCleareds;
        } else {
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("event_name","未结");
            List<GoodsBillEvent> goodsBillEvents = goodsBillEventMapper.selectList(queryWrapper);
            for (GoodsBillEvent goodsBillEvent : goodsBillEvents) {
                GoodsBill goodsBill = goodsBillMapper.selectById(goodsBillEvent.getGoodsBillId());// 找到货运单主表
                if (goodsBill != null){
                    if (goodsBill.getHelpAcceptPayment() != 0) { // 是代收
                        QueryWrapper queryWrapper1 = new QueryWrapper();
                        queryWrapper1.eq("goods_bill_code",goodsBillEvent.getGoodsBillId());
                        ProxyFeeClear temp = proxyFeeClearMapper.selectOne(queryWrapper1);
                        if (temp == null) {
                            ProxyFeeClear proxyFeeClear = new ProxyFeeClear();
                            proxyFeeClear.setGoodsBillCode(goodsBillEvent.getGoodsBillId()); // 货运单编号
                            proxyFeeClear.setCustomerCode(goodsBill.getSendGoodsCustomerNo()); // 客户名称
                            // 应收货款 算
                            proxyFeeClear.setFactReceiveFund(goodsBill.getHelpAcceptPayment()); // 实收货款
                            double goodsPayChange = goodsBill.getMoneyOfChangePay() - goodsBill.getHelpAcceptPayment(); // 变更
                            proxyFeeClear.setGoodsPayChange(goodsPayChange); // 变更
                            proxyFeeClear.setCommisionRate(0.02f); // 佣金率
                            // 已收佣金 填
                            // 应收佣金 算
                            // 结算时间 填
                            proxyFeeClearMapper.insert(proxyFeeClear);
                            proxyFeeUnClears.add(proxyFeeClear);
                        } else {
                            proxyFeeUnClears.add(temp);
                        }
                    }
                }
            }
            return proxyFeeUnClears;
        }
    }

    @Override
    public ProxyFeeClear selectByGoodsBillCode(String goodsBillCode) {
        return proxyFeeClearMapper.selectById(goodsBillCode);
    }

    @Override
    public boolean helpClear(ProxyFeeClear proxyFeeClear) {
        try {
            double factReceiveFund = proxyFeeClear.getFactReceiveFund(); // 实收
            double commisionRate = proxyFeeClear.getCommisionRate(); // 佣金率
            double receivedCommision = proxyFeeClear.getReceivedCommision(); // 已收佣金
            double commisionReceivable = factReceiveFund * commisionRate - receivedCommision; // 应收
            proxyFeeClear.setCommisionReceivable(commisionReceivable);
            if ((commisionRate * factReceiveFund > receivedCommision) || (commisionRate > 0 && factReceiveFund == 0)
                    || (commisionReceivable != 0)) {
                proxyFeeClearMapper.insert(proxyFeeClear);
            } else {
                proxyFeeClearMapper.insert(proxyFeeClear);
                UpdateWrapper updateWrapper = new UpdateWrapper();
                updateWrapper.eq("goods_bill_code",proxyFeeClear.getGoodsBillCode());
                GoodsBillEvent goodsBillEvent = new GoodsBillEvent();
                goodsBillEvent.setEventName("已结运单");
                goodsBillEventMapper.update(goodsBillEvent,updateWrapper);
            }
            return true;
        } catch (Exception e) {
            System.err.println("代收结算 插入失败！");
            return false;
        }
    }

    @Override
    public boolean saveExtraClear(ExtraClear extraClear) {
        try {
            extraClearMapper.insert(extraClear);
            return true;
        } catch (Exception e) {
            System.err.println("杂费结算 插入失败！");
            return false;
        }
    }

    @Override
    public Page<ExtraClear> selectAllExtraClearByPage(int pageNo, int pageSize) {
        Page<ExtraClear> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        Page<ExtraClear> pages = extraClearMapper.selectPage(page,queryWrapper);
        return pages;
    }
}
