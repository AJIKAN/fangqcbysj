package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.DriverAmount;
import com.gxust.fangqcbysj.entity.DriverClear;
import com.gxust.fangqcbysj.entity.dto.DriverClearDto;
import com.gxust.fangqcbysj.mapper.DriverAmountMapper;
import com.gxust.fangqcbysj.mapper.DriverClearMapper;
import com.gxust.fangqcbysj.service.DriverAmountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class DriverAmountServiceImpl extends ServiceImpl<DriverAmountMapper, DriverAmount> implements DriverAmountService {

    @Autowired
    private DriverAmountMapper driverAmountMapper;

    @Autowired
    private DriverClearMapper driverClearMapper;

    @Override
    public List<DriverAmount> selectAllDriAcount() {
        List<DriverClearDto> strings = driverClearMapper.find();
        List<DriverAmount> driverAmounts = new ArrayList<>();
        for (DriverClearDto clearDto : strings) {
            DriverAmount driverAmount = new DriverAmount();
            driverAmount.setDriverCode(clearDto.getDriverCode());
            driverAmount.setCarryFeeTotal(Double.parseDouble(clearDto.getCarryFeeTotal()));
            driverAmount.setAddCarriageTotal(Double.parseDouble(clearDto.getAddCarriageTotal()));
            driverAmount.setTotal(Double.parseDouble(clearDto.getTotal()));
            driverAmountMapper.insert(driverAmount);
            driverAmounts.add(driverAmount);
        }
        return driverAmounts;
    }
}
