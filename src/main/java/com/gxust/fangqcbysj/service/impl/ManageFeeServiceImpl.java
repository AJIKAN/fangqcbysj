package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.ManageFee;
import com.gxust.fangqcbysj.mapper.ManageFeeMapper;
import com.gxust.fangqcbysj.service.ManageFeeService;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Service
public class ManageFeeServiceImpl extends ServiceImpl<ManageFeeMapper, ManageFee> implements ManageFeeService {
}
