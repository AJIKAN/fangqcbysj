package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.LineOverall;
import com.gxust.fangqcbysj.entity.dto.CargoReceiptDto;
import com.gxust.fangqcbysj.mapper.CargoReceiptMapper;
import com.gxust.fangqcbysj.mapper.LineOverallMapper;
import com.gxust.fangqcbysj.service.LineOverallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Service
public class LineOverallServiceImpl extends ServiceImpl<LineOverallMapper, LineOverall> implements LineOverallService {

    @Autowired
    private LineOverallMapper lineOverallMapper;

    @Autowired
    private CargoReceiptMapper cargoReceiptMapper;

    @Override
    public List<LineOverall> printAllLineOverall() {
        List<CargoReceiptDto> cargoReceiptDtos = cargoReceiptMapper.find();
        List<LineOverall> lineOveralls = new ArrayList<>();
        for(CargoReceiptDto cargoReceiptDto : cargoReceiptDtos) {
            LineOverall overall = new LineOverall();
            overall.setLoadStation(cargoReceiptDto.getLoadStation());
            overall.setDealGoodsStation(cargoReceiptDto.getDealGoodsStation());
            overall.setAllCarriageTotal(Double.parseDouble(cargoReceiptDto.getAllCarriageTotal()));
            overall.setInsuranceTotal(Double.parseDouble(cargoReceiptDto.getInsuranceTotal()));
            overall.setTimes(Integer.parseInt(cargoReceiptDto.getTimes()));
            lineOverallMapper.insert(overall);
            lineOveralls.add(overall);
        }
        return lineOveralls;
    }
}
