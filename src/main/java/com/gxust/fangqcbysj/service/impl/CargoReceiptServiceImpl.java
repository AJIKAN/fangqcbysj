package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.mapper.*;
import com.gxust.fangqcbysj.service.CargoReceiptService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class CargoReceiptServiceImpl extends ServiceImpl<CargoReceiptMapper, CargoReceipt> implements CargoReceiptService {

    @Autowired
    private CargoReceiptMapper cargoReceiptMapper;

    @Autowired
    private RegionMapper regionMapper;

    @Autowired
    private RouteInfoMapper routeInfoMapper;

    @Autowired
    private CargoReceiptDetailMapper cargoReceiptDetailMapper;

    @Autowired
    private GoodsBillMapper goodsBillMapper;

    @Autowired
    private BillInfoMapper billInfoMapper;

    @Autowired
    private GoodsBillEventMapper goodsBillEventMapper;


    @Override
    public boolean addCargoReceipt(CargoReceipt cargoReceipt) {
        try {
            System.out.println("cargoReceipt = " + cargoReceipt);
            if (StringUtils.isNotEmpty(cargoReceipt.getGoodsRevertBillCode())){
                // 保存货运回执单
                cargoReceipt.setBackBillState("未出合同");
                cargoReceiptMapper.insert(cargoReceipt);
                // 更新货运单中的中转地和中转费信息
                String loadStation = cargoReceipt.getLoadStation();
                String dealGoodsStation = cargoReceipt.getDealGoodsStation();
                QueryWrapper queryWrapper = new QueryWrapper();
                queryWrapper.eq("city",loadStation);
                QueryWrapper queryWrapper1 = new QueryWrapper();
                queryWrapper1.eq("city",dealGoodsStation);
                int startStation = regionMapper.selectOne(queryWrapper).getId();
                int endStation = regionMapper.selectOne(queryWrapper1).getId();
                QueryWrapper queryWrapper2 = new QueryWrapper();
                queryWrapper2.eq("start_station",startStation);
                queryWrapper2.eq("end_station",endStation);
                List<RouteInfo> routeList = routeInfoMapper.selectList(queryWrapper2);
                String passStation = routeList.get(0).getPassStation();
                for (int i = 1; i < routeList.size(); i++) {
                    String temp = routeList.get(i).getPassStation();
                    passStation = (temp.length() < passStation.length() ? temp : passStation);
                }
                String[] pass_station = passStation.split(",");
                String goodsBillCode = cargoReceiptDetailMapper.selectById(cargoReceipt.getGoodsRevertBillCode()).getGoodsBillDetailId();
                GoodsBill goodsBill = goodsBillMapper.selectById(goodsBillCode);
                double transfer_fee = 1.3 * pass_station.length;
                goodsBill.setTransferFee(transfer_fee);
                String result = "";
                if (StringUtils.isNotEmpty(passStation)){
                    for (int i = 0; i < pass_station.length; i++) {
                        String station_name = regionMapper.selectById(Integer.valueOf(pass_station[i])).getCity();
                        result += (i == 0 ? "" : ",");
                        result += station_name;
                    }
                }
                goodsBill.setTransferStation(result);
                goodsBillMapper.updateById(goodsBill);
                // 保存单据明细表
                BillInfo billInfo = new BillInfo();
                billInfo.setBillType("货运回执单");
                billInfo.setAcceptStation(cargoReceipt.getAcceptStation());
                billInfo.setBillCode(cargoReceipt.getGoodsRevertBillCode());
                billInfo.setBillState("已填");
                billInfo.setWriteDate(new Date());
                billInfoMapper.insert(billInfo);
                return true;
            }else {
                return false;
            }
        } catch (Exception e) {
            System.err.println("货运回执单添加失败");
            return false;
        }
    }

    @Override
    public List<String> selectAllCodes() {
        List<CargoReceiptDetail> cargoReceiptDetails = cargoReceiptDetailMapper.selectList(null);
        List<String> newList = cargoReceiptDetails.stream().map(CargoReceiptDetail::getGoodsRevertBillId).collect(Collectors.toList());
        return newList;
    }

    @Override
    public List<?> selectLeftCodes() {
        return cargoReceiptDetailMapper.findLeftCodes();
    }

    @Override
    public GoodsBill selectGoodsBill(String goodsRevertBillCode) {
        String goodsBillCode = cargoReceiptDetailMapper.selectById(goodsRevertBillCode).getGoodsBillDetailId();
        return goodsBillMapper.selectById(goodsBillCode);
    }

    @Override
    public Page<CargoReceipt> selectAll(int pageNo, int pageSize) {
        Page<CargoReceipt> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        Page<CargoReceipt> pages = cargoReceiptMapper.selectPage(page,queryWrapper);
        return pages;
    }

    @Override
    public Page<CargoReceipt> selectByEvent(String backBillState, int pageNo, int pageSize) {
        Page<CargoReceipt> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("back_bill_state",backBillState);
        Page<CargoReceipt> pages = cargoReceiptMapper.selectPage(page,queryWrapper);
        return pages;
    }

    @Override
    public CargoReceipt selectByCode(String goodsRevertBillCode) {
        System.out.println("goodsRevertBillCode = " + goodsRevertBillCode);
        return cargoReceiptMapper.selectById(goodsRevertBillCode);
    }

    @Override
    public boolean updateCargoReceipt(CargoReceipt cargoReceipt) {
        try {
            cargoReceiptMapper.updateById(cargoReceipt);
            return true;
        } catch (Exception e) {
            System.err.println("货运回执单更新失败");
            return false;
        }
    }

    @Override
    public boolean submitCargoReceipt(CargoReceipt cargoReceipt) {
        try {
            cargoReceipt.setBackBillState("未到车辆");
            cargoReceiptMapper.updateById(cargoReceipt);
            String goodsBillCode = cargoReceiptDetailMapper.selectById(cargoReceipt.getGoodsRevertBillCode()).getGoodsBillDetailId();
            GoodsBillEvent goodsBillEvent = goodsBillEventMapper.selectById(goodsBillCode);
            goodsBillEvent.setEventName("未到");
            goodsBillEvent.setOccurTime(new Date());
            goodsBillEventMapper.updateById(goodsBillEvent);
            return true;
        } catch (Exception e) {
            System.err.println("货运回执单提交失败");
            return false;
        }
    }

    @Override
    public boolean deleteCargoReceipt(String goodsRevertBillCode) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("bill_code",goodsRevertBillCode);
        BillInfo billInfo = billInfoMapper.selectOne(queryWrapper);
        billInfo.setBillState("作废");
        billInfo.setWriteDate(new Date());
        try {
            cargoReceiptMapper.deleteById(goodsRevertBillCode);
            return true;
        } catch (Exception e) {
            System.err.println("货运回执单删除失败");
            return false;
        }
    }

    @Override
    public List<CargoReceipt> findByDriverId(String driverId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("driver_id",driverId);
        return cargoReceiptMapper.selectList(queryWrapper);
    }

    @Override
    public Page<CargoReceipt> findByDriverIdAndState(String driverId, String backBillState, int pageNo, int pageSize) {
        Page<CargoReceipt> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("driver_id",driverId);
        queryWrapper.eq("back_bill_state",backBillState);
        Page<CargoReceipt> pages = cargoReceiptMapper.selectPage(page,queryWrapper);
        return pages;
    }

    @Override
    public CargoReceiptDetail findByGoodsBillCode(String goodsBillCode) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("goods_bill_detail_id",goodsBillCode);
        return cargoReceiptDetailMapper.selectOne(queryWrapper);
    }
}
