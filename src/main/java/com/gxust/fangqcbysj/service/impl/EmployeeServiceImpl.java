package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.dto.EmployeeDto;
import com.gxust.fangqcbysj.entity.*;
import com.gxust.fangqcbysj.mapper.EmployeeMapper;
import com.gxust.fangqcbysj.mapper.UserGroupMapper;
import com.gxust.fangqcbysj.mapper.UserMapper;
import com.gxust.fangqcbysj.mapper.UserWithGroupMapper;
import com.gxust.fangqcbysj.service.EmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Random;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserGroupMapper userGroupMapper;

    @Autowired
    UserWithGroupMapper userWithGroupMapper;

    @Override
    public boolean addEmployee(EmployeeDto employeeDto) {
        try {
            String department = employeeDto.getDepartment();
            String employeeCode = "";
            employeeCode += departmentPrefix(department);
            while (true) {
                employeeCode += randomCode();
                QueryWrapper queryWrapper = new QueryWrapper();
                queryWrapper.eq("employee_code",employeeCode);
                Employee employee1 = employeeMapper.selectOne(queryWrapper);
                if (employee1 == null) {
                    break;
                }
            }
            employeeDto.setCondition(1);
            if (employeeDto.getCondition() == 1) {
                User user = new User();
                user.setIfOnline(0);
                user.setLoginId(employeeCode);
                user.setPassword("E10ADC3949BA59ABBE56E057F20F883E");
                int insert = userMapper.insert(user);
                System.out.println(insert);
            }
            // 添加职工与组情况
            QueryWrapper queryWrapper1 = new QueryWrapper();
            queryWrapper1.eq("group_name",department);
            UserGroup userGroup = userGroupMapper.selectOne(queryWrapper1);
            int groupId = userGroup.getId();
            UserWithGroup userWithGroup = new UserWithGroup();
            userWithGroup.setGroupId(groupId);
            userWithGroup.setUserId(employeeCode);
            userWithGroupMapper.insert(userWithGroup);
            employeeDto.setEmployeeCode(employeeCode);
            Employee employee = new Employee();
            BeanUtils.copyProperties(employeeDto,employee);
            employee.setEmployeeCode(employeeCode);
            employeeMapper.insert(employee);
            return true;
        } catch (Exception e) {
            System.err.println("职员 | 用户 | 用户组关系表 信息插入失败！");
            return false;
        }
    }

    @Override
    public boolean deleteEmployee(String employeeCode) {
        try {
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("employee_code",employeeCode);
            employeeMapper.delete(queryWrapper);
            QueryWrapper queryWrapper1 = new QueryWrapper();
            queryWrapper1.eq("user_id",employeeCode);
            userWithGroupMapper.delete(queryWrapper1);
            QueryWrapper queryWrapper2 = new QueryWrapper();
            queryWrapper2.eq("login_id",employeeCode);
            User user = userMapper.selectOne(queryWrapper2);
            if (user != null) {
                userMapper.delete(queryWrapper2);
            }
            return true;
        } catch (Exception e) {
            System.err.println("职员信息删除失败！");
            return false;
        }
    }

    @Override
    public Page<Employee> selectAllEmpByPage(int pageNo, int pageSize) {
        Page<Employee> page = new Page<>(pageNo, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        Page<Employee> pages = employeeMapper.selectPage(page,queryWrapper);
        return pages;
    }

    @Override
    public Employee selectByEmployeeCode(String employeeCode) {
        return employeeMapper.selectById(employeeCode);
    }

    @Override
    public boolean updateEmployee(EmployeeDto employeeDto) {
        Employee emp = employeeMapper.selectById(employeeDto.getEmployeeCode());
        try {
            if (!emp.getDepartment().equals(employeeDto.getDepartment())) {
                String department = employeeDto.getDepartment();
                QueryWrapper queryWrapper = new QueryWrapper();
                queryWrapper.eq("employee_code",employeeDto.getEmployeeCode());
                employeeMapper.delete(queryWrapper);
                emp.setDepartment(department);

                //更新工号
                String newEmployeeCode = "";
                newEmployeeCode += departmentPrefix(department);
                while (true) {
                    newEmployeeCode += randomCode();
                    if (employeeMapper.selectById(newEmployeeCode) == null) {
                        break;
                    }
                }
                emp.setEmployeeCode(newEmployeeCode);
                // 更新职工与组情况
                QueryWrapper queryWrapper1 = new QueryWrapper();
                queryWrapper1.eq("group_name",department);
                UserGroup userGroup = userGroupMapper.selectOne(queryWrapper1);
                int groupId = userGroup.getId();
                QueryWrapper queryWrapper2 = new QueryWrapper();
                queryWrapper2.eq("user_id",employeeDto.getEmployeeCode());
                UserWithGroup userWithGroup = userWithGroupMapper.selectOne(queryWrapper2);
                userWithGroup.setGroupId(groupId);
                userWithGroupMapper.insert(userWithGroup);
            }
            emp.setBirthday(employeeDto.getBirthday());
            emp.setPosition(employeeDto.getPosition());
            employeeMapper.updateById(emp);
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("login_id",employeeDto.getEmployeeCode());
            User user = userMapper.selectOne(queryWrapper);
            if (user == null && employeeDto.getCondition() == 1) {
                user = new User(employeeDto.getEmployeeCode(), "E10ADC3949BA59ABBE56E057F20F883E", 0);
                userMapper.insert(user);
            } else if (user != null && employeeDto.getCondition() == 0) {
                userMapper.delete(queryWrapper);
            }
            return true;
        } catch (Exception e) {
            System.err.println("职员 | 用户 | 用户组关系表 信息更新失败！");
            return false;
        }
    }

    private String randomCode() {
        StringBuilder stringBuilder = new StringBuilder();
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < 6; i++) {
            stringBuilder.append(random.nextInt(10));
        }
        return stringBuilder.toString();
    }

    private String departmentPrefix(String department) {
        String result = "";
        if (department.equals("管理组")) {
            result = "GL";
        } else if (department.equals("票据组")) {
            result = "PJ";
        } else if (department.equals("财务组")) {
            result = "CW";
        } else if (department.equals("客服组")) {
            result = "KF";
        } else if (department.equals("监控组")) {
            result = "JK";
        }
        return result;
    }
}
