package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.CityExpand;
import com.gxust.fangqcbysj.entity.Region;
import com.gxust.fangqcbysj.entity.RouteInfo;
import com.gxust.fangqcbysj.mapper.CityExpandMapper;
import com.gxust.fangqcbysj.mapper.RegionMapper;
import com.gxust.fangqcbysj.mapper.RouteInfoMapper;
import com.gxust.fangqcbysj.service.RouteInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Service
public class RouteInfoServiceImpl extends ServiceImpl<RouteInfoMapper, RouteInfo> implements RouteInfoService {

    @Autowired
    private RegionMapper regionMapper;

    @Autowired
    private RouteInfoMapper routeInfoMapper;

    @Autowired
    private CityExpandMapper cityExpandMapper;


    private List<List<Integer>> routeList;

    @Override
    public void generateRoute() {
        routeList = new LinkedList<>();
        List<Region> list = regionMapper.selectList(null);
        for (Region region : list) {
            List<Integer> temp = new LinkedList<>();
            int cityId = region.getId();
            temp.add(cityId);
            dfs(cityId, temp);
        }
        for (List<Integer> route : routeList) {
            RouteInfo routeInfo = new RouteInfo();
            routeInfo.setStartStation(route.get(0));
            routeInfo.setEndStation(route.get(route.size()-1));
            String passStation = "";
            for (int i = 1; i < route.size()-1; i++) {
                passStation += (i == 1 ? "" : ",");
                passStation += route.get(i);
            }
            routeInfo.setPassStation(passStation);
            routeInfo.setDistance(Double.valueOf((route.size()-1) * 100));
            routeInfo.setFetchTime(Double.valueOf(route.size()-1));
            routeInfoMapper.insert(routeInfo);
        }
    }

    @Override
    public List<RouteInfo> findAllRouteInfos() {
        return routeInfoMapper.selectList(null);
    }


    public void dfs(int id, List<Integer> temp) {
        QueryWrapper<CityExpand> queryWrapper = new QueryWrapper();
        queryWrapper.eq("city_id",id);
        // queryWrapper.eq("range_city",);
        List<CityExpand> cityExpand = cityExpandMapper.selectList(queryWrapper);
        if (cityExpand == null) {
            return ;
        }
        // String[] rangeCity = cityExpand.getRangeCity().split(",");
        List<Integer> rangeCity = new ArrayList<>();
        for (CityExpand expand : cityExpand) {
            rangeCity.add(expand.getCityId());
        }
        for (Integer integer : rangeCity) {
            Integer rangeCityId = integer;
            if (temp.contains(rangeCityId)) {
                continue;
            }
            temp.add(rangeCityId);
            dfs(rangeCityId, temp);
            routeList.add(new LinkedList<>(temp));
            temp.remove(rangeCityId);
        }
        return ;
    }
}
