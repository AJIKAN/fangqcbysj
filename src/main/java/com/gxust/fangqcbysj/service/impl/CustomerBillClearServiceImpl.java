package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.CustomerBillClear;
import com.gxust.fangqcbysj.mapper.CustomerBillClearMapper;
import com.gxust.fangqcbysj.service.CustomerBillClearService;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class CustomerBillClearServiceImpl extends ServiceImpl<CustomerBillClearMapper, CustomerBillClear> implements CustomerBillClearService {
}
