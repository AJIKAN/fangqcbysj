package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.CityExpand;
import com.gxust.fangqcbysj.entity.Region;
import com.gxust.fangqcbysj.entity.dto.CityExpandDto;
import com.gxust.fangqcbysj.mapper.CityExpandMapper;
import com.gxust.fangqcbysj.mapper.RegionMapper;
import com.gxust.fangqcbysj.mapper.RouteInfoMapper;
import com.gxust.fangqcbysj.service.CityExpandService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class CityExpandServiceImpl extends ServiceImpl<CityExpandMapper, CityExpand> implements CityExpandService {

    @Autowired
    private CityExpandMapper cityExpandMapper;

    @Autowired
    private RouteInfoMapper routeInfoMapper;

    @Autowired
    private RegionMapper regionMapper;

    @Override
    public boolean addExpand(CityExpand cityExpand) {
        try {
            cityExpandMapper.insert(cityExpand);
            // routeInfoDao.truncateTable();
            return true;
        } catch (Exception e) {
            System.err.println("城市扩充表添加失败 | 路线信息表清空失败");
            return false;
        }
    }

    @Override
    public boolean deleteExpand(Integer id) {
        try {
            cityExpandMapper.deleteById(id);
            // routeInfoDao.truncateTable();
            return true;
        } catch (Exception e) {
            System.err.println("城市扩充表添加失败 | 路线信息表清空失败");
            return false;
        }
    }

    @Override
    public boolean updateExpand(CityExpand cityExpand) {
        try{
            UpdateWrapper<CityExpand> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("id",cityExpand.getId()).set("city_id",cityExpand.getCityId()).set("range_city",cityExpand.getRangeCity());
            cityExpandMapper.update(cityExpand,updateWrapper);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Region> findAllRegions() {
        List<Region> regionList = regionMapper.selectList(null);
        return regionList;
    }

    @Override
    public List<Region> findLeftRegions() {
        return regionMapper.findLeftRegions();
    }

    @Override
    public List<CityExpandDto> findAllExpands(int pageNo, int pageSize) {
        List<CityExpandDto> list = new ArrayList<>();
        Page<CityExpand> page = new Page<>(pageNo, pageSize);
        Page<CityExpand> cityExpandPage = cityExpandMapper.selectPage(page, null);
        for (CityExpand cityExpand : cityExpandPage.getRecords()) {
            CityExpandDto cityExpandDto = new CityExpandDto();
            BeanUtils.copyProperties(cityExpand,cityExpandDto);
            Region region = regionMapper.selectById(cityExpand.getCityId());
            cityExpandDto.setCity(region.getCity());
            String rangCity = cityExpand.getRangeCity();
            StringBuffer rangeCityName = new StringBuffer();
            for (String s : rangCity.split(",")) {
                Region region1 = regionMapper.selectById(Integer.parseInt(s));
                rangeCityName.append(region1.getCity());
                rangeCityName.append(",");
            }
            rangeCityName.deleteCharAt(rangeCityName.length()-1);
            cityExpandDto.setRangeCityName(rangeCityName.toString());
            list.add(cityExpandDto);
        }
        return list;
    }

    @Override
    public CityExpand findById(int id) {
        return cityExpandMapper.selectById(id);
    }
}
