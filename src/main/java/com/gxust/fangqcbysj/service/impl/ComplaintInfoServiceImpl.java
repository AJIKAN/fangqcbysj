package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.ComplaintInfo;
import com.gxust.fangqcbysj.mapper.ComplaintInfoMapper;
import com.gxust.fangqcbysj.service.ComplaintInfoService;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class ComplaintInfoServiceImpl extends ServiceImpl<ComplaintInfoMapper, ComplaintInfo> implements ComplaintInfoService {
}
