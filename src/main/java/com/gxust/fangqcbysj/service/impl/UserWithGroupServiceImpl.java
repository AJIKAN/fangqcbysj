package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.UserWithGroup;
import com.gxust.fangqcbysj.mapper.UserWithGroupMapper;
import com.gxust.fangqcbysj.service.UserWithGroupService;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Service
public class UserWithGroupServiceImpl extends ServiceImpl<UserWithGroupMapper, UserWithGroup> implements UserWithGroupService {
}
