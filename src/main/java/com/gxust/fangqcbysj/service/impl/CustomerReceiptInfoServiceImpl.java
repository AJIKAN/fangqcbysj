package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.CustomerReceiptInfo;
import com.gxust.fangqcbysj.entity.Vo.CustomerReceiptInfoVo;
import com.gxust.fangqcbysj.mapper.CustomerReceiptInfoMapper;
import com.gxust.fangqcbysj.service.CustomerReceiptInfoService;
import com.gxust.fangqcbysj.utils.DateUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class CustomerReceiptInfoServiceImpl extends ServiceImpl<CustomerReceiptInfoMapper,CustomerReceiptInfo> implements CustomerReceiptInfoService {

    @Autowired
    private CustomerReceiptInfoMapper customerReceiptInfoMapper;

    @Override
    public boolean addCustomerReceiptInfo(CustomerReceiptInfoVo customerReceiptInfoVo) {
        try {
            CustomerReceiptInfo customerReceiptInfo = new CustomerReceiptInfo();
            BeanUtils.copyProperties(customerReceiptInfoVo,customerReceiptInfo);
            String carryBillEventId = randomCode();
            customerReceiptInfo.setCarryBillEventId(Integer.parseInt(carryBillEventId));
            customerReceiptInfo.setReceiveGoodsDate(DateUtil.parse(customerReceiptInfoVo.getReceiveGoodsDate(),"yyyy-MM-dd"));
            customerReceiptInfoMapper.insert(customerReceiptInfo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("顾客回执信息添加失败");
            return false;
        }
    }

    private String randomCode() {
        StringBuilder stringBuilder = new StringBuilder();
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < 6; i++) {
            stringBuilder.append(random.nextInt(10));
        }
        return stringBuilder.toString();
    }
}
