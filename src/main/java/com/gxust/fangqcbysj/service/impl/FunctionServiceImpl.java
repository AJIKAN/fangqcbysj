package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.Function;
import com.gxust.fangqcbysj.mapper.FunctionMapper;
import com.gxust.fangqcbysj.service.FunctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class FunctionServiceImpl extends ServiceImpl<FunctionMapper, Function> implements FunctionService {


    @Autowired
    private FunctionMapper functionMapper;

    @Override
    public List<Function> findAllFunction() {
        return functionMapper.selectList(null);
    }
}
