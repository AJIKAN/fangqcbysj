package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.CargoError;
import com.gxust.fangqcbysj.mapper.CargoErrorMapper;
import com.gxust.fangqcbysj.service.CargoErrorService;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class CargoErrorServiceImpl extends ServiceImpl<CargoErrorMapper, CargoError> implements CargoErrorService {
}
