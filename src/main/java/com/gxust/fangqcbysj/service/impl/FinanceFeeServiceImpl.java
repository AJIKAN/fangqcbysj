package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.FinanceFee;
import com.gxust.fangqcbysj.mapper.FinanceFeeMapper;
import com.gxust.fangqcbysj.service.FinanceFeeService;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class FinanceFeeServiceImpl extends ServiceImpl<FinanceFeeMapper, FinanceFee> implements FinanceFeeService {
}
