package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.ProxyFeeClear;
import com.gxust.fangqcbysj.mapper.ProxyFeeClearMapper;
import com.gxust.fangqcbysj.service.ProxyFeeClearService;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Service
public class ProxyFeeClearServiceImpl extends ServiceImpl<ProxyFeeClearMapper, ProxyFeeClear> implements ProxyFeeClearService {
}
