package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.GoodsReceiptInfo;
import com.gxust.fangqcbysj.mapper.GoodsReceiptInfoMapper;
import com.gxust.fangqcbysj.service.GoodsReceiptInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Service
public class GoodsReceiptInfoServiceImpl extends ServiceImpl<GoodsReceiptInfoMapper,GoodsReceiptInfo> implements GoodsReceiptInfoService{

    @Autowired
    private GoodsReceiptInfoMapper goodsReceiptInfoMapper;

    @Override
    public boolean addGoodsReceipt(GoodsReceiptInfo goodsReceiptInfo) {
        try {
            goodsReceiptInfoMapper.insert(goodsReceiptInfo);
            return true;
        } catch (Exception e) {
            System.err.println("货物回执信息添加失败");
            return false;
        }
    }
}
