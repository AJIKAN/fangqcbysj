package com.gxust.fangqcbysj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxust.fangqcbysj.entity.ExtraClear;
import com.gxust.fangqcbysj.mapper.ExtraClearMapper;
import com.gxust.fangqcbysj.service.ExtraClearService;
import org.springframework.stereotype.Service;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
@Service
public class ExtraClearServiceImpl extends ServiceImpl<ExtraClearMapper, ExtraClear> implements ExtraClearService {
}
