package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.CustomerReceiptInfo;
import com.gxust.fangqcbysj.entity.GoodsBill;
import com.gxust.fangqcbysj.entity.TransferComInfo;
import com.gxust.fangqcbysj.entity.TransferInfo;
import com.gxust.fangqcbysj.entity.Vo.TransferInfoVo;


import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface TransferInfoService extends IService<TransferInfo> {

    /**
     * 添加中转公司信息
     * @param transferComInfo transferComInfo
     * @return boolean
     */
    boolean addCompany(TransferComInfo transferComInfo);

    /**
     * 添加中转信息
     * @param transferInfo transferInfo
     * @return boolean
     */
    boolean addTransferInfo(TransferInfo transferInfo);

    /**
     * 列表分页
     */
    Page<TransferComInfo> findAllByPage(int pageNo,int pageSize, TransferComInfo transferComInfo);

    /**
     * 查询运单的中转详情
     * @param goodsBillCode goodsBillCode
     * @return TransferComInfo
     */
    TransferComInfo findByGoodsBillCode(String goodsBillCode);


    List<GoodsBill> findOnWayBills();

    IPage<TransferInfo> findInfoByPage(int pageNo,int pageSize, TransferInfoVo transferInfoVo);

    IPage<CustomerReceiptInfo> findCusRecPage(int pageNo,int pageSize,String receiveGoodsPerson);
}
