package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.BillInfo;
import com.gxust.fangqcbysj.entity.BillRelease;
import com.gxust.fangqcbysj.entity.GoodsReceiptInfo;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface BillReleaseService extends IService<BillRelease> {

    boolean addRelease(BillRelease billRelease);

    boolean addGoodsReceipt(GoodsReceiptInfo goodsReceiptInfo);

    Page<BillInfo> findAllByPage(int pageNo, int pageSize);

    Page<BillInfo> findNotRelease(int pageNo, int pageSize);
}
