package com.gxust.fangqcbysj.service;

/**
 * @Author 方全朝
 * @Description
 * @Date 2021/3/14
 */
public interface IMapApiService {
    /**
     * 根据两个地址获取公里数的接口
     *
     * @param address
     * @param otherAddress
     * @return
     */
    Double getDistanceByTwoPlace(String address, String otherAddress);
}
