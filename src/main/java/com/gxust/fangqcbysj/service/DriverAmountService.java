package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.DriverAmount;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface DriverAmountService extends IService<DriverAmount> {

    /**
     * 打印司机用量
     * @return List<DriverAmount>
     */
    List<DriverAmount> selectAllDriAcount();
}
