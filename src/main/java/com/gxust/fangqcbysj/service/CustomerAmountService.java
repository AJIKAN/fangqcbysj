package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.CustomerAmount;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface CustomerAmountService extends IService<CustomerAmount> {

    /**
     * 打印客户用量
     * @return List<CustomerAmount>
     */
    List<CustomerAmount> selectAllCusAcount();
}
