package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.ContactsService;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface ContactsServiceService extends IService<ContactsService> {

    List<ContactsService> printAllContactsService();

    ContactsService selectByGoodsBillCode(String goodsBillCode);
}
