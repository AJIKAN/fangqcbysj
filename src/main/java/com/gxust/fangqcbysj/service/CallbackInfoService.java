package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.CallbackInfo;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface CallbackInfoService extends IService<CallbackInfo> {

    boolean addInfo(CallbackInfo callbackInfo);

    CallbackInfo findDetail(String goodsBillId,String type);
}
