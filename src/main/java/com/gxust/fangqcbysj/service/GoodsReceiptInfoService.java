package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.GoodsReceiptInfo;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface GoodsReceiptInfoService extends IService<GoodsReceiptInfo> {

    boolean addGoodsReceipt(GoodsReceiptInfo goodsReceiptInfo);
}
