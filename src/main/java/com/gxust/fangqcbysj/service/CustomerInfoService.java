package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.CustomerInfo;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface CustomerInfoService extends IService<CustomerInfo> {

    boolean addCustomer(CustomerInfo customer);

    boolean deleteCustomer(String customerCode);

    Page<CustomerInfo>  selectAllCusByPage(int pageNo, int pageSize);

    CustomerInfo selectByCustomerCode(String customerCode);

    List<String> selectAllCusCode();

    boolean updateCustomer(String customerCode,CustomerInfo customerInfo);
}
