package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.BillInfo;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface BillInfoService extends IService<BillInfo> {
}
