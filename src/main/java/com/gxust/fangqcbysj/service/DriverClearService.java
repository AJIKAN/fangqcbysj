package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.CustomerBillClear;
import com.gxust.fangqcbysj.entity.DriverClear;
import com.gxust.fangqcbysj.entity.ExtraClear;
import com.gxust.fangqcbysj.entity.ProxyFeeClear;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/4/30
 */
public interface DriverClearService extends IService<DriverClear> {

    List<DriverClear> selectDrclear(String eventName);

    DriverClear selectByCargoReceiptCode(String goodsBillCode);

    boolean driClear(DriverClear driverClear);

    List<CustomerBillClear> selectCusclear(String eventName);

    CustomerBillClear selectByBillCode(String goodsBillCode);

    boolean cusClear(CustomerBillClear customerBillClear);

    List<ProxyFeeClear> selectHelpclear(String eventName);

    ProxyFeeClear selectByGoodsBillCode(String goodsBillCode);

    boolean helpClear(ProxyFeeClear proxyFeeClear);

    boolean saveExtraClear(ExtraClear extraClear);

    Page<ExtraClear> selectAllExtraClearByPage(int pageNo, int pageSize);
}
