package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.dto.FunctionWithGroupDto;
import com.gxust.fangqcbysj.entity.UserGroup;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface UserGroupService extends IService<UserGroup> {

    boolean addGroup(UserGroup userGroup);

    boolean addFuncGro(FunctionWithGroupDto functionWithGroupDto);

    boolean delFuncGro(int id);

    Page<UserGroup> findAllByPage(int pageNo, int  pageSize);

    UserGroup findOne(int id);

    boolean updateUserGroup(int id, String description);

    List<UserGroup> findAll();
}
