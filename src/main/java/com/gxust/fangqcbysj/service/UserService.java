package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.User;
import com.gxust.fangqcbysj.mapper.UserMapper;

/**
 * @Author 方全朝
 * @Description
 * @Date 2021/3/27
 */
public interface UserService extends IService<User>{

    /**
     * 登录
     * @param loginId id
     * @param password 密码
     * @return user
     */
    User login(String loginId,String password);

    /**
     * 修改密码
     * @param loginId id
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     * @return 布尔
     */
    Boolean changePassword(String loginId, String oldPassword, String newPassword);

    boolean ifExist(String loginId);
}
