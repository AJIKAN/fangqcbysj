package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.RouteInfo;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface RouteInfoService extends IService<RouteInfo> {

    void generateRoute();

    /**
     * 得到所有线路信息
     * @return List<RouteInfo>
     */
    List<RouteInfo> findAllRouteInfos();
}
