package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.dto.EmployeeDto;
import com.gxust.fangqcbysj.entity.Employee;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
public interface EmployeeService extends IService<Employee> {

    boolean addEmployee(EmployeeDto employeeDto);

    boolean deleteEmployee(String employeeCode);

    Page<Employee> selectAllEmpByPage(int pageNo,int pageSize);

    Employee selectByEmployeeCode(String employeeCode);

    boolean updateEmployee(EmployeeDto employeeDto);
}
