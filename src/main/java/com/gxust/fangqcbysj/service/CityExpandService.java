package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.CityExpand;
import com.gxust.fangqcbysj.entity.Region;
import com.gxust.fangqcbysj.entity.dto.CityExpandDto;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
public interface CityExpandService extends IService<CityExpand> {

    /**
     * 新增城市扩充信息
     * @param cityExpand
     * @return
     */
    boolean addExpand(CityExpand cityExpand);

    /**
     *
     * @param id Integer
     * @return boolean
     */
    boolean deleteExpand(Integer id);

    /**
     * 更新一条城市扩充信息
     * @param cityExpand
     * @return
     */
    boolean updateExpand(CityExpand cityExpand);

    /**
     * 得到所有城市
     * @return List<Region>
     */
    List<Region> findAllRegions();

    /**
     * 得到无范围的城市
     * @return List<Region>
     */
    List<Region> findLeftRegions();

    /**
     * 得到所有的城市范围信息
     * @param pageNo int
     * @param pageSize int
     * @return Page<CityExpand>
     */
    List<CityExpandDto> findAllExpands(int pageNo, int pageSize);

    /**
     * 得到一条的城市范围信息
     * @param id int
     * @return CityExpand
     */
    CityExpand findById(int id);
}
