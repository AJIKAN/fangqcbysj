package com.gxust.fangqcbysj.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gxust.fangqcbysj.entity.DriverInfo;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/30
 */
public interface DriverInfoService extends IService<DriverInfo> {

    boolean addNewDriver(DriverInfo driverInfo);

    boolean deleteById(String id);

    boolean updateDriverById(String id, DriverInfo driverInfo);

    Page<DriverInfo>  selectAllEmpByPage(int pageNo,int pageSize);

    DriverInfo findDriverById(String id);

    List<String> findDriverAllId();
}
