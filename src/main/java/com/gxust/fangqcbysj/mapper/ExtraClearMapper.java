package com.gxust.fangqcbysj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxust.fangqcbysj.entity.ExtraClear;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Mapper
public interface ExtraClearMapper extends BaseMapper<ExtraClear> {

    List<ExtraClear> findByBalanceDate(Date beginTime, Date endTime);
}
