package com.gxust.fangqcbysj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxust.fangqcbysj.entity.GoodsBill;
import com.gxust.fangqcbysj.entity.dto.GoodsBillDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Mapper
public interface GoodsBillMapper extends BaseMapper<GoodsBill> {

    /**
     *
     * @param type String
     * @param driverId String
     * @return List<GoodsBill>
     */
    List<GoodsBill>  transferState(@Param("type") String type,@Param("driverId") String driverId);

    /**
     *
     * @param type String
     * @param driverId String
     * @return List<GoodsBill>
     */
    List<GoodsBill> arriveGoods(String type, String driverId);

    List<GoodsBillDto> find();

    List<GoodsBill> selectGoodsBillList(String customerCode);

    List<GoodsBill> findGoodsBillByBillType(String billType,Page<GoodsBill> page);

    List<GoodsBill> findGoodsBillByType(String type, Page<GoodsBill> page);

    List<GoodsBill> findGoodsByAllGot(Page<GoodsBill> page);

    List<GoodsBill> findOnWayBills();
}
