package com.gxust.fangqcbysj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxust.fangqcbysj.entity.CargoReceipt;
import com.gxust.fangqcbysj.entity.dto.CargoReceiptDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Mapper
public interface CargoReceiptMapper extends BaseMapper<CargoReceipt> {

    List<CargoReceiptDto> find();

    List<CargoReceipt> findBySignTime(Date date,Date date1);
}
