package com.gxust.fangqcbysj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxust.fangqcbysj.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author 方全朝
 * @Description
 * @Date 2021/3/14
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
