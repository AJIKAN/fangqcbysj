package com.gxust.fangqcbysj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxust.fangqcbysj.entity.DriverAmount;
import com.gxust.fangqcbysj.entity.LineOverall;
import com.gxust.fangqcbysj.entity.dto.DriverClearDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Mapper
public interface DriverAmountMapper extends BaseMapper<DriverAmount> {


}
