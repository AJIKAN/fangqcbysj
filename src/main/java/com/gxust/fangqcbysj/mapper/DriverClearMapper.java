package com.gxust.fangqcbysj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxust.fangqcbysj.entity.DriverClear;
import com.gxust.fangqcbysj.entity.dto.DriverClearDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Mapper
public interface DriverClearMapper extends BaseMapper<DriverClear> {

    List<DriverClearDto> find();
}
