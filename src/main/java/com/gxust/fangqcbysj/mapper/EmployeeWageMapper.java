package com.gxust.fangqcbysj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxust.fangqcbysj.entity.EmployeeWage;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Mapper
public interface EmployeeWageMapper extends BaseMapper<EmployeeWage> {

    List<EmployeeWage> findByDate(Date beginTime, Date endTime);
}
