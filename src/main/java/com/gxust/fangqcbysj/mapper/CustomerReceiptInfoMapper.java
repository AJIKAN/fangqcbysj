package com.gxust.fangqcbysj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gxust.fangqcbysj.entity.CustomerReceiptInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 方全朝
 * @Description
 * @Date 2021/3/29
 */
@Mapper
public interface CustomerReceiptInfoMapper extends BaseMapper<CustomerReceiptInfo> {
}
