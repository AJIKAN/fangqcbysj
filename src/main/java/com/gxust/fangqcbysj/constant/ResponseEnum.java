package com.gxust.fangqcbysj.constant;

import lombok.Getter;

/**
 * @Author 方全朝
 * @Description
 * @Date 2021/3/14
 */
@Getter
public enum ResponseEnum {
    FAIL(0, "失败"),
    SUCCESS(1, "成功"),
    ;

    private int code;
    private String message;

    ResponseEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
