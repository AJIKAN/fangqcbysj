/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost:3306
 Source Schema         : logistics-test

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 11/06/2021 01:03:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for billinfo
-- ----------------------------
DROP TABLE IF EXISTS `billinfo`;
CREATE TABLE `billinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `accept_station` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '接货地点',
  `bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '货运单号',
  `bill_state` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '装态',
  `bill_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '票据类型',
  `write_date` datetime(6) NULL DEFAULT NULL COMMENT '填写日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '单据明细表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of billinfo
-- ----------------------------
INSERT INTO `billinfo` VALUES (71, NULL, 'HY794688', '已填', '货运单', '2021-05-22 22:13:28.057000');
INSERT INTO `billinfo` VALUES (72, '接货点', 'HZ999649', '已填', '货运回执单', '2021-05-22 22:15:19.488000');
INSERT INTO `billinfo` VALUES (73, NULL, 'HY334904', '已填', '货运单', '2021-05-22 23:28:53.467000');
INSERT INTO `billinfo` VALUES (74, '10', 'HZ860441', '已填', '货运回执单', '2021-05-22 23:30:46.060000');
INSERT INTO `billinfo` VALUES (75, NULL, 'HY062233', '已填', '货运单', '2021-05-23 00:05:49.077000');
INSERT INTO `billinfo` VALUES (76, NULL, 'HY713525', '已填', '货运单', '2021-05-23 00:07:04.286000');
INSERT INTO `billinfo` VALUES (77, '接货点', 'HZ492894', '已填', '货运回执单', '2021-05-23 00:19:48.382000');

-- ----------------------------
-- Table structure for billrelease
-- ----------------------------
DROP TABLE IF EXISTS `billrelease`;
CREATE TABLE `billrelease`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `accept_station` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '接货地点',
  `bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '货运单编号',
  `bill_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '单据类型',
  `receive_bill_person` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '接单人',
  `receive_bill_time` date NULL DEFAULT NULL COMMENT '日期',
  `release_person` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '操作人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '单据分发表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of billrelease
-- ----------------------------
INSERT INTO `billrelease` VALUES (59, '接货点', 'HY794688', '货运单', 'SJ258311', '2021-05-22', 'PJ495467');
INSERT INTO `billrelease` VALUES (60, '10', 'HY334904', '货运单', 'SJ258311', '2021-05-22', 'PJ495467');
INSERT INTO `billrelease` VALUES (61, '10', 'HY062233', '货运单', 'SJ258311', '2021-05-23', 'PJ495467');
INSERT INTO `billrelease` VALUES (62, '10', 'HY713525', '货运单', 'SJ258311', '2021-05-23', 'PJ495467');

-- ----------------------------
-- Table structure for callbackinfo
-- ----------------------------
DROP TABLE IF EXISTS `callbackinfo`;
CREATE TABLE `callbackinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `bill_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `bill_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '回告内容',
  `dial_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '拨出号码',
  `finally_dial_time` date NULL DEFAULT NULL COMMENT '最终拨出时间',
  `goods_bill_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '货运单编号',
  `locked` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '锁定',
  `success` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '成功',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '类别',
  `write_time` date NULL DEFAULT NULL COMMENT '填写日期',
  `writer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '填写人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 57 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '回告信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of callbackinfo
-- ----------------------------
INSERT INTO `callbackinfo` VALUES (49, '917589', '', '货物中转', '18275806425', '2021-05-22', 'HY794688', '0', '1', '中转回告', '2021-05-22', 'KF044108');
INSERT INTO `callbackinfo` VALUES (50, '945670', '', '到货回告', '18275806425', '2021-05-22', 'HY794688', '0', '1', '到货回告', '2021-05-22', 'KF044108');
INSERT INTO `callbackinfo` VALUES (51, '950733', '', '提货回告', '18275806425', '2021-05-22', 'HY794688', '0', '1', '提货回告', '2021-05-22', 'KF044108');
INSERT INTO `callbackinfo` VALUES (52, '406100', '', '已提回告', '18275806425', '2021-05-22', 'HY794688', '0', '1', '已提回告', '2021-05-22', 'KF044108');
INSERT INTO `callbackinfo` VALUES (53, '061272', '', '中转回告', '18275806425', '2021-05-22', 'HY334904', '0', '1', '中转回告', '2021-05-22', 'KF044108');
INSERT INTO `callbackinfo` VALUES (54, '176249', '', '11', '18275806425', '2021-05-22', 'HY334904', '0', '1', '提货回告', '2021-05-22', 'KF044108');
INSERT INTO `callbackinfo` VALUES (55, '247955', '', '11', '18275806425', '2021-05-22', 'HY334904', '0', '1', '到货回告', '2021-05-22', 'KF044108');
INSERT INTO `callbackinfo` VALUES (56, '515405', '', '111', '18275806425', '2021-05-22', 'HY334904', '0', '1', '已提回告', '2021-05-22', 'KF044108');

-- ----------------------------
-- Table structure for carcost
-- ----------------------------
DROP TABLE IF EXISTS `carcost`;
CREATE TABLE `carcost`  (
  `driver_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `add_carriage_total` double NOT NULL,
  `allow_carry_weight` double NULL DEFAULT NULL,
  `back_bill_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `balance_time` datetime(6) NULL DEFAULT NULL,
  `car_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `car_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `car_width` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `carry_fee_total` double NOT NULL,
  `deal_goods_station` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `fact_carriage_total` double NOT NULL,
  `goods_height` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `load_station` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`driver_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cargoerror
-- ----------------------------
DROP TABLE IF EXISTS `cargoerror`;
CREATE TABLE `cargoerror`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `goods_bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `goods_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `goods_revert_bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `goods_value` double NULL DEFAULT NULL,
  `mistake_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `piece_amount` int(11) NULL DEFAULT NULL,
  `size` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cargoreceipt
-- ----------------------------
DROP TABLE IF EXISTS `cargoreceipt`;
CREATE TABLE `cargoreceipt`  (
  `goods_revert_bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '回执单编号',
  `accept_station` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '接货点',
  `all_carriage` double NOT NULL COMMENT '总运费',
  `arrive_time` date NULL DEFAULT NULL COMMENT '到达时间',
  `back_bill_state` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '回执单状态',
  `carriage_banlance_mode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '结算方式',
  `carriage_mode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '计价方式',
  `carry_goods_bill_deposit` double NOT NULL COMMENT '送货单回执押金',
  `carry_goods_insurance` double NOT NULL COMMENT '承运人订装货物保证金',
  `deal_goods_station` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '交货地点',
  `dispatch_service_fee` double NOT NULL COMMENT '配载服务费',
  `driver_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '司机编号',
  `if_balance` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否结算',
  `insurance` double NOT NULL COMMENT '保险费',
  `linkman_phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系人电话',
  `load_station` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '装货地点',
  `receive_goods_detail_addr` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '收货详细地址',
  `receive_goods_linkman` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '收货联系人',
  `remark` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `sign_time` date NULL DEFAULT NULL COMMENT '签订时间',
  `start_advance` double NOT NULL COMMENT '起运时的预付费用',
  `start_carry_time` date NULL DEFAULT NULL COMMENT '起运时间',
  PRIMARY KEY (`goods_revert_bill_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '货运回执单主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cargoreceipt
-- ----------------------------
INSERT INTO `cargoreceipt` VALUES ('HZ492894', '接货点', 10, NULL, '未出合同', '运费结算方式', '运费计价方式', 10, 10, '顺德', 10, 'SJ258311', 'false', 10, '18275806425', '广州', '开源大道11号广州开发区科技企业加速器E1栋111', '方全朝', '备注', '2021-05-22', 10, NULL);
INSERT INTO `cargoreceipt` VALUES ('HZ860441', '10', 10, '2021-05-22', '已结合同', '10', '10', 10, 10, '顺德', 10, 'SJ258311', 'false', 10, '18275806425', '广州', '开源大道11号广州开发区科技企业加速器E1栋111', '方全朝', '10', '2021-05-21', 10, '2021-05-21');
INSERT INTO `cargoreceipt` VALUES ('HZ999649', '接货点', 10, '2021-05-22', '已结合同', '运费结算方式', '运费计价方式', 10, 10, '顺德', 10, 'SJ258311', 'false', 10, '18275806425', '广州', '开源大道11号广州开发区科技企业加速器E1栋111', '方全朝', '备注', '2021-05-21', 10, '2021-05-21');

-- ----------------------------
-- Table structure for cargoreceiptdetail
-- ----------------------------
DROP TABLE IF EXISTS `cargoreceiptdetail`;
CREATE TABLE `cargoreceiptdetail`  (
  `goods_revert_bill_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '回执单编号',
  `goods_bill_detail_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '货运单号',
  `goods_value` double NOT NULL,
  `piece_amount` int(11) NOT NULL COMMENT '件数',
  `price_mode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '计价方式',
  `price_standard` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '收费标准',
  `volume` double NOT NULL COMMENT '体积',
  `weight` double NOT NULL COMMENT '重量',
  PRIMARY KEY (`goods_revert_bill_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '货运回执单详表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cargoreceiptdetail
-- ----------------------------
INSERT INTO `cargoreceiptdetail` VALUES ('HZ492894', 'HY713525', 10, 10, '10', '10', 10, 10);
INSERT INTO `cargoreceiptdetail` VALUES ('HZ860441', 'HY334904', 10, 10, '10', '10', 1010, 10);
INSERT INTO `cargoreceiptdetail` VALUES ('HZ999649', 'HY794688', 100, 1, '计价方式', '计费标准', 10, 10);

-- ----------------------------
-- Table structure for cityexpand
-- ----------------------------
DROP TABLE IF EXISTS `cityexpand`;
CREATE TABLE `cityexpand`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `city_id` int(11) NOT NULL COMMENT '城市ID',
  `range_city` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '范围城市',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '城市扩充表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cityexpand
-- ----------------------------
INSERT INTO `cityexpand` VALUES (8, 1, '3,2');
INSERT INTO `cityexpand` VALUES (9, 2, '3');

-- ----------------------------
-- Table structure for compensationinfo
-- ----------------------------
DROP TABLE IF EXISTS `compensationinfo`;
CREATE TABLE `compensationinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amends` double NOT NULL,
  `amends_time` date NULL DEFAULT NULL,
  `bad_destroy_goods` double NOT NULL,
  `customer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `receive_station_id` int(11) NOT NULL,
  `receive_station_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `write_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for complaintinfo
-- ----------------------------
DROP TABLE IF EXISTS `complaintinfo`;
CREATE TABLE `complaintinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appeal_content` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `appeal_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `call_back_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `customer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `deal_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `deal_person` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `deal_result` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `goods_bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `if_callback` bit(1) NULL DEFAULT NULL,
  `if_handle` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for contactsservice
-- ----------------------------
DROP TABLE IF EXISTS `contactsservice`;
CREATE TABLE `contactsservice`  (
  `send_goods_customer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `balance` double NOT NULL,
  `bill_money` double NOT NULL,
  `carriage` double NOT NULL,
  `goods_bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `insurance` double NOT NULL,
  `money_receivable` double NOT NULL,
  `receive_goods_addr` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `received_money` double NOT NULL,
  `send_goods_addr` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `send_goods_date` datetime(6) NULL DEFAULT NULL,
  PRIMARY KEY (`send_goods_customer`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for customeramount
-- ----------------------------
DROP TABLE IF EXISTS `customeramount`;
CREATE TABLE `customeramount`  (
  `send_goods_customer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `carriage_total` double NOT NULL,
  `insurance_total` double NOT NULL,
  `piece_amount_total` int(11) NOT NULL,
  PRIMARY KEY (`send_goods_customer`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for customerbillclear
-- ----------------------------
DROP TABLE IF EXISTS `customerbillclear`;
CREATE TABLE `customerbillclear`  (
  `goods_bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '货运单编号',
  `customer_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户编号',
  `balance` double NULL DEFAULT NULL COMMENT '余额',
  `balance_time` date NULL DEFAULT NULL COMMENT '结算时间',
  `balance_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '结算类型',
  `bill_money` double NULL DEFAULT NULL COMMENT '本单金额',
  `carriage_reduce_fund` double NULL DEFAULT NULL COMMENT '运费减款',
  `carry_goods_fee` double NULL DEFAULT NULL COMMENT '送货费',
  `insurance` double NULL DEFAULT NULL COMMENT '保险费',
  `money_receivable` double NULL DEFAULT NULL COMMENT '应收金额',
  `pay_kickback` double NULL DEFAULT NULL COMMENT '付回扣',
  `prepay_money` double NULL DEFAULT NULL COMMENT '预收金额',
  `received_money` double NULL DEFAULT NULL COMMENT '已收金额',
  PRIMARY KEY (`goods_bill_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户运单结算表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customerbillclear
-- ----------------------------
INSERT INTO `customerbillclear` VALUES ('HY334904', 'KH881265', 0, '2021-05-22', '结入', 10, 0, 10, 10, 10, 10, 10, 20);
INSERT INTO `customerbillclear` VALUES ('HY794688', 'KH881265', 10, '2021-05-22', '结入', 10, 0, 10, 10, 10, 10, 10, 20);

-- ----------------------------
-- Table structure for customerinfo
-- ----------------------------
DROP TABLE IF EXISTS `customerinfo`;
CREATE TABLE `customerinfo`  (
  `customer_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '客户编号',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '地址',
  `customer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户名称',
  `customer_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户类型',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '邮件',
  `enterprise_property` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '企业性质',
  `enterprise_size` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '企业规模',
  `fax` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '传真',
  `linkman` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系人',
  `linkman_mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系人电话',
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '电话',
  `post_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '邮编',
  PRIMARY KEY (`customer_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户基本信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customerinfo
-- ----------------------------
INSERT INTO `customerinfo` VALUES ('KH232164', '开源大道11号广州开发区科技企业加速器E1栋', '方全朝', '测试客户类型', '2241495216@qq.com', '测试企业性质', '测试企业规模', '1234567', '方全朝联系人', '17687620572', '18275806425', '532703');
INSERT INTO `customerinfo` VALUES ('KH399347', '开源大道11号广州开发区科技企业加速器E1栋111', '方全朝', '测试客户类型', '2241495216@qq.com', '测试企业性质', '大', '123456', '方全朝', '17687620572', '18275806425', '532703');
INSERT INTO `customerinfo` VALUES ('KH505513', '开源大道11号广州开发区科技企业加速器E1栋111', '方全朝', '客户类型', '2241495216@qq.com', '企业性质', '大', '1234567', '方全朝', '18275806425', '18275806425', '532703');
INSERT INTO `customerinfo` VALUES ('KH881265', '开源大道11号广州开发区科技企业加速器E1栋', '方全朝', '客户类型', '2241495216@qq.com', '企业性质', '大', '1234567', '方全朝', '18275806425', '17687620572', '532703');

-- ----------------------------
-- Table structure for customerreceiptinfo
-- ----------------------------
DROP TABLE IF EXISTS `customerreceiptinfo`;
CREATE TABLE `customerreceiptinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `carry_bill_event_id` int(11) NOT NULL COMMENT '回执单编号',
  `check_goods_record` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '验收记录',
  `customer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户名称',
  `goods_bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '货运单编号',
  `receive_goods_date` date NULL DEFAULT NULL COMMENT '收货日期',
  `receive_goods_person` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '收货人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户回执信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customerreceiptinfo
-- ----------------------------
INSERT INTO `customerreceiptinfo` VALUES (13, 458870, '客户到货回执', '方全朝', 'HY794688', '2021-05-21', 'KH505513');
INSERT INTO `customerreceiptinfo` VALUES (14, 310585, '', '', '', NULL, '');
INSERT INTO `customerreceiptinfo` VALUES (15, 188727, '11111', '方全朝', 'HY334904', '2021-05-21', 'KH505513');

-- ----------------------------
-- Table structure for driveramount
-- ----------------------------
DROP TABLE IF EXISTS `driveramount`;
CREATE TABLE `driveramount`  (
  `driver_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `add_carriage_total` double NOT NULL,
  `carry_fee_total` double NOT NULL,
  `total` double NOT NULL,
  PRIMARY KEY (`driver_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for driverclear
-- ----------------------------
DROP TABLE IF EXISTS `driverclear`;
CREATE TABLE `driverclear`  (
  `back_bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '回执单编号',
  `add_carriage` double NULL DEFAULT NULL COMMENT '加运费',
  `balance` double NULL DEFAULT NULL COMMENT '预付金额',
  `balance_time` date NULL DEFAULT NULL COMMENT '结算时间',
  `balance_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '结算类型',
  `bind_insurance` double NULL DEFAULT NULL COMMENT '保险费',
  `carry_fee` double NULL DEFAULT NULL COMMENT '承运费',
  `dispatch_service_fee` double NULL DEFAULT NULL COMMENT '配载服务费',
  `driver_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '司机编号',
  `insurance` double NULL DEFAULT NULL COMMENT '保险费',
  `need_payment` double NULL DEFAULT NULL COMMENT '应付金额',
  `payed_money` double NULL DEFAULT NULL COMMENT '已付金额',
  `prepay_money` double NULL DEFAULT NULL COMMENT '预付金额',
  PRIMARY KEY (`back_bill_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '司机结算主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of driverclear
-- ----------------------------
INSERT INTO `driverclear` VALUES ('HZ860441', 10, 0, NULL, '结出', 10, 10, 10, 'SJ258311', 10, 30, 30, 10);
INSERT INTO `driverclear` VALUES ('HZ999649', 10, 0, NULL, '结出', 10, 10, 10, 'SJ258311', 10, 10, 30, 10);

-- ----------------------------
-- Table structure for driverinfo
-- ----------------------------
DROP TABLE IF EXISTS `driverinfo`;
CREATE TABLE `driverinfo`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '地址',
  `allow_carry_volume` double NULL DEFAULT NULL COMMENT '准载体积',
  `allow_carry_weight` double NULL DEFAULT NULL COMMENT '准载高',
  `birthday` date NULL DEFAULT NULL COMMENT '生日',
  `biz_licence` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '营运证',
  `car_dept` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '车属单位',
  `car_dept_tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '电话',
  `car_frame_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '车架号',
  `car_length` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '车长',
  `car_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '车号',
  `car_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '车型',
  `car_width` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '车宽',
  `company_car` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '是否公司车',
  `drive_licence` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '驾驶证',
  `driver_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '姓名',
  `engine_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '发动机号',
  `gender` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '性别',
  `goods_height` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '载物高度',
  `id_card` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '身份证',
  `insurance_card` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '车架号',
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '电话',
  `remark` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `run_licence` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '行驶证',
  `state` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '司机信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of driverinfo
-- ----------------------------
INSERT INTO `driverinfo` VALUES ('SJ047519', '司机2地址', 10, 10, '2021-05-13', '123456', '车属单位', '123456', '123456', '5', '123456111111', '车型', '2', '211312391736', '123456', '方全朝', '123456', '男', '3', '450123199907160913', '123456', '17687620572', '司机2备注', '123456', '空闲');
INSERT INTO `driverinfo` VALUES ('SJ258311', '司机住址', 10, 10, '2021-05-14', '123456', '123456', '18275806425', '123456', '5', '123456', '车型', '2', '48', '123456', '方全朝', '123456', '男', '3', '450123199907160913', '123456', '18275806425', '司机备注', '123456', '空闲');
INSERT INTO `driverinfo` VALUES ('SJ779687', '测试住址', 10, 10, '2021-04-11', '测试运营证', '测试车属单位', '18275806425', '123456', '5', '123456', '测试车型', '2', '0', '123456', '方全朝', '123456', '男', '3', '450123199907160913', '测试保险证', '18275806425', '测试备注', '123456', '空闲');

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `employee_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '职员编号',
  `birthday` date NULL DEFAULT NULL COMMENT '生日',
  `department` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '组名称',
  `employee_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '职员姓名',
  `gender` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '性别',
  `position` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '职位',
  PRIMARY KEY (`employee_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '职员信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('CW059907', '2021-05-14', '财务组', '方全朝', '男', '经理');
INSERT INTO `employee` VALUES ('GL034714', '2021-05-13', '管理组', '测试职员', '男', '员工');
INSERT INTO `employee` VALUES ('GL206577', '2021-05-14', '管理组', '方全朝', '男', '助理');
INSERT INTO `employee` VALUES ('KF044108', '2021-05-14', '客服组', '方全朝', '男', '经理');
INSERT INTO `employee` VALUES ('PJ495467', '2021-05-14', '票据组', '方全朝', '男', '经理');

-- ----------------------------
-- Table structure for employeeuser
-- ----------------------------
DROP TABLE IF EXISTS `employeeuser`;
CREATE TABLE `employeeuser`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `employee_id` int(11) NOT NULL COMMENT '职员ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '职员用户关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for employeewage
-- ----------------------------
DROP TABLE IF EXISTS `employeewage`;
CREATE TABLE `employeewage`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `allowance` double NOT NULL COMMENT '津贴',
  `basic_wage` double NOT NULL COMMENT '基本工资',
  `date` date NULL DEFAULT NULL COMMENT '日期',
  `employee` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `employee_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '员工编号',
  `station_wage` double NOT NULL COMMENT '岗位工资',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '员工工资表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employeewage
-- ----------------------------
INSERT INTO `employeewage` VALUES (6, 100, 100, '2021-05-22', '方全朝', '34307', 100);
INSERT INTO `employeewage` VALUES (7, 100, 100, '2021-05-22', '方全朝', 'CW059907', 100);

-- ----------------------------
-- Table structure for extraclear
-- ----------------------------
DROP TABLE IF EXISTS `extraclear`;
CREATE TABLE `extraclear`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `balance_date` date NULL DEFAULT NULL COMMENT '结算日期',
  `balance_money` double NULL DEFAULT NULL COMMENT '结算金额',
  `balance_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '结算类型',
  `remark` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `subject_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '科目',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '杂费结算表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of extraclear
-- ----------------------------
INSERT INTO `extraclear` VALUES (3, '2021-05-14', 100, '类型', '其他结算', '其他结算科目名称');
INSERT INTO `extraclear` VALUES (4, '2021-05-14', 1000, '其他结算', '备注', '测试其他结算');

-- ----------------------------
-- Table structure for extraincome
-- ----------------------------
DROP TABLE IF EXISTS `extraincome`;
CREATE TABLE `extraincome`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `income_month` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '收入月份',
  `money` double NOT NULL COMMENT '金额',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '收入名称',
  `write_date` date NULL DEFAULT NULL COMMENT '填写日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '营业外收入表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of extraincome
-- ----------------------------
INSERT INTO `extraincome` VALUES (4, '2021-04-30T16:00:00.000Z', 100, '业外收入', '2021-05-14');
INSERT INTO `extraincome` VALUES (5, '2021-04-30T16:00:00.000Z', 1000, '测试业外收入', '2021-05-14');

-- ----------------------------
-- Table structure for financefee
-- ----------------------------
DROP TABLE IF EXISTS `financefee`;
CREATE TABLE `financefee`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `fee` double NOT NULL COMMENT '费用',
  `payout_month` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '支出月份',
  `write_date` date NULL DEFAULT NULL COMMENT '填写日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '财务费用表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of financefee
-- ----------------------------
INSERT INTO `financefee` VALUES (3, 100, '2021-04-30T16:00:00.000Z', '2021-05-14');
INSERT INTO `financefee` VALUES (4, 1200, '2021-04-30T16:00:00.000Z', '2021-05-14');

-- ----------------------------
-- Table structure for function_
-- ----------------------------
DROP TABLE IF EXISTS `function_`;
CREATE TABLE `function_`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `page_function` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '页面功能',
  `page_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '功能名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '菜单功能表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of function_
-- ----------------------------
INSERT INTO `function_` VALUES (1, NULL, '票据管理');
INSERT INTO `function_` VALUES (2, NULL, '接货管理');
INSERT INTO `function_` VALUES (3, NULL, '配车管理');
INSERT INTO `function_` VALUES (4, NULL, '到货管理');
INSERT INTO `function_` VALUES (5, NULL, '中转管理');
INSERT INTO `function_` VALUES (6, NULL, '结算管理');
INSERT INTO `function_` VALUES (7, NULL, '客户服务');
INSERT INTO `function_` VALUES (8, NULL, '成本核算');
INSERT INTO `function_` VALUES (9, NULL, '应用管理');
INSERT INTO `function_` VALUES (10, NULL, '系统管理');

-- ----------------------------
-- Table structure for functionwithgroup
-- ----------------------------
DROP TABLE IF EXISTS `functionwithgroup`;
CREATE TABLE `functionwithgroup`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `function_id` int(11) NOT NULL COMMENT '功能ID',
  `group_id` int(11) NOT NULL COMMENT '组ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '功能与组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of functionwithgroup
-- ----------------------------
INSERT INTO `functionwithgroup` VALUES (1, 1, 1);
INSERT INTO `functionwithgroup` VALUES (2, 2, 1);
INSERT INTO `functionwithgroup` VALUES (3, 3, 1);
INSERT INTO `functionwithgroup` VALUES (4, 4, 1);
INSERT INTO `functionwithgroup` VALUES (5, 5, 1);
INSERT INTO `functionwithgroup` VALUES (6, 6, 1);
INSERT INTO `functionwithgroup` VALUES (7, 7, 1);
INSERT INTO `functionwithgroup` VALUES (8, 8, 1);
INSERT INTO `functionwithgroup` VALUES (9, 9, 1);
INSERT INTO `functionwithgroup` VALUES (10, 10, 1);
INSERT INTO `functionwithgroup` VALUES (12, 6, 3);
INSERT INTO `functionwithgroup` VALUES (13, 7, 4);
INSERT INTO `functionwithgroup` VALUES (14, 5, 6);
INSERT INTO `functionwithgroup` VALUES (15, 4, 6);
INSERT INTO `functionwithgroup` VALUES (16, 3, 6);
INSERT INTO `functionwithgroup` VALUES (17, 8, 3);
INSERT INTO `functionwithgroup` VALUES (18, 2, 5);
INSERT INTO `functionwithgroup` VALUES (19, 4, 5);
INSERT INTO `functionwithgroup` VALUES (55, 1, 2);

-- ----------------------------
-- Table structure for goodsbill
-- ----------------------------
DROP TABLE IF EXISTS `goodsbill`;
CREATE TABLE `goodsbill`  (
  `goods_bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '货运单编号',
  `accept_procedure_rate` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '代收手续费率',
  `accept_station` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '接货地点',
  `carriage` double NULL DEFAULT NULL COMMENT '运费',
  `carry_goods_fee` double NULL DEFAULT NULL COMMENT '送货费',
  `employee_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '员工编号',
  `fact_deal_date` date NULL DEFAULT NULL COMMENT '实际交货日期',
  `fetch_goods_mode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '取货方式',
  `help_accept_payment` double NULL DEFAULT NULL COMMENT '代收贷款',
  `if_audit` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否审核',
  `if_settle_accounts` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否结账',
  `insurance` double NULL DEFAULT NULL COMMENT '保险费',
  `money_of_change_pay` double NULL DEFAULT NULL COMMENT '变更后金额',
  `pay_kickback` double NULL DEFAULT NULL COMMENT '代回扣',
  `pay_mode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '付款方式',
  `predelivery_date` date NULL DEFAULT NULL COMMENT '预计交货日期',
  `receive_goods_addr` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '收货地址',
  `receive_goods_customer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '接货客户',
  `receive_goods_customer_addr` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '接货客户地址',
  `receive_goods_customer_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '接货客户编号',
  `receive_goods_customer_tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '接货客户电话',
  `reduce_fund` double NULL DEFAULT NULL COMMENT '减款',
  `remark` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `send_goods_addr` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '发货客户地址',
  `send_goods_customer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '发货客户',
  `send_goods_customer_addr` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '发货客户地址',
  `send_goods_customer_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '发货客户编号',
  `send_goods_customer_tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '发货客户电话',
  `send_goods_date` date NULL DEFAULT NULL COMMENT '发货日期',
  `transfer_fee` double NULL DEFAULT NULL COMMENT '中转费',
  `transfer_station` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '中转地',
  `validity` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否有效',
  `write_bill_person` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '填写人',
  `write_date` date NULL DEFAULT NULL COMMENT '填写日期',
  PRIMARY KEY (`goods_bill_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '货运单主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goodsbill
-- ----------------------------
INSERT INTO `goodsbill` VALUES ('HY062233', '10', '接货点', 10, 10, 'KH881265', NULL, '取货方式', 10, '0', 'false', 10, 0, 10, '付款方式', '2021-05-24', '', '方全朝', '开源大道11号广州开发区科技企业加速器E1栋111', 'KH505513', '18275806425', 0, '备注', '', '方全朝', '开源大道11号广州开发区科技企业加速器E1栋', 'KH881265', '17687620572', '2021-05-22', NULL, '', '0', 'KH881265', '2021-05-22');
INSERT INTO `goodsbill` VALUES ('HY334904', '10', '10', 10, 10, 'KH881265', '2021-05-22', '10', 10, '1', 'false', 10, 0, 10, '10', '2021-05-20', '', '方全朝', '开源大道11号广州开发区科技企业加速器E1栋111', 'KH505513', '18275806425', 0, '10', '', '方全朝', '开源大道11号广州开发区科技企业加速器E1栋', 'KH881265', '17687620572', '2021-05-20', 1.3, '佛山', '1', 'KH881265', '2021-05-20');
INSERT INTO `goodsbill` VALUES ('HY713525', '10', '10', 10, 10, '', NULL, '10', 10, '1', 'false', 10, 0, 10, '10', '2021-05-22', '', '方全朝', '开源大道11号广州开发区科技企业加速器E1栋111', 'KH505513', '18275806425', 0, '10', '', '方全朝', '开源大道11号广州开发区科技企业加速器E1栋', 'KH881265', '17687620572', '2021-05-20', 1.3, '佛山', '1', '', '2021-05-20');
INSERT INTO `goodsbill` VALUES ('HY794688', '10', '接货点', 10, 10, 'KH881265', '2021-05-22', '取货方式', 10, '1', 'false', 10, 0, 10, '付款方式', '2021-05-23', '', '方全朝', '开源大道11号广州开发区科技企业加速器E1栋111', 'KH505513', '18275806425', 0, '备注', '', '方全朝', '开源大道11号广州开发区科技企业加速器E1栋', 'KH881265', '17687620572', '2021-05-20', 1.3, '佛山', '1', 'KH881265', '2021-05-20');

-- ----------------------------
-- Table structure for goodsbillevent
-- ----------------------------
DROP TABLE IF EXISTS `goodsbillevent`;
CREATE TABLE `goodsbillevent`  (
  `goods_bill_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '货运单号',
  `event_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '事件名称',
  `occur_time` datetime(6) NULL DEFAULT NULL COMMENT '发生时间',
  `remark` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '备注',
  PRIMARY KEY (`goods_bill_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '货运单事件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goodsbillevent
-- ----------------------------
INSERT INTO `goodsbillevent` VALUES ('HY062233', '未到', '2021-05-23 00:07:47.238000', '单据已填');
INSERT INTO `goodsbillevent` VALUES ('HY334904', '已结运单', '2021-05-22 23:35:41.237000', '单据已填');
INSERT INTO `goodsbillevent` VALUES ('HY713525', '未到', '2021-05-23 00:07:56.639000', '单据已填');
INSERT INTO `goodsbillevent` VALUES ('HY794688', '已结运单', '2021-05-22 22:18:20.270000', '单据已填');

-- ----------------------------
-- Table structure for goodsreceiptinfo
-- ----------------------------
DROP TABLE IF EXISTS `goodsreceiptinfo`;
CREATE TABLE `goodsreceiptinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `check_goods_record` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '验收状态',
  `driver_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '司机姓名',
  `goods_revert_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '货物回执编号',
  `rceive_goods_date` date NULL DEFAULT NULL COMMENT '货物回执时间',
  `receive_goods_person` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '回执客户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '货物回执信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goodsreceiptinfo
-- ----------------------------
INSERT INTO `goodsreceiptinfo` VALUES (35, '司机到货通知', '方全朝', 'HZ999649', '2021-05-22', '方全朝');
INSERT INTO `goodsreceiptinfo` VALUES (36, '到货', '方全朝', 'HZ860441', '2021-05-22', '方全朝');

-- ----------------------------
-- Table structure for incomemonthlytemp
-- ----------------------------
DROP TABLE IF EXISTS `incomemonthlytemp`;
CREATE TABLE `incomemonthlytemp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `biz_fee` double NOT NULL COMMENT '经营费用',
  `biz_income` double NOT NULL COMMENT '营业收入',
  `car_carriage` double NOT NULL COMMENT '车运费',
  `carriage_money` double NOT NULL COMMENT '运费金额',
  `convey_wage` double NOT NULL COMMENT '搬运工资',
  `finance_fee` double NOT NULL COMMENT '财务费用',
  `house_rent` double NOT NULL COMMENT '房租',
  `income` double NOT NULL COMMENT '收入',
  `insurance_money` double NOT NULL COMMENT '保险金额',
  `manage_fee` double NOT NULL COMMENT '管理费用',
  `month` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '月份',
  `office_fee` double NOT NULL COMMENT '办公费',
  `other` double NOT NULL COMMENT '其他费用',
  `payout` double NOT NULL COMMENT '支出',
  `phone_fee` double NOT NULL COMMENT '电话费',
  `profit` double NULL DEFAULT NULL COMMENT '利润',
  `unbiz_income` double NOT NULL COMMENT '非营业收入',
  `wage` double NOT NULL COMMENT '工资',
  `water_elec_fee` double NOT NULL COMMENT '水电费',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '成本损益表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for lineoverall
-- ----------------------------
DROP TABLE IF EXISTS `lineoverall`;
CREATE TABLE `lineoverall`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `all_carriage_total` double NOT NULL COMMENT '运费总计',
  `deal_goods_station` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '交货地点',
  `insurance_total` double NOT NULL COMMENT '保险费总计',
  `load_station` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '装货地点',
  `times` int(11) NOT NULL COMMENT '次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '打印专线整体表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for managefee
-- ----------------------------
DROP TABLE IF EXISTS `managefee`;
CREATE TABLE `managefee`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `house_rent` double NOT NULL COMMENT '房租',
  `office_fee` double NOT NULL COMMENT '办公费',
  `other_payout` double NOT NULL COMMENT '其他费用',
  `payout_month` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '支出月份',
  `phone_fee` double NOT NULL COMMENT '电话费',
  `water_elec_fee` double NOT NULL COMMENT '水电费',
  `write_date` date NULL DEFAULT NULL COMMENT '填写日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '管理费用表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of managefee
-- ----------------------------
INSERT INTO `managefee` VALUES (2, 100, 100, 100, '2021-04-30T16:00:00.000Z', 100, 100, '2021-05-14');
INSERT INTO `managefee` VALUES (3, 10, 10, 10, '2021-04-30T16:00:00.000Z', 10, 10, '2021-05-14');

-- ----------------------------
-- Table structure for proxyfeeclear
-- ----------------------------
DROP TABLE IF EXISTS `proxyfeeclear`;
CREATE TABLE `proxyfeeclear`  (
  `goods_bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '货运单号',
  `account_receivable` double NULL DEFAULT NULL COMMENT '应收贷款',
  `balance_date` date NULL DEFAULT NULL COMMENT '余额日期',
  `commision_rate` float NULL DEFAULT NULL COMMENT '佣金率',
  `commision_receivable` double NULL DEFAULT NULL COMMENT '应收佣金',
  `customer_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户编号',
  `fact_receive_fund` double NULL DEFAULT NULL COMMENT '实收款项',
  `goods_pay_change` double NULL DEFAULT NULL COMMENT '贷款变更',
  `received_commision` double NULL DEFAULT NULL COMMENT '已收佣金',
  PRIMARY KEY (`goods_bill_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '代收货款结算表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for region
-- ----------------------------
DROP TABLE IF EXISTS `region`;
CREATE TABLE `region`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '城市',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '城市表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of region
-- ----------------------------
INSERT INTO `region` VALUES (1, '广州');
INSERT INTO `region` VALUES (2, '佛山');
INSERT INTO `region` VALUES (3, '顺德');

-- ----------------------------
-- Table structure for routeinfo
-- ----------------------------
DROP TABLE IF EXISTS `routeinfo`;
CREATE TABLE `routeinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `distance` double NOT NULL COMMENT '距离',
  `end_station` int(11) NULL DEFAULT NULL COMMENT '目的地ID',
  `fetch_time` double NOT NULL COMMENT '耗时',
  `pass_station` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '经过地区ID',
  `start_station` int(11) NULL DEFAULT NULL COMMENT '起始地ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '地区城市表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of routeinfo
-- ----------------------------
INSERT INTO `routeinfo` VALUES (2, 200, 3, 2, '2', 1);
INSERT INTO `routeinfo` VALUES (3, 100, 2, 1, '', 1);
INSERT INTO `routeinfo` VALUES (4, 100, 3, 1, '', 2);

-- ----------------------------
-- Table structure for sentlist
-- ----------------------------
DROP TABLE IF EXISTS `sentlist`;
CREATE TABLE `sentlist`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `back_cost` double NOT NULL COMMENT '回结',
  `car_card_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '车牌号',
  `cash_pay` double NOT NULL COMMENT '现付',
  `driver_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '司机名称',
  `goods_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '货物编号',
  `goods_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '货物名称',
  `goods_revert_bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '货运回执单编号',
  `help_accept_fund` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '代收款',
  `mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '手机',
  `pickup_pay` double NOT NULL COMMENT '提付',
  `piece_amount` int(11) NOT NULL COMMENT '件数',
  `remark` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `send_goods_customer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '发货客户',
  `send_goods_customer_tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '发货客户电话',
  `transfer_destination` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '中转目的地',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '装货发车清单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for transfercominfo
-- ----------------------------
DROP TABLE IF EXISTS `transfercominfo`;
CREATE TABLE `transfercominfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '城市',
  `company_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '中转公司名称',
  `detail_address` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '中转公司地址',
  `link_phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系电话',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '中转公司信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transfercominfo
-- ----------------------------
INSERT INTO `transfercominfo` VALUES (9, '广州', '广州中转公司', '开源大道11号广州开发区科技企业加速器E1栋', '18275806425');
INSERT INTO `transfercominfo` VALUES (10, '佛山', '佛山中转公司', '碧桂园西苑', '18275806425');
INSERT INTO `transfercominfo` VALUES (11, '顺德', '顺得中转公司', '碧乐时光', '18275806425');
INSERT INTO `transfercominfo` VALUES (12, '广州', '测试中转公司', '开源大道11号广州开发区科技企业加速器E1栋', '18275806425');

-- ----------------------------
-- Table structure for transferinfo
-- ----------------------------
DROP TABLE IF EXISTS `transferinfo`;
CREATE TABLE `transferinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `after_transfer_bill` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '中转货运单号',
  `check_time` date NULL DEFAULT NULL COMMENT '审核时间',
  `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `goods_bill_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '货运单号',
  `transfer_addr` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '中转地址',
  `transfer_check` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '中转审核人ID',
  `transfer_company` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '中转公司名称',
  `transfer_fee` double NULL DEFAULT NULL COMMENT '中转费',
  `transfer_station` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '中转站',
  `transfer_station_tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '联系电话',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '中转信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transferinfo
-- ----------------------------
INSERT INTO `transferinfo` VALUES (26, 'HY794688', '2021-05-22', '司机中转', 'HY794688', '碧桂园西苑', 'SJ258311', '佛山中转公司', 1.3, '佛山', '18275806425');
INSERT INTO `transferinfo` VALUES (27, 'HY334904', '2021-05-22', '中转', 'HY334904', '碧桂园西苑', 'SJ258311', '佛山中转公司', 1.3, '佛山', '18275806425');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `login_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '登录ID',
  `if_online` bit(1) NULL DEFAULT b'0' COMMENT '是否启用',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`login_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('CW059907', b'1', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('GL206577', b'1', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('GL846489', b'1', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('KF044108', b'1', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('KH232164', b'0', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('KH343307', b'0', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('KH399347', b'0', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('KH505513', b'1', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('KH881265', b'1', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('PJ495467', b'1', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('SJ047519', b'0', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('SJ258311', b'1', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('SJ779687', b'0', 'E10ADC3949BA59ABBE56E057F20F883E');
INSERT INTO `user` VALUES ('SJ919682', b'0', 'E10ADC3949BA59ABBE56E057F20F883E');

-- ----------------------------
-- Table structure for usergroup
-- ----------------------------
DROP TABLE IF EXISTS `usergroup`;
CREATE TABLE `usergroup`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `group_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '组名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of usergroup
-- ----------------------------
INSERT INTO `usergroup` VALUES (1, '系统管理员，最大权限', '管理组');
INSERT INTO `usergroup` VALUES (2, '票据分发，填写一份货运单合同', '票据组');
INSERT INTO `usergroup` VALUES (3, '结算管理，结算各种费用，核算成本', '财务组');
INSERT INTO `usergroup` VALUES (4, '客户服务，管理提货回告、到货回告、已提回告、中转回告', '客服组');
INSERT INTO `usergroup` VALUES (5, '填写货运单，收货，填写收货回执', '客户组');
INSERT INTO `usergroup` VALUES (6, '司机组，填写运输合同，发货，中转，到货回执', '司机组');

-- ----------------------------
-- Table structure for userwithgroup
-- ----------------------------
DROP TABLE IF EXISTS `userwithgroup`;
CREATE TABLE `userwithgroup`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_id` int(11) NOT NULL COMMENT '组ID',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户与组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of userwithgroup
-- ----------------------------
INSERT INTO `userwithgroup` VALUES (1, 1, 'GL846489');
INSERT INTO `userwithgroup` VALUES (36, 5, 'KH505513');
INSERT INTO `userwithgroup` VALUES (38, 5, 'KH881265');
INSERT INTO `userwithgroup` VALUES (39, 6, 'SJ258311');
INSERT INTO `userwithgroup` VALUES (41, 6, 'SJ047519');
INSERT INTO `userwithgroup` VALUES (45, 5, 'KH343307');
INSERT INTO `userwithgroup` VALUES (46, 6, 'SJ919682');
INSERT INTO `userwithgroup` VALUES (48, 2, 'PJ495467');
INSERT INTO `userwithgroup` VALUES (49, 3, 'CW059907');
INSERT INTO `userwithgroup` VALUES (50, 4, 'KF044108');
INSERT INTO `userwithgroup` VALUES (51, 1, 'GL206577');
INSERT INTO `userwithgroup` VALUES (52, 5, 'KH399347');
INSERT INTO `userwithgroup` VALUES (53, 1, 'GL034714');
INSERT INTO `userwithgroup` VALUES (54, 5, 'KH232164');
INSERT INTO `userwithgroup` VALUES (55, 6, 'SJ779687');

SET FOREIGN_KEY_CHECKS = 1;
