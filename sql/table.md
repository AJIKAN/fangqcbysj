```mysql
CREATE TABLE `billinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `accept_station` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '接货地点',
  `bill_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '货运单号',
  `bill_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '装态',
  `bill_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '票据类型',
  `write_date` datetime(6) DEFAULT NULL COMMENT '填写日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='单据明细表';

CREATE TABLE `billrelease` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `accept_station` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '接货地点',
  `bill_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '货运单编号',
  `bill_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '单据类型',
  `receive_bill_person` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '接单人',
  `receive_bill_time` date DEFAULT NULL COMMENT '日期',
  `release_person` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '操作人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='单据分发表';

CREATE TABLE `callbackinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `bill_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bill_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '回告内容',
  `dial_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '拨出号码',
  `finally_dial_time` date DEFAULT NULL COMMENT '最终拨出时间',
  `goods_bill_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '货运单编号',
  `locked` bit(1) NOT NULL COMMENT '锁定',
  `success` bit(1) NOT NULL COMMENT '成功',
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '类别',
  `write_time` date DEFAULT NULL COMMENT '填写日期',
  `writer` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '填写人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='回告信息表';

CREATE TABLE `carcost` (
  `driver_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `add_carriage_total` double NOT NULL,
  `allow_carry_weight` double DEFAULT NULL,
  `back_bill_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance_time` datetime(6) DEFAULT NULL,
  `car_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `car_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `car_width` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `carry_fee_total` double NOT NULL,
  `deal_goods_station` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fact_carriage_total` double NOT NULL,
  `goods_height` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `load_station` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`driver_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `cargoerror` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `goods_bill_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `goods_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `goods_revert_bill_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `goods_value` double DEFAULT NULL,
  `mistake_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `piece_amount` int(11) DEFAULT NULL,
  `size` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `cargoreceipt` (
  `goods_revert_bill_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '回执单编号',
  `accept_station` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '接货点',
  `all_carriage` double NOT NULL COMMENT '总运费',
  `arrive_time` date DEFAULT NULL COMMENT '到达时间',
  `back_bill_state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '回执单状态',
  `carriage_banlance_mode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '结算方式',
  `carriage_mode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '计价方式',
  `carry_goods_bill_deposit` double NOT NULL COMMENT '送货单回执押金',
  `carry_goods_insurance` double NOT NULL COMMENT '承运人订装货物保证金',
  `deal_goods_station` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '交货地点',
  `dispatch_service_fee` double NOT NULL COMMENT '配载服务费',
  `driver_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '司机编号',
  `if_balance` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '是否结算',
  `insurance` double NOT NULL COMMENT '保险费',
  `linkman_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系人电话',
  `load_station` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '装货地点',
  `receive_goods_detail_addr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收货详细地址',
  `receive_goods_linkman` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收货联系人',
  `remark` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `sign_time` date DEFAULT NULL COMMENT '签订时间',
  `start_advance` double NOT NULL COMMENT '起运时的预付费用',
  `start_carry_time` date DEFAULT NULL COMMENT '起运时间',
  PRIMARY KEY (`goods_revert_bill_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='货运回执单主表';

CREATE TABLE `cargoreceiptdetail` (
  `goods_revert_bill_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '回执单编号',
  `goods_bill_detail_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '货运单号',
  `goods_value` double NOT NULL,
  `piece_amount` int(11) NOT NULL COMMENT '件数',
  `price_mode` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '计价方式',
  `price_standard` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '收费标准',
  `volume` double NOT NULL COMMENT '体积',
  `weight` double NOT NULL COMMENT '重量',
  PRIMARY KEY (`goods_revert_bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='货运回执单详表';

CREATE TABLE `cityexpand` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `city_id` int(11) NOT NULL COMMENT '城市ID',
  `range_city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '范围城市',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='城市扩充表';

CREATE TABLE `compensationinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amends` double NOT NULL,
  `amends_time` date DEFAULT NULL,
  `bad_destroy_goods` double NOT NULL,
  `customer` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receive_station_id` int(11) NOT NULL,
  `receive_station_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `write_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `complaintinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appeal_content` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appeal_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `call_back_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deal_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deal_person` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deal_result` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `goods_bill_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `if_callback` bit(1) DEFAULT NULL,
  `if_handle` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `contactsservice` (
  `send_goods_customer` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `balance` double NOT NULL,
  `bill_money` double NOT NULL,
  `carriage` double NOT NULL,
  `goods_bill_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insurance` double NOT NULL,
  `money_receivable` double NOT NULL,
  `receive_goods_addr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `received_money` double NOT NULL,
  `send_goods_addr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `send_goods_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`send_goods_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `customeramount` (
  `send_goods_customer` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `carriage_total` double NOT NULL,
  `insurance_total` double NOT NULL,
  `piece_amount_total` int(11) NOT NULL,
  PRIMARY KEY (`send_goods_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `customerbillclear` (
  `goods_bill_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '货运单编号',
  `customer_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户编号',
  `balance` double DEFAULT NULL COMMENT '余额',
  `balance_time` date DEFAULT NULL COMMENT '结算时间',
  `balance_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '结算类型',
  `bill_money` double DEFAULT NULL COMMENT '本单金额',
  `carriage_reduce_fund` double DEFAULT NULL COMMENT '运费减款',
  `carry_goods_fee` double DEFAULT NULL COMMENT '送货费',
  `insurance` double DEFAULT NULL COMMENT '保险费',
  `money_receivable` double DEFAULT NULL COMMENT '应收金额',
  `pay_kickback` double DEFAULT NULL COMMENT '付回扣',
  `prepay_money` double DEFAULT NULL COMMENT '预收金额',
  `received_money` double DEFAULT NULL COMMENT '已收金额',
  PRIMARY KEY (`goods_bill_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='客户运单结算表';

CREATE TABLE `customerinfo` (
  `customer_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '客户编号',
  `address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '地址',
  `customer` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户名称',
  `customer_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户类型',
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮件',
  `enterprise_property` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业性质',
  `enterprise_size` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业规模',
  `fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '传真',
  `linkman` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系人',
  `linkman_mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系人电话',
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '电话',
  `post_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮编',
  PRIMARY KEY (`customer_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='客户基本信息表';

CREATE TABLE `customerreceiptinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `carry_bill_event_id` int(11) NOT NULL COMMENT '回执单编号',
  `check_goods_record` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '验收记录',
  `customer` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户名称',
  `goods_bill_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '货运单编号',
  `receive_goods_date` date DEFAULT NULL COMMENT '收货日期',
  `receive_goods_person` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收货人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='客户回执信息表';

CREATE TABLE `driveramount` (
  `driver_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `add_carriage_total` double NOT NULL,
  `carry_fee_total` double NOT NULL,
  `total` double NOT NULL,
  PRIMARY KEY (`driver_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `driverclear` (
  `back_bill_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '回执单编号',
  `add_carriage` double DEFAULT NULL COMMENT '加运费',
  `balance` double DEFAULT NULL COMMENT '预付金额',
  `balance_time` date DEFAULT NULL COMMENT '结算时间',
  `balance_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '结算类型',
  `bind_insurance` double DEFAULT NULL COMMENT '保险费',
  `carry_fee` double DEFAULT NULL COMMENT '承运费',
  `dispatch_service_fee` double DEFAULT NULL COMMENT '配载服务费',
  `driver_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '司机编号',
  `insurance` double DEFAULT NULL COMMENT '保险费',
  `need_payment` double DEFAULT NULL COMMENT '应付金额',
  `payed_money` double DEFAULT NULL COMMENT '已付金额',
  `prepay_money` double DEFAULT NULL COMMENT '预付金额',
  PRIMARY KEY (`back_bill_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='司机结算主表';

CREATE TABLE `driverinfo` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID',
  `address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '地址',
  `allow_carry_volume` double DEFAULT NULL COMMENT '准载体积',
  `allow_carry_weight` double DEFAULT NULL COMMENT '准载高',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `biz_licence` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营运证',
  `car_dept` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '车属单位',
  `car_dept_tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '电话',
  `car_frame_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '车架号',
  `car_length` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '车长',
  `car_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '车号',
  `car_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '车型',
  `car_width` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '车宽',
  `company_car` bit(1) NOT NULL COMMENT '是否公司车',
  `drive_licence` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '驾驶证',
  `driver_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '姓名',
  `engine_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发动机号',
  `gender` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '性别',
  `goods_height` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '载物高度',
  `id_card` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '身份证',
  `insurance_card` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '车架号',
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '电话',
  `remark` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `run_licence` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '行驶证',
  `state` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='司机信息表';

CREATE TABLE `employee` (
  `employee_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '职员编号',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `department` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '组名称',
  `employee_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '职员姓名',
  `gender` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '性别',
  `position` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '职位',
  PRIMARY KEY (`employee_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='职员信息表';

CREATE TABLE `employeeuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `employee_id` int(11) NOT NULL COMMENT '职员ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='职员用户关系表';

CREATE TABLE `employeewage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `allowance` double NOT NULL COMMENT '津贴',
  `basic_wage` double NOT NULL COMMENT '基本工资',
  `date` date DEFAULT NULL COMMENT '日期',
  `employee` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '员工姓名',
  `employee_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '员工编号',
  `station_wage` double NOT NULL COMMENT '岗位工资',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='员工工资表';

CREATE TABLE `extraclear` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `balance_date` date DEFAULT NULL COMMENT '结算日期',
  `balance_money` double DEFAULT NULL COMMENT '结算金额',
  `balance_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '结算类型',
  `remark` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `subject_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '科目',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='杂费结算表';

CREATE TABLE `extraincome` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `income_month` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收入月份',
  `money` double NOT NULL COMMENT '金额',
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收入名称',
  `write_date` date DEFAULT NULL COMMENT '填写日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='营业外收入表';

CREATE TABLE `financefee` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `fee` double NOT NULL COMMENT '费用',
  `payout_month` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '支出月份',
  `write_date` date DEFAULT NULL COMMENT '填写日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='财务费用表';

CREATE TABLE `function_` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `page_function` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '页面功能',
  `page_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '功能名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='菜单功能表';

CREATE TABLE `functionwithgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `function_id` int(11) NOT NULL COMMENT '功能ID',
  `group_id` int(11) NOT NULL COMMENT '组ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='功能与组表';

CREATE TABLE `goodsbill` (
  `goods_bill_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '货运单编号',
  `accept_procedure_rate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '代收手续费率',
  `accept_station` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '接货地点',
  `carriage` double DEFAULT NULL COMMENT '运费',
  `carry_goods_fee` double DEFAULT NULL COMMENT '送货费',
  `employee_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '员工编号',
  `fact_deal_date` date DEFAULT NULL COMMENT '实际交货日期',
  `fetch_goods_mode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '取货方式',
  `help_accept_payment` double DEFAULT NULL COMMENT '代收贷款',
  `if_audit` bit(1) DEFAULT NULL COMMENT '是否审核',
  `if_settle_accounts` bit(1) DEFAULT NULL COMMENT '是否结账',
  `insurance` double DEFAULT NULL COMMENT '保险费',
  `money_of_change_pay` double DEFAULT NULL COMMENT '变更后金额',
  `pay_kickback` double DEFAULT NULL COMMENT '代回扣',
  `pay_mode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '付款方式',
  `predelivery_date` date DEFAULT NULL COMMENT '预计交货日期',
  `receive_goods_addr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收货地址',
  `receive_goods_customer` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '接货客户',
  `receive_goods_customer_addr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '接货客户地址',
  `receive_goods_customer_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '接货客户编号',
  `receive_goods_customer_tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '接货客户电话',
  `reduce_fund` double DEFAULT NULL COMMENT '减款',
  `remark` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `send_goods_addr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发货客户地址',
  `send_goods_customer` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发货客户',
  `send_goods_customer_addr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发货客户地址',
  `send_goods_customer_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发货客户编号',
  `send_goods_customer_tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发货客户电话',
  `send_goods_date` date DEFAULT NULL COMMENT '发货日期',
  `transfer_fee` double DEFAULT NULL COMMENT '中转费',
  `transfer_station` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '中转地',
  `validity` bit(1) DEFAULT NULL COMMENT '是否有效',
  `write_bill_person` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '填写人',
  `write_date` date DEFAULT NULL COMMENT '填写日期',
  PRIMARY KEY (`goods_bill_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='货运单主表';

CREATE TABLE `goodsbillevent` (
  `goods_bill_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '货运单号',
  `event_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '事件名称',
  `occur_time` datetime(6) DEFAULT NULL COMMENT '发生时间',
  `remark` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '备注',
  PRIMARY KEY (`goods_bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='货运单事件表';

CREATE TABLE `goodsreceiptinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `check_goods_record` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '验收状态',
  `driver_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '司机姓名',
  `goods_revert_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '货物回执编号',
  `rceive_goods_date` date DEFAULT NULL COMMENT '货物回执时间',
  `receive_goods_person` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '回执客户',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='货物回执信息表';

CREATE TABLE `incomemonthlytemp` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `biz_fee` double NOT NULL COMMENT '经营费用',
  `biz_income` double NOT NULL COMMENT '营业收入',
  `car_carriage` double NOT NULL COMMENT '车运费',
  `carriage_money` double NOT NULL COMMENT '运费金额',
  `convey_wage` double NOT NULL COMMENT '搬运工资',
  `finance_fee` double NOT NULL COMMENT '财务费用',
  `house_rent` double NOT NULL COMMENT '房租',
  `income` double NOT NULL COMMENT '收入',
  `insurance_money` double NOT NULL COMMENT '保险金额',
  `manage_fee` double NOT NULL COMMENT '管理费用',
  `month` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '月份',
  `office_fee` double NOT NULL COMMENT '办公费',
  `other` double NOT NULL COMMENT '其他费用',
  `payout` double NOT NULL COMMENT '支出',
  `phone_fee` double NOT NULL COMMENT '电话费',
  `profit` double DEFAULT NULL COMMENT '利润',
  `unbiz_income` double NOT NULL COMMENT '非营业收入',
  `wage` double NOT NULL COMMENT '工资',
  `water_elec_fee` double NOT NULL COMMENT '水电费',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='成本损益表';

CREATE TABLE `lineoverall` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `all_carriage_total` double NOT NULL COMMENT '运费总计',
  `deal_goods_station` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '交货地点',
  `insurance_total` double NOT NULL COMMENT '保险费总计',
  `load_station` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '装货地点',
  `times` int(11) NOT NULL COMMENT '次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='打印专线整体表';

CREATE TABLE `managefee` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `house_rent` double NOT NULL COMMENT '房租',
  `office_fee` double NOT NULL COMMENT '办公费',
  `other_payout` double NOT NULL COMMENT '其他费用',
  `payout_month` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '支出月份',
  `phone_fee` double NOT NULL COMMENT '电话费',
  `water_elec_fee` double NOT NULL COMMENT '水电费',
  `write_date` date DEFAULT NULL COMMENT '填写日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='管理费用表';

CREATE TABLE `proxyfeeclear` (
  `goods_bill_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '货运单号',
  `account_receivable` double DEFAULT NULL COMMENT '应收贷款',
  `balance_date` date DEFAULT NULL COMMENT '余额日期',
  `commision_rate` float DEFAULT NULL COMMENT '佣金率',
  `commision_receivable` double DEFAULT NULL COMMENT '应收佣金',
  `customer_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户编号',
  `fact_receive_fund` double DEFAULT NULL COMMENT '实收款项',
  `goods_pay_change` double DEFAULT NULL COMMENT '贷款变更',
  `received_commision` double DEFAULT NULL COMMENT '已收佣金',
  PRIMARY KEY (`goods_bill_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='代收货款结算表';

CREATE TABLE `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '城市',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='城市表';

CREATE TABLE `routeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `distance` double NOT NULL COMMENT '距离',
  `end_station` int(11) DEFAULT NULL COMMENT '目的地ID',
  `fetch_time` double NOT NULL COMMENT '耗时',
  `pass_station` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经过地区ID',
  `start_station` int(11) DEFAULT NULL COMMENT '起始地ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1951 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='地区城市表';

CREATE TABLE `sentlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `back_cost` double NOT NULL COMMENT '回结',
  `car_card_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '车牌号',
  `cash_pay` double NOT NULL COMMENT '现付',
  `driver_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '司机名称',
  `goods_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '货物编号',
  `goods_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '货物名称',
  `goods_revert_bill_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '货运回执单编号',
  `help_accept_fund` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '代收款',
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '手机',
  `pickup_pay` double NOT NULL COMMENT '提付',
  `piece_amount` int(11) NOT NULL COMMENT '件数',
  `remark` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `send_goods_customer` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发货客户',
  `send_goods_customer_tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发货客户电话',
  `transfer_destination` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '中转目的地',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='装货发车清单';

CREATE TABLE `transfercominfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '城市',
  `company_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '中转公司名称',
  `detail_address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '中转公司地址',
  `link_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系电话',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='中转公司信息表';

CREATE TABLE `transferinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `after_transfer_bill` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '中转货运单号',
  `check_time` date DEFAULT NULL COMMENT '审核时间',
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述',
  `goods_bill_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '货运单号',
  `transfer_addr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '中转地址',
  `transfer_check` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '中转审核人ID',
  `transfer_company` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '中转公司名称',
  `transfer_fee` double DEFAULT NULL COMMENT '中转费',
  `transfer_station` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '中转站',
  `transfer_station_tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系电话',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='中转信息表';

CREATE TABLE `user` (
  `login_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '登录ID',
  `if_online` bit(1) DEFAULT b'0' COMMENT '是否启用',
  `password` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户表';

CREATE TABLE `usergroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述',
  `group_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '组名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户组表';

CREATE TABLE `userwithgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_id` int(11) NOT NULL COMMENT '组ID',
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户与组表';
```

```java
spring.datasource.url = jdbc:mysql://127.0.0.1:3306/logistics-test?characterEncoding=utf8&useSSL=false
spring.datasource.username = root
spring.datasource.password = fangqc
```

